using UnityEngine;
using System.Collections;
using AssemblyCSharp;
/*using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.SOAP;*/

[System.Serializable]
public class BourkeV2
{

		Terrain terrain;
		TerrainData tData;
		float[,] heights;
		int xRes, yRes;

		Main main = GameObject.Find("Terrain").GetComponent<Main>();

		float incrBourke, decrBourke;
		

		public BourkeV2 (TerrainData _tData)
		{
				tData = _tData;
				xRes = tData.heightmapWidth;
				yRes = tData.heightmapHeight;
				heights = tData.GetHeights(0, 0, xRes, yRes);

				incrBourke = main.incrBourke;
				decrBourke = main.decrBourke;
		}


		public TerrainData Crear()
		{
				tData = AlgoritmoBourkeV2(UnityEngine.Random.Range(0.0001f, 0.001f), UnityEngine.Random.Range(100,300));
				return tData;
		}
		
		
		public TerrainData Crear(float ataqueBourke, int iteraciones)
		{
				tData = AlgoritmoBourkeV2(ataqueBourke, iteraciones);
				return tData;	
		}

		public TerrainData Crear(int iteraciones)
		{
				tData = AlgoritmoBourkeV2(UnityEngine.Random.Range(0.0001f, 0.001f), iteraciones);
				return tData;
		}

		public float[,] heightsMap
		{
			get { return heights; }
		}


		TerrainData AlgoritmoBourkeV2(float ataqueBourke, int iteraciones)
		{
				int angulo, x, y;

				if(main.generarNuevoBourke)
				{
					for (y = 0; y < yRes; y++)
					{
						for (x = 0; x < xRes; x++)
						{
							heights[x,y] = 0f;
						}
					}
				}

				for (int i = 0; i < iteraciones; i ++)
				{
					// escogemos un punto del mapa al azar:
					x = UnityEngine.Random.Range(50, (xRes-50));
					y = UnityEngine.Random.Range(50, (yRes-50));
					
					// escogemos un angulo al azar:
					angulo = UnityEngine.Random.Range(1, 90);
					
					int signo = Random.Range(1,2);
					if (signo == 1) { ataqueBourke = - ataqueBourke; }

					// triangangulo superior contando el eje Y hacia arriba:
					
					float b2 = xRes - x;
					float a2 = Mathf.Tan(angulo) * b2;
					
					for (int _y = y; _y < yRes; _y++)
					{
						for (int _x = 0; _x < xRes; _x++)
						{
							if (_x < (int) b2) { heights[_x, _y] = heights[_x, _y] - ataqueBourke * decrBourke; }
							else { heights[_x, _y] = heights[_x, _y] + ataqueBourke * incrBourke; }
						}
						a2--;
						b2 = a2 / Mathf.Tan(angulo);
					}
					
					
					// triangangulo inferior y el eje Y contando hacia abajo:
					
					b2 = xRes - x;
					a2 = Mathf.Tan(-angulo) * b2;
					
					for (int _y = (y-1); _y >= 0; _y--)
					{
						for (int _x = 0; _x < xRes; _x++)
						{
							if (_x < (int) b2) { heights[_x, _y] = heights[_x, _y] - ataqueBourke * decrBourke; }
							else { heights[_x, _y] = heights[_x, _y] + ataqueBourke * incrBourke; }
						}
						a2--;
						b2 = a2 / Mathf.Tan(-angulo);
					}
				}

				tData.SetHeights(0, 0, heights);
				return tData;
		}
}

