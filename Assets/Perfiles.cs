using UnityEngine;
using System.Collections;
using AssemblyCSharp;
using System;

public class Perfiles : MonoBehaviour
{
		Terrain terrain;
		TerrainData tData;
		float[,] heights;
		int xRes, yRes;

		/*int counter = 0, excepciones = 0;
		bool generar = false;*/
		bool mostrarGUI = true;
		string perfilTexturas = "normal";
	
		Contador contadorUniversal;
		Main main;
		object tecnicaGeneradora;
	
		string tituloGUI = "Generador de terreno automatico a traves de perfiles conocidos.";

		int puntosAltura;		String sPuntosAltura;
		float altMin;			String sAltMin;
		float altMax;			String sAltMax;
		float ataqBourke;		String sAtaqBourke;
		int iterBourke;			String sIterBourke;
		int crateres;			String sCrateres;
		float variabilidad = 1f;	string sVariabilidad;
		float pesoNoise = 1f;		string sPesoNoise;
		float pesoMin = 0.3f;		String sPesoMin = "0.8";
		float pesoMax = 0.8f;		String sPesoMax = "1";
		float varMin = 1f;			String sVarMin = "1";
		float varMax = 1.5f;		String sVarMax = "1";
		string tecnica = "ridge";
	


		void OnGUI ()
		{
			if (mostrarGUI)
			{
				// Titulos:
				GUI.color = Color.yellow;
				GUI.Box(new Rect(250, 10, tituloGUI.Length*6+20, 20), tituloGUI);
				GUI.Box(new Rect(250, 50, 135, 20), "Perfiles: ");
				GUI.Box(new Rect(390, 50, 250, 20), "Parametros: ");
				GUI.color = Color.white;

				// Perfiles:

				if (GUI.Button(new Rect(250, 70, 135, 20), "Desiertos con mar"))
				{
					puntosAltura = UnityEngine.Random.Range(1, 3);
					altMin = UnityEngine.Random.Range(-0.4f, -0.2f);
					altMax = UnityEngine.Random.Range(0.2f, 0.4f);

					ataqBourke = 0f;
					iterBourke = -1;

					crateres = 0;

					perfilTexturas = "desierto";

					variabilidad = 0.5f;
					//tecnicaGeneradora = new LibNoise.Generator.RidgedMultifractal();
					tecnica = "ridged";
			
					ConsolidarValores();
				}
				if (GUI.Button(new Rect(250, 230, 135, 20), "Desierto sin mar"))
				{
					puntosAltura = UnityEngine.Random.Range(1, 3);
					altMin = UnityEngine.Random.Range(0f, 0.1f);
					altMax = UnityEngine.Random.Range(0.2f, 0.4f);
					
					ataqBourke = 0f;
					iterBourke = -1;
					
					crateres = 0;
					
					perfilTexturas = "desierto";
					
					variabilidad = 0.5f;
					//tecnicaGeneradora = new LibNoise.Generator.RidgedMultifractal();
					tecnica = "ridged";
					
					ConsolidarValores();
				}
				if (GUI.Button(new Rect(250, 90, 135, 20), "Campo sin lagos"))
					{
						puntosAltura = UnityEngine.Random.Range(2, 4);
						altMin = UnityEngine.Random.Range(0, 0.1f);
						altMax = UnityEngine.Random.Range(0.2f, 0.5f);

						ataqBourke = 0f;
						iterBourke = -1;

						crateres = 0;

						perfilTexturas = "campo";

						//tecnicaGeneradora = new LibNoise.Generator.Perlin();
						tecnica = "perlin";
				
						ConsolidarValores();
					}
				if (GUI.Button(new Rect(250, 110, 135, 20), "Campo con lagos"))
					{
						puntosAltura = UnityEngine.Random.Range(2, 4);
						altMin = UnityEngine.Random.Range(-0.3f, -0.1f);
						altMax = UnityEngine.Random.Range(0.2f, 0.4f);

						ataqBourke = 0f;
						iterBourke = -1;

						crateres = 0;

						perfilTexturas = "campo";

						//tecnicaGeneradora = new LibNoise.Generator.Perlin();
						tecnica = "perlin";
				
						ConsolidarValores();
					}
				if (GUI.Button(new Rect(250, 130, 135, 20), "Pantano"))
					{
						puntosAltura = UnityEngine.Random.Range(5, 7);
						altMin = UnityEngine.Random.Range(-0.15f, -0.1f);
						altMax = UnityEngine.Random.Range(0.1f, 0.15f);
						
						//tecnicaGeneradora = new LibNoise.Generator.Billow();
						tecnica = "billow";
						variabilidad = 1f;

						ataqBourke = 0f;
						iterBourke = -1;

						crateres = 0;

						perfilTexturas = "pantano";

						ConsolidarValores();
					}
				if (GUI.Button(new Rect(250, 150, 135, 20), "Marte"))
					{
						puntosAltura = UnityEngine.Random.Range(4, 5);
						altMin = UnityEngine.Random.Range(0f, 0.1f);
						altMax = UnityEngine.Random.Range(0.2f, 0.4f);

						ataqBourke = UnityEngine.Random.Range(0.0001f, 0.0003f);
						iterBourke = 10 * UnityEngine.Random.Range(20, 40);

						//tecnicaGeneradora = new LibNoise.Generator.RidgedMultifractal();
						tecnica = "ridged";
						variabilidad = UnityEngine.Random.Range(1f, 3f);

						crateres = 0;

						perfilTexturas = "marte";

						ConsolidarValores();
					}
				if (GUI.Button(new Rect(250, 170, 135, 20), "Montanas sin lagos"))
					{
						puntosAltura = UnityEngine.Random.Range(3, 5);
						altMin = UnityEngine.Random.Range(0f, 0.1f);
						altMax = UnityEngine.Random.Range(0.4f, 0.7f);
						ataqBourke = UnityEngine.Random.Range(0.0001f, 0.0003f);
						iterBourke = 10 * UnityEngine.Random.Range(30, 40);
						crateres = UnityEngine.Random.Range(0, 2);
						perfilTexturas = "montana";
						//tecnicaGeneradora = new LibNoise.Generator.Perlin();
						tecnica = "perlin";
						variabilidad = UnityEngine.Random.Range(1f, 3f);
						ConsolidarValores();
					}
				if (GUI.Button(new Rect(250, 190, 135, 20), "Montanas con lagos"))
					{
						puntosAltura = UnityEngine.Random.Range(3, 5);
						altMin = UnityEngine.Random.Range(-0.4f, -0.2f);
						altMax = UnityEngine.Random.Range(0.4f, 0.7f);
						ataqBourke = UnityEngine.Random.Range(0.0001f, 0.0002f);
						iterBourke = 10 * UnityEngine.Random.Range(30, 40);
						crateres = UnityEngine.Random.Range(0, 2);
						perfilTexturas = "montana";
						//tecnicaGeneradora = new LibNoise.Generator.Perlin();
						tecnica = "perlin";
						variabilidad = UnityEngine.Random.Range(1f, 3f);
						ConsolidarValores();
					}
				if (GUI.Button(new Rect(250, 210, 135, 20), "Lunar"))
					{
						puntosAltura = UnityEngine.Random.Range(2, 8);
						altMin = UnityEngine.Random.Range(0f, 0.1f);
						altMax = UnityEngine.Random.Range(0.2f, 0.4f);

						ataqBourke = UnityEngine.Random.Range(0.0001f, 0.0005f);
						iterBourke = 10 * UnityEngine.Random.Range(40, 50);

						crateres = 10 * UnityEngine.Random.Range(1, 5);

						perfilTexturas = "lunar";

						//tecnicaGeneradora = new LibNoise.Generator.RidgedMultifractal();
						tecnica = "ridged";
						variabilidad = UnityEngine.Random.Range(1f, 2f);
						pesoNoise = UnityEngine.Random.Range(0.1f, 0.3f);
						/*varMin = UnityEngine.Random.Range(1f, 1.3f);
						varMax = UnityEngine.Random.Range(1.7f, 2f);
						pesoMin = UnityEngine.Random.Range(0.4f, 0.6f);
						pesoMax = UnityEngine.Random.Range(0.8f, 1f);*/
						
						ConsolidarValores();
					}
				if (GUI.Button(new Rect(250, 250, 135, 20), "Antartico"))
					{
						puntosAltura = UnityEngine.Random.Range(1, 2);
						altMin = UnityEngine.Random.Range(-0.2f, 0f);
						altMax = UnityEngine.Random.Range(0.2f, 0.4f);
						
						ataqBourke = 0f;
						iterBourke = -1;
						
						crateres = 0;
						
						perfilTexturas = "antartico";
						
						variabilidad = 5f;
						pesoNoise = UnityEngine.Random.Range(0.3f, 1f);
						//tecnicaGeneradora = new LibNoise.Generator.Voronoi();
						tecnica = "voronoi";						

						ConsolidarValores();
					}
				if (GUI.Button(new Rect(250, 270, 135, 20), "Loco"))
					{
						puntosAltura = UnityEngine.Random.Range(1, 4);
						altMin = UnityEngine.Random.Range(-0.3f, 0f);
						altMax = UnityEngine.Random.Range(0.2f, 0.4f);
						
						ataqBourke = 0f;
						iterBourke = -1;
						
						crateres = 0;
						
						perfilTexturas = "loco";
						
						variabilidad = UnityEngine.Random.Range(1f, 5f);;
						//tecnicaGeneradora = new LibNoise.Generator.RidgedMultifractal();
						tecnica = "ridged";
						
						ConsolidarValores();
					}
	
					// Parametros:
					GUI.Box(new Rect(390, 70, 90, 20), "Ptos de altura: ");
					sPuntosAltura = GUI.TextField(new Rect(480, 70, 40, 20), sPuntosAltura, 4);
	
					GUI.Box(new Rect(390, 90, 90, 20), "Alturas: ");
					GUI.Box(new Rect(480, 90, 40, 20), "min: ");
					sAltMin = GUI.TextField(new Rect(520, 90, 40, 20), sAltMin, 5);
					GUI.Box(new Rect(560, 90, 40, 20), "max: ");
					sAltMax = GUI.TextField(new Rect(600, 90, 40, 20), sAltMax, 5);
	
					GUI.Box(new Rect(390, 110, 90, 20), "Bourke: ");
					GUI.Box(new Rect(480, 110, 80, 20), "Ataque: ");
					sAtaqBourke = GUI.TextField(new Rect(560, 110, 80, 20), sAtaqBourke, 7);
					GUI.Box(new Rect(480, 130, 80, 20), "Iteraciones: ");
					sIterBourke = GUI.TextField(new Rect(560, 130, 80, 20), sIterBourke, 3);


					GUI.Box(new Rect(390, 150, 90, 20), "Peso: ");
					sPesoNoise = GUI.TextField(new Rect(560, 170, 80, 20), sPesoNoise, 5);
					
					GUI.Box(new Rect(390, 190, 90, 20), "Variab.: ");
					sVariabilidad = GUI.TextField(new Rect(560, 210, 80, 20), sVariabilidad, 5);
					
					
					GUI.color = Color.gray;
					GUI.Box(new Rect(480, 150, 40, 20), "min: ");
					sPesoMin = GUI.TextField(new Rect(520, 150, 40, 20), sPesoMin, 5);
					GUI.Box(new Rect(560, 150, 40, 20), "max: ");
					sPesoMax = GUI.TextField(new Rect(600, 150, 40, 20), sPesoMax, 5);
		
					GUI.Box(new Rect(480, 170, 80, 20), "--> ");
					
		
					GUI.Box(new Rect(480, 190, 40, 20), "min: ");
					sVarMin = GUI.TextField(new Rect(520, 190, 40, 20), sVarMin, 5);
					GUI.Box(new Rect(560, 190, 40, 20), "max: ");
					sVarMax = GUI.TextField(new Rect(600, 190, 40, 20), sVarMax, 5);
		
					GUI.Box(new Rect(480, 210, 80, 20), "--> ");
					GUI.color = Color.white;

			
					GUI.Box(new Rect(390, 250, 90, 20), "Crateres");
					sCrateres = GUI.TextField(new Rect(480, 250, 80, 20), sCrateres, 3);

					GUI.Box(new Rect(390, 230, 90, 20), "LibNoise:");
					tecnica = GUI.TextField(new Rect(480, 230, 80, 20), tecnica, 20);
	
	
					if (GUI.Button(new Rect(390, 290, 250, 20), "Generar"))
					{
						// actualizar valores escritos en los TextFields:
						puntosAltura = int.Parse(sPuntosAltura);
						altMin = float.Parse(sAltMin);
						altMax = float.Parse(sAltMax);

						ataqBourke = float.Parse(sAtaqBourke);
						iterBourke = int.Parse(sIterBourke);

						crateres = int.Parse(sCrateres);

						varMin = float.Parse(sVarMin);
						varMax = float.Parse(sVarMax);
						variabilidad = float.Parse(sVariabilidad);

						pesoMin = float.Parse(sPesoMin);
						pesoMax = float.Parse(sPesoMax);
						pesoNoise = float.Parse(sPesoNoise);

					
	
						// resetear terreno:
						try
							{
							for (int y = 0; y < yRes; y++) { for (int x = 0; x < xRes; x++)	{ heights[x,y] = 0f; } }
							tData.SetHeights(0, 0, heights);
							}
						catch (NullReferenceException e)
							{
							Debug.Log("No fue posible resetear el terreno.\n" + e);
							}
						tData.SetHeights(0, 0, heights);
	
						// generar terreno:
						MidPointDisplacementV3 mpdsp3 = new MidPointDisplacementV3(tData);
						tData = mpdsp3.Crear(puntosAltura, 1, altMin, altMax, 10);
	
						// generar crateres:
						CordilleraV2 cordillera = new CordilleraV2(tData);
	
						for (int i = 0; i < crateres; i++)
						{
							float altura0 = UnityEngine.Random.Range(altMin*0.5f, altMax*0.5f);
	
							cordillera.Crear(	/*numPicos*/ 1,
												/*altura*/ altura0,
												/*ataque*/ UnityEngine.Random.Range(0.95f, 0.99f),
												/*hipMin*/ UnityEngine.Random.Range(10, 20),
												/*hipMax*/ UnityEngine.Random.Range(40, 50),
												/*radioInicio*/ UnityEngine.Random.Range(3,15),
												/*radioFin*/ xRes);
						}
	
						/*// capa Bourke num 1
						BourkeV2 bourke = new BourkeV2(tData);
						tData = bourke.Crear(ataqBourke, iterBourke);
	
						// capa Bourke num 2
						tData = bourke.Crear((ataqBourke*10), iterBourke);*/


						// Seleccionar la tecnica generadora de LibNoise a partir del string:
						switch (tecnica)
						{
							case "ridged":
							{
								tecnicaGeneradora = new LibNoise.Generator.RidgedMultifractal(UnityEngine.Random.Range(1.0f,3.0f), UnityEngine.Random.Range(1.0f,3.0f), 6, UnityEngine.Random.Range(0,3), LibNoise.QualityMode.Medium);
								break;
							}
							case "billow":
							{
								tecnicaGeneradora = new LibNoise.Generator.Billow(UnityEngine.Random.Range(0.1f,3.0f), UnityEngine.Random.Range(1.0f,3.0f), 0.5f, 6, UnityEngine.Random.Range(0,3), LibNoise.QualityMode.Medium);
								break;
							}
							case "perlin":
							{
								tecnicaGeneradora = new LibNoise.Generator.Perlin(UnityEngine.Random.Range(0.1f,3.0f), UnityEngine.Random.Range(1.0f,3.0f), 0.5f, 6, UnityEngine.Random.Range(0,3), LibNoise.QualityMode.Medium);
								break;
							}
							case "voronoi":
							{
								tecnicaGeneradora = new LibNoise.Generator.Voronoi(UnityEngine.Random.Range(0.1f,3.0f), UnityEngine.Random.Range(1.0f,3.0f), UnityEngine.Random.Range(0, 3), false);
								break;
							}
							case "spheres":
							default:
							{
								Debug.Log("No se encontro coincidencia con la tecnica generadora propuesta y la biblioteca de LibNoise.\nUsando una rarita por defecto.");
								tecnicaGeneradora = new LibNoise.Generator.Spheres();
								break;
							}
						}


						Debug.Log("Arrancando LibNoise con la tecnica: " + tecnicaGeneradora);
						LibNoise.Noise2D noise2d = new LibNoise.Noise2D(xRes, yRes, (LibNoise.ModuleBase) tecnicaGeneradora);
						noise2d.GeneratePlanar(-variabilidad, variabilidad, -variabilidad, variabilidad, true);
	
						float[,] preHeights = new float[xRes, yRes];
						preHeights = noise2d.GetData(true, 0, 0, true);
	
						heights = tData.GetHeights(0, 0, xRes, yRes);
					
						for (int x = 0; x < xRes; x ++)
						{
							for (int y = 0; y < yRes; y ++)
							{
								heights[x,y] *= (preHeights[x,y] * pesoNoise);
							}
						}
					
						tData.SetHeights(0, 0, heights);
						tecnicaGeneradora = null; noise2d = null; GC.Collect();
	
						Debug.Log("Aplicando texturas para terreno de tipo: " + perfilTexturas);

						Texturizar2 texturizar = new Texturizar2(tData);
						texturizar.Aplicar(perfilTexturas);


						// reseteamos la instancia del objeto Perfiles, porque si no el objeto LibNoise se queda anulado:
						noise2d = null;
						GameObject.Destroy(this.gameObject);
					}
			}

			if (Event.current.Equals(Event.KeyboardEvent(main.teclaMenu)))
			{
				if (mostrarGUI)
				{
					mostrarGUI = false;
				}
				else if (!mostrarGUI)
				{
					mostrarGUI = true;
				}
			}
		}


		// Use this for initialization
		void Start ()
		{
			this.name = "objetoActivo";
			
			// referencia a Main para guardar y recuperar parametros:
			main = GameObject.Find("Terrain").GetComponent<Main>();
			// referencia a Main para acceder al terreno:
			terrain = GameObject.Find("Terrain").GetComponent<Terrain>();
			
			tData = terrain.terrainData;
			xRes = tData.heightmapWidth;
			yRes = tData.heightmapHeight;
			heights = tData.GetHeights(0, 0, xRes, yRes);
			
			contadorUniversal = GameObject.Find("Contador").GetComponent<Contador>();
			contadorUniversal.contador = 0;

			tecnicaGeneradora = null;	

			// Recuperar valores guardados de instancias previas:
			puntosAltura = main.puntosAltura;	sPuntosAltura = puntosAltura.ToString();
			altMin = main.alturaMinima;			sAltMin = altMin.ToString();
			altMax = main.alturaMaxima;			sAltMax = altMax.ToString();
			ataqBourke = main.ataqueBourke;		sAtaqBourke = ataqBourke.ToString();
			iterBourke = main.iterBourke;		sIterBourke = iterBourke.ToString();
			crateres = main.crateres;			sCrateres = crateres.ToString();
			pesoNoise = main.pesoNoise;			sPesoNoise = pesoNoise.ToString();
			variabilidad = main.variabilidad;	sVariabilidad = variabilidad.ToString();
			perfilTexturas = main.perfilTexturas;
			tecnica = main.tecnica;
		}

	
		// Update is called once per frame
		void Update ()
		{

		}


		void ConsolidarValores()
		{
			sPuntosAltura = puntosAltura.ToString();
			sAltMin = altMin.ToString();
			sAltMax = altMax.ToString();

			sAtaqBourke = ataqBourke.ToString();
			sIterBourke = iterBourke.ToString();

			sCrateres = crateres.ToString();

			pesoNoise = UnityEngine.Random.Range(pesoMin, pesoMax);
			sPesoNoise = pesoNoise.ToString();
			
			variabilidad = UnityEngine.Random.Range(varMin, varMax);
			sVariabilidad = variabilidad.ToString();
		

			// Guardar valores en Main:
			main.puntosAltura = puntosAltura;
			main.alturaMinima = altMin;
			main.alturaMaxima = altMax;
			main.ataqueBourke = ataqBourke;
			main.iterBourke = iterBourke;
			main.crateres = crateres;
			main.variabilidad = variabilidad;
			main.pesoNoise = pesoNoise;
			main.perfilTexturas = perfilTexturas;
			main.tecnica = tecnica;
		}
}

