using System;
using UnityEngine;
using System.Collections;

//using GeneradorMundos;

namespace AssemblyCSharp
{
	public class ValleV2
	{
		TerrainData tData;
		float[,] heights;
		int xRes, yRes;
		
		int numValles;
		float profundidadHoyos;
		float ataque;

		int x, y;
		float angulo;
		int excepciones = 0;

		public ValleV2 (TerrainData _tData)
		{
			tData = _tData;
			xRes = tData.heightmapWidth;
			yRes = tData.heightmapHeight;
			heights = tData.GetHeights(0, 0, xRes, yRes);

			// escogemos un punto del mapa al azar:
			x = UnityEngine.Random.Range(50, (xRes-50));
			y = UnityEngine.Random.Range(50, (yRes-50));

			// escogemos un angulo al azar:
			angulo = UnityEngine.Random.Range(1, 180);
		}
		
		
		public TerrainData Crear()
		{
			tData = AlgoritmoValleV2(x, y, angulo, UnityEngine.Random.Range(1,2), UnityEngine.Random.Range(-0.1f,-0.3f), UnityEngine.Random.Range(0.2f,0.7f), 10f, 15f);
			tData.SetHeights(0, 0, heights);
			return tData;
		}
		
		
		public TerrainData Crear(int numHoyos, float profundidadHoyos, float ataqSuavHoyos)
		{
			tData = AlgoritmoValleV2(x, y, angulo, numHoyos, profundidadHoyos, ataqSuavHoyos, 5f, 10f);
			tData.SetHeights(0, 0, heights);
			return tData;
		}
		
		
		
		
		TerrainData AlgoritmoValleV2(int x, int y, float angulo, int numHoyos, float profundidadHoyos, float ataque, float hipMin, float hipMax)
		{
				// algoritmo per se:
				for (int i = 0; i < numHoyos; i++)
				{
					// escoger hipotenusa y lado adyacente aleatorios:
					float hipotenusa = UnityEngine.Random.Range(hipMin, hipMax);

					do
					{
						float adyacente = Mathf.Cos (angulo) * hipotenusa;
						x = x + (int) adyacente;
						float opuesto = Mathf.Sin (angulo) * hipotenusa;
						y = y + (int) opuesto;
	
						try
							{
							if ((heights[x,y] > profundidadHoyos))
								{
								heights[x,y] -= profundidadHoyos * UnityEngine.Random.Range(0.70f, 1f);

								heights = AlgoritmoSuavizarValle(heights, x, y, ataque, angulo);
								}
							}
						catch (IndexOutOfRangeException)
							{
							excepciones ++;
							}
						hipotenusa--;
					}
					while (hipotenusa > 0);

					// escogemos un angulo al azar:
					angulo += UnityEngine.Random.Range(-50f, 50f);
				}

			Debug.Log("Excepciones encontradas: " + excepciones);

			// terminar y devolver el nuevo mapa a main():
			return tData;
			
		}

		float[,] AlgoritmoSuavizarValle(float[,] heights, int x, int y, float ataque, float anguloInicial)
		{
			// metodo de suavizado en el que se ataca al terreno desde el centro hacia afuera en linea recta, para todos los angulos alrededor del centro
			
			int _x = x;
			int _y = y;
			float profundidadInicial = heights[x,y];
			float profundidadActual = profundidadInicial;

			/*for (int ang = 0; ang < 359; ang++)
			{
				alturaActual = alturaInicial;

				for (int radio = 0; radio < 50; radio++)
				{
					x = (int) (_x + radio * Mathf.Cos(ang));
					y = (int) (_y + radio * Mathf.Sin(ang));

					try
					{
						if ((heights[x,y] > alturaActual))
							{
							heights[x,y] = alturaActual;
							alturaActual = alturaActual * ataque * 0.99f;	// el ultimo parametro multiplicador parece que define la inclinacion del extremo de la montaña. Valores bajos crea cerros y mesetas. Valores cercanos a 1 crea montes.
							}
						else { continue; }
					}
					catch (IndexOutOfRangeException)
					{
						excepciones++;
					}
				}
			}*/

			// metodo de suavizado en el que se ataca al terreno en forma de anillos concentricos

			_x = x;
			_y = y;
			profundidadActual = heights[x, y];

			for (int radio = 1; radio < 150; radio++)	// distancia al punto
				{
					profundidadActual = profundidadActual * ataque;
					
					for (int angulo = 0; angulo < 359; angulo++)	// girar alrededor del punto
					{
						x = (int) (_x + radio * Mathf.Cos(angulo));
						y = (int) (_y + radio * Mathf.Sin(angulo));

						try
						{
							if ((heights[x,y] > profundidadActual)) { heights[x,y] -= profundidadActual; }
							else { continue; }

						}
						catch (IndexOutOfRangeException)
						{
							excepciones ++;
						}
					}
				}

			return heights;
		}
	}
}

