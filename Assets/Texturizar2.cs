using UnityEngine;
using System.Collections;
using System.Linq; // used for Sum of array
using AssemblyCSharp;


public class Texturizar2
{

		public Terrain terrain;
		TerrainData terrainData;
		float[,] heights;
		int xRes, yRes;

		Main main;

		int a = 0, b = 1, c = 2, d = 3, e = 4;
		int cte, inclinacion, altura;
	


		public Texturizar2 (TerrainData _tData)
		{
			terrainData = _tData;
			xRes = terrainData.heightmapWidth;
			yRes = terrainData.heightmapHeight;
			heights = terrainData.GetHeights(0, 0, xRes, yRes);

			main = GameObject.Find("Terrain").GetComponent<Main>();
			terrain = GameObject.Find("Terrain").GetComponent<Terrain>();
		}

		public void cambiarPerfilTexturas(string perfil)
		{
		if (string.Equals(perfil, "desierto")) 	{ cte = 0; altura = 2; inclinacion = 3; }
		if (string.Equals(perfil, "campo")) 	{ cte = 5; altura = 6; inclinacion = 7; }
		if (string.Equals(perfil, "pantano")) 	{ cte = 5; altura = 7; inclinacion = 9; }		
		if (string.Equals(perfil, "marte")) 	{ cte = 4; altura = 1; inclinacion = 8; }
		if (string.Equals(perfil, "montana")) 	{ cte = 7; altura = 11; inclinacion = 5; }
		if (string.Equals(perfil, "lunar")) 	{ cte = 11; altura = 12; inclinacion = 0; }
		if (string.Equals(perfil, "antartico"))	{ cte = 11; altura = 12; inclinacion = 12; }
		
		if (string.Equals(perfil, "loco"))		{ cte = Random.Range(0,12); altura = Random.Range(0,12); inclinacion = Random.Range(0,12); }
		}


		public void Aplicar()
		{
			Aplicar("campo");
		}


		public void Aplicar(string perfil)
		{		
			cambiarPerfilTexturas(perfil);

			Terrain terrainTexturas = terrain;
			TerrainData tDataTexturas = terrain.terrainData;
			terrainTexturas.terrainData.heightmapResolution = main.resolucionTerreno;
			terrainTexturas.terrainData.size = new Vector3(2000, 600, 2000);

			TexturaBasadaEnLibNoise:
			{
				float variabilidad = 1f;
				LibNoise.Noise2D noise2d = new LibNoise.Noise2D(xRes, yRes, (LibNoise.ModuleBase) new LibNoise.Generator.RidgedMultifractal());
				noise2d.GeneratePlanar(UnityEngine.Random.Range(-variabilidad, 0f), UnityEngine.Random.Range(0f, variabilidad), UnityEngine.Random.Range(-variabilidad, 0f), UnityEngine.Random.Range(0f, variabilidad), false);
				
				// guardar el mapa de ruido creado con Noise2D en un terreno completo auxiliar:
				float[,] preHeights = new float[xRes, yRes];
				preHeights = noise2d.GetData(true, 0, 0, true);

				// multiplicar el terreno actual con el del noise2d:
				for (int x = 0; x < xRes; x ++)
				{
					for (int y = 0; y < yRes; y ++)
					{
						preHeights[x,y] *= heights[x,y];
					}
				}
	
				// consolidar terreno auxiliar:
				tDataTexturas.SetHeights(0,0, preHeights);
			}
		
			
			float[,] steepness = new float[tDataTexturas.alphamapHeight,tDataTexturas.alphamapWidth];
			//Debug.Log("height: " + tDataTexturas.alphamapHeight + "\t, width: " + tDataTexturas.alphamapWidth);
		          
			// http://alastaira.wordpress.com/2013/11/14/procedural-terrain-splatmapping/						
			
			// Splatmap data is stored internally as a 3d array of floats, so declare a new empty array ready for your custom splatmap data:
			float[, ,] splatmapData = new float[tDataTexturas.alphamapWidth, tDataTexturas.alphamapHeight, tDataTexturas.alphamapLayers];
			
			for (int y = 0; y < tDataTexturas.alphamapHeight; y++)
			{
				for (int x = 0; x < tDataTexturas.alphamapWidth; x++)
				{
					// Normalise x/y coordinates to range 0-1 
					float y_01 = (float) y / (float) tDataTexturas.alphamapHeight;
					float x_01 = (float) x / (float) tDataTexturas.alphamapWidth;
					
					// Sample the height at this location (note GetHeight expects int coordinates corresponding to locations in the heightmap array)
					float height = tDataTexturas.GetHeight(Mathf.RoundToInt(y_01 * tDataTexturas.heightmapHeight),Mathf.RoundToInt(x_01 * tDataTexturas.heightmapWidth) );
					
					// Calculate the normal of the terrain (note this is in normalised coordinates relative to the overall terrain dimensions)
					Vector3 normal = tDataTexturas.GetInterpolatedNormal(y_01,x_01);
					
					// Calculate the steepness of the terrain
					steepness[x,y] = terrainData.GetSteepness(y_01, x_01);

					// Setup an array to record the mix of texture weights at this point
					float[] splatWeights = new float[tDataTexturas.alphamapLayers];
					
					// CHANGE THE RULES BELOW TO SET THE WEIGHTS OF EACH TEXTURE ON WHATEVER RULES YOU WANT
					
					// Textura con influencia constante:
					splatWeights[cte] = Random.Range(0f, 0.1f);

					// Textura dependiente de la inclinacion, con altura normalizada:
					//splatWeights[inclinacion] = 0.5f - Mathf.Clamp01((Mathf.Pow(steepness[x,y], 2))/(tDataTexturas.heightmapHeight));	
					splatWeights[inclinacion] = Mathf.Clamp01(steepness[x,y]) / 10;

					// Textura que aumenta su influencia con la altura, pero solo para el eje Y:
					splatWeights[altura] = height * Random.Range(0.0001f, 0.001f) * Mathf.Clamp01(normal.y);
				
					// Texturas dependientes de la altura:
					splatWeights[a] = 0; //Mathf.Clamp01(((tDataTexturas.heightmapHeight/Random.Range(4f,5f)) - height));
					splatWeights[b] = 0; //Mathf.Clamp01(((tDataTexturas.heightmapHeight/Random.Range(5f,10f)) - height));

					
					// Sum of all textures weights must add to 1, so calculate normalization factor from sum of weights
					float z = splatWeights.Sum();
					
					// Loop through each terrain texture
					for(int i = 0; i < tDataTexturas.alphamapLayers; i++)
					{
						// Normalize so that sum of all texture weights = 1
						splatWeights[i] /= z;
						
						// Assign this point to the splatmap array
						splatmapData[x, y, i] = splatWeights[i];
					}
				}
			}

			// Finally assign the new splatmap to the terrainData:
			terrainData.SetAlphamaps(0, 0, splatmapData);
			terrainData.SetHeights(0,0,heights);
			main.texturizado = true;
	}
	
	
		public void Eliminar()
		{
			float[, ,] splatmapData = new float[terrainData.alphamapWidth, terrainData.alphamapHeight, terrainData.alphamapLayers];

			float[] splatWeights = new float[terrainData.alphamapLayers];
			for (int i = 0; i < (terrainData.alphamapLayers-1); i++)
				{
					splatWeights[i] = 0f;
				}
			splatWeights[(terrainData.alphamapLayers-1)] = 1f;

			for (int x = 0; x < terrainData.alphamapWidth; x++)
			{
				for (int y = 0; y < terrainData.alphamapHeight; y++)
				{
					for (int z = 0; z < terrainData.alphamapLayers; z++)
					{
						splatmapData[x,y,z] = splatWeights[z];
					}
				}
			}
			
			terrainData.SetAlphamaps(0, 0, splatmapData);
			main.texturizado = false;
		}
	
		// Update is called once per frame
		/*void Update ()
		{
	
		}*/
}

