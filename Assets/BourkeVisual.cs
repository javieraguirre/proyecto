using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class BourkeVisual : MonoBehaviour
	{
	//public Terrain terrain;
	Terrain terrain;
	TerrainData tData;
	float[,] heights;
	int xRes, yRes;
	
	public float altura = 1f;			string sAltura;
	public int iterBourke = 75;			string sIter;
	public float ataqueBourke = 0.01f;	string sAtaque;
	public bool generarNuevo = false;	string sNuevo;
	public float incrBourke = 1f;		string sIncr;
	public float decrBourke = 1f;		string sDecr;

	int counter = 0;
	int turno = 0;
	int desvioX, desvioY;
	bool mostrarGUI = true;

	Contador contadorUniversal;
	bool generar = false;

	string tituloGUI = "Generador de terreno basado en la tecnica esferica de Bourke:";

	Main main;


	// Use this for initialization
	void Start ()
	{
		this.name = "objetoActivo";

		terrain = GameObject.Find("Terrain").GetComponent<Terrain>();

		tData = terrain.terrainData;
		xRes = tData.heightmapWidth;
		yRes = tData.heightmapHeight;
		heights = tData.GetHeights(0, 0, xRes, yRes);

		contadorUniversal = GameObject.Find("Contador").GetComponent<Contador>();
		contadorUniversal.contador = 0;

		// referencia a Main para guardar y recuperar parametros:
		main = GameObject.Find("Terrain").GetComponent<Main>();
		// recuperar valores memorizados en main:
		iterBourke = main.iterBourke;
		altura = main.alturaInicialBourke;
		ataqueBourke = main.ataqueBourke;
		generarNuevo = main.generarNuevoBourke;
		incrBourke = main.incrBourke;
		decrBourke = main.decrBourke;

		sIter = iterBourke.ToString();
		sAltura = altura.ToString();
		sAtaque = ataqueBourke.ToString();
		sIncr = incrBourke.ToString();
		sDecr = decrBourke.ToString();
		if (generarNuevo) { sNuevo = "1"; } else { sNuevo = "0"; }

	}
	
	// Update is called once per frame
	void Update ()
	{
		if (generar)
		{
			if(generarNuevo && (counter == 0))
			{
				for (int y = 0; y < yRes; y++)
				{
					for (int x = 0; x < xRes; x++)
					{
						heights[x,y] = altura; //0.5f;
					}
				}
			}

			if (counter < iterBourke)
			//for (int i = 0; i < iterBourke; i++)
			{
				desvioX = UnityEngine.Random.Range(1, 3);
				//desvioY = UnityEngine.Random.Range(1, 3);
				

				// elegir punto aleatorio:
				int posX = UnityEngine.Random.Range(0, xRes);
				int posY = UnityEngine.Random.Range(0, yRes);
				
				// elegir direccion aleatoria:
				/*int dirX = UnityEngine.Random.Range(-1, 1); if (dirX == 0) { dirX = 1; }
				int dirY = UnityEngine.Random.Range(-1, 1); if (dirY == 0) { dirY = 1; }*/
				int dirX = UnityEngine.Random.Range(-1, 2); //if (dirX == 0) { dirX = 1; }
				int dirY = UnityEngine.Random.Range(-1, 2); //if (dirY == 0) { dirY = 1; }
				
				// Pasada en angulo recto:
				//if (turno == 0)
				{						
					for (int x = 0; x < xRes; x++)
					{
						for (int y = 0; y < yRes; y++)
						{
							if ((Mathf.Sign(x - posX) == dirX) && (Mathf.Sign(y - posY) == dirY)) { heights[x,y] += ataqueBourke * 0.25f; }
							else { heights[x,y] -= ataqueBourke * 0.25f; }
						}
					}
				}

				//else if (turno == 1)
				{
					// Pasada en oblicuo hacia arriba:
					{
						int y; int _posY = posY;				
						for (int x = posX; x < xRes; x = x + desvioX)
						{
							y = 0;
							while (y < _posY)
							{
								heights[x,y] += ataqueBourke * dirX * incrBourke;
								y++;
							}
							while (y < yRes)
							{
								heights[x,y] -= ataqueBourke * dirX * decrBourke;
								y++;
							}
							if (_posY < yRes) { _posY++; }
						}
					}
				}

				//else if (turno == 2)
				{
					// Pasada en oblicuo hacia abajo:
					{
						int y; int _posY = posY;			
						for (int x = (posX-1); x >= 0; x = x - desvioX)
						{
							y = 0;
							while (y < _posY)
							{
								heights[x,y] += ataqueBourke * dirY * incrBourke;
								y++;
							}
							while (y < yRes)
							{
								heights[x,y] -= ataqueBourke * dirY * decrBourke;
								y++;
							}
							if (_posY > 0) { _posY--; }
						}
					}
				}

				//else if (turno == 3)
				{
					// Pasada en oblicuo al otro lado hacia arriba:
					{
						int y; int _posY = posY;				
						for (int x = posX; x < xRes; x = x + desvioX)
						{
							y = 0;
							while (y < _posY)
							{
								heights[x,y] += ataqueBourke * dirX * incrBourke;
								y++;
							}
							while (y < yRes)
							{
								heights[x,y] -= ataqueBourke * dirX * decrBourke;
								y++;
							}
							if (_posY > 0) { _posY--; }
						}
					}
				}

				//else if (turno == 4)
				{
					// Pasada en oblicuo al otro lado hacia abajo:
					{
						int y; int _posY = posY;			
						for (int x = (posX-1); x >= 0; x = x - desvioX)
						{
							y = 0;
							while (y < _posY)
							{
								heights[x,y] += ataqueBourke * dirY * incrBourke;
								y++;
							}
							while (y < yRes)
							{
								heights[x,y] -= ataqueBourke * dirY * decrBourke;
								y++;
							}
							if (_posY < yRes) { _posY++; }
						}
					}
				}

			tData.SetHeights(0, 0, heights);
			}

			else
			{
				// memorizar en main los valores elegidos por el usuario, antes de destruir el objeto:
				main.ataqueBourke = ataqueBourke;
				main.iterBourke = iterBourke;
				main.generarNuevoBourke = generarNuevo;
				main.ataqueBourke = ataqueBourke;
				main.decrBourke = decrBourke;
				main.incrBourke = incrBourke;

				Object.Destroy(this.gameObject);
				/*this.name = "objetoFinalizado";
				generar = false;
				counter = 0;*/
			}
			/*turno++;
			if (turno > 5) { turno = 0; }*/

			counter++;
			contadorUniversal.contador = (int) (((double) counter / (double) iterBourke) * 100);
			//Debug.Log((int) (((double) counter / (double) iterBourke) * 100));
		}
	}

	void OnGUI()
	{
		if (mostrarGUI)
		{
			GUI.Box(new Rect(250, 10, /*400*/ tituloGUI.Length*6+20, 20), tituloGUI);
			
			GUI.Box(new Rect(250, 40, 180, 20), "Nº iteraciones: ");
			sIter = GUI.TextField(new Rect(430, 40, 40, 20), sIter, 3);
	
			GUI.Box(new Rect(250, 60, 180, 20), "Altura inicial: ");
			sAltura = GUI.TextField(new Rect(430, 60, 40, 20), sAltura, 5);
	
			GUI.Box(new Rect(470, 60, 40, 20), "x /\\: ");
			sIncr = GUI.TextField(new Rect(510, 60, 30, 20), sIncr, 3);
			GUI.Box(new Rect(540, 60, 40, 20), "x \\/: ");
			sDecr = GUI.TextField(new Rect(590, 60, 30, 20), sDecr, 3);
	
			GUI.Box(new Rect(250, 80, 180, 20), "Ataque: ");
			sAtaque = GUI.TextField(new Rect(430, 80, 40, 20), sAtaque, 5);
	
			GUI.Box(new Rect(250, 100, 180, 20), "Descartar terreno previo: ");
			sNuevo = GUI.TextField(new Rect(430, 100, 40, 20), sNuevo, 1);
			
			if (!generar)
			{
				if (GUI.Button(new Rect(250, 120, 120, 20), "Generar"))
					{
						iterBourke = int.Parse(sIter);
						altura = float.Parse(sAltura);
						ataqueBourke = float.Parse(sAtaque);
						incrBourke = float.Parse(sIncr);
						decrBourke = float.Parse(sDecr);
						if ( (string.Equals(sNuevo, "0")) || (string.Equals(sNuevo, "f")) || (string.Equals(sNuevo, "F")) || (string.Equals(sNuevo, "n")) || (string.Equals(sNuevo, "N")) ) { generarNuevo = false; }
						if ( (string.Equals(sNuevo, "1")) || (string.Equals(sNuevo, "t")) || (string.Equals(sNuevo, "T")) || (string.Equals(sNuevo, "v")) || (string.Equals(sNuevo, "V")) || (string.Equals(sNuevo, "s")) || (string.Equals(sNuevo, "S"))) { generarNuevo = true; }
						generar = true;
					}
			}
			else if (generar)
			{
				GUI.color = Color.yellow;
				GUI.Box(new Rect(250, 120, 120, 20), "Generando..." + contadorUniversal.contador + " %");
				GUI.color = Color.white;
			}
		}

		if (Event.current.Equals(Event.KeyboardEvent("m")))
		{
			if (mostrarGUI)
			{
				mostrarGUI = false;
				// GameObject.Find("objetoActivo") == true
				//controlRaton.enabled = true;
			}
			else if (!mostrarGUI)
			{
				mostrarGUI = true;
				//controlRaton.enabled = false;
			}
		}
	}
}
