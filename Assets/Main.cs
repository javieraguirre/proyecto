﻿/*

Proyecto de generador procedural de terreno para el motor de juegos Unity3D.

Javier Aguirre Moreno
Master en Sistemas Telematicos e Informaticos
Universidad Rey Juan Carlos
curso 2014-2015

Incluye tecnicas descritas y desarrolladas por distintos autores.


Enlaces desde donde empezar a inspirarse:

http://nbickford.wordpress.com/2012/12/21/creating-fake-landscapes/
http://www.gameprogrammer.com/fractal.html
https://www.google.es/search?q=The+Definition+and+Rendering+of+Terrain+Maps.+SIGGRAPH+pdf&bav=on.2,or.r_qf.&cad=b&ech=1&psi=BnaNVIn5A9XYapfwgLAD.1418556934693.3&ei=BnaNVIn5A9XYapfwgLAD&emsg=NCSR&noj=1
http://paulbourke.net/fractals/noise/
http://nbickford.wordpress.com/2012/12/21/creating-fake-landscapes/
http://en.wikipedia.org/wiki/Fractal_landscape
https://alastaira.wordpress.com/2013/11/14/procedural-terrain-splatmapping/
http://http.developer.nvidia.com/GPUGems3/gpugems3_ch01.html
http://www.lighthouse3d.com/opengl/terrain/index.php3?fault
*/


using UnityEngine;
using System.Collections;
using AssemblyCSharp;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;


namespace AssemblyCSharp
{
	
	public class Main : MonoBehaviour
	{
		// objetos o "assets" que participan en el proyecto:

		public Terrain terrain;
		public Terrain terrainAgua;

		BourkeVisualV2 bourkeVisual;
		MidPointDisplacementV3visual mpd3visual;
		MidPointDisplacementV3 mpd3;
		CordilleraVisual cordilleraVisual;

		MouseLook controlRaton;
		GameObject controlPersonaje;
		Contador contadorUniversal;
		TexturizarV3 texturizar;
		Resetear resetear;
		Erosion erosion;
		Perfiles perfiles;


		// valores propios del terreno principal:
		TerrainData tData;
		static int xRes;
		static int yRes;
		static float[,] heights;
		
		
		// valores comunes para todas las tecnicas:
		public float alturaTerrenoInicial = 0;
		int alturaBotones = 20;
		int multAltBotones = 1;
		public string objetoActivo = "";
		bool mostrarGUI = false;
		public int velocidad = 1;

		bool volar = false;
		public int resolucionTerreno = 513;


		// valores publicos para la tecnica Midpoint Displacement:
		public bool generarNuevoMDP = true;
		public float alturaInicialMDP = 0f;
		public int puntosAltura = 4;
		public int profundidad = 10;
		public float multiplicadorAltura = 1f;
		public float alturaMinima = -0.3f;
		public float alturaMaxima = 0.3f;


		// valores publicos para la tecnica de Bourke:
		public bool generarNuevoBourke = false;
		public float alturaInicialBourke = 0f;
		public int iterBourke = 500;
		public float ataqueBourke = 0.001f;
		public float decrBourke = 1f;
		public float incrBourke = 3f;
		public int dependenciaInclinacion = 0;
		public float potenciaInclinacion = 1f;
		public float alturaUmbral = 0.15f;
		public float alturaCota = 0.6f;


		// valores publicos para LibNoise:
		public float variabilidad = 1f;
		public float pesoNoise = 1f;
		public string tecnica = "spheres";
		

		// valores publicos para los generadores de orografia:
		public int radioInicio = 0;
		public int radioFinal = 150;
		public float hipotenusaMin = 10f;
		public float hipotenusaMax = 15f;

		// montañas:
		public int numCordilleras = 1;
		public int numPicos = 30;
		public float alturaPicos = 0.1f;
		public float ataqSuavPicos = 0.85f;
		public float ataqPicos = 0.85f, ataqCrat = 0.85f;
		public float hipMin = 4, hipMax = 8;
		public int radMin = 1, radMax = 150, radCrat = 0;
		public int crateres;
		
		
		// valles:
		public int numValles = 1;
		public int numHoyos = 30;
		public float profHoyos = 0.1f;
		public float ataqSuavHoyos = 0.85f;


		// rugosidad:
		public float rugosidadSuelo = 0.01f;
		
		// suavizador:
		public int pasadasSuavFinal = 1;
		public float ataqSuavFinal = 0.1f;

		// mar:
		public float nivelMar = 0.1f;
		public float multiplicadorMarea = 0.01f;

		// texturas:
		public bool texturizado = false;
		public string perfilTexturas = "campo";

		Rect tamanoVentana = new Rect((Screen.width/4), (Screen.height/4), /*(Screen.width / 2)*/440, /*(Screen.height / 2)+20*/260);
		bool mostrarAcercaDe = true;
		public string teclaMenu = "Tab";
		
		int a = 0;
		
		
		public void iniciarTerreno()
		{
			tData = terrain.terrainData;
			xRes = tData.heightmapWidth;
			yRes = tData.heightmapHeight;
			heights = tData.GetHeights(0, 0, xRes, yRes);

			float oldRes = (int) terrain.terrainData.heightmapResolution;

			texturizar = new TexturizarV3(tData);
			texturizar.Eliminar();


			terrain.heightmapPixelError = 1;

			terrain.terrainData.heightmapResolution = resolucionTerreno;
			terrain.terrainData.size = new Vector3(2000, 600, 2000);
			//terrain.terrainData.alphamapHeight = resolucionTerreno - 1;
			//terrain.terrainData.alphamapWidth = resolucionTerreno - 1;
			terrain.terrainData.alphamapResolution = resolucionTerreno - 1;
			terrain.terrainData.baseMapResolution = resolucionTerreno - 1;
			//terrain.terrainData.detailWidth = resolucionTerreno - 1;
			//terrain.terrainData.detailHeight = resolucionTerreno - 1;
			//terrain.terrainData.detailResolution = resolucionTerreno - 1;

			terrainAgua.terrainData.heightmapResolution = 33;
			terrainAgua.terrainData.size = new Vector3(2000, 100, 2000);


			float factor = (oldRes - 1) / (resolucionTerreno - 1); //Debug.Log("oldRes: " + oldRes + "\tfactor: " + factor + "\tnewRes: " + resolucionTerreno);
			int excepciones = 0;

			float[,] newHeights = new float[resolucionTerreno, resolucionTerreno];

			float _factor = 0;
			if (resolucionTerreno > oldRes) { _factor = 1/factor; }
			if (resolucionTerreno < oldRes) { _factor = factor; }

			if (resolucionTerreno != oldRes)
			{
				for (float f = 0; f < (((oldRes-1) * (_factor)) + 1); f = f + factor)
				{
					for (float h = 0; h < (((oldRes-1) * (_factor)) + 1); h = h + factor)
					{
						try { newHeights[(int) (f*(1/factor)),(int) (h*(1/factor))] = heights[(int) f,(int) h]; } catch (System.IndexOutOfRangeException) { excepciones++; }
					}
				}
			}

			tData.SetHeights(0, 0, newHeights);

			tData = terrain.terrainData;
			xRes = tData.heightmapWidth;
			yRes = tData.heightmapHeight;
			//Debug.Log("Excepciones al cambiar la resolucion: " + excepciones);
			heights = tData.GetHeights(0, 0, xRes, yRes);

			//Screen.SetResolution(800,600,false);
			Debug.Log("Resolucion: " + Screen.width + " x " + Screen.height);
		}		


		// Use this for initialization
		void Start ()
			{
			// Buscar y asignar automaticamente los assets de Unity:

			contadorUniversal = GameObject.Find("Contador").GetComponent<Contador>();
			controlRaton = GameObject.Find("First Person Controller").GetComponent<MouseLook>();
			controlPersonaje = GameObject.Find("First Person Controller");

			terrain = GameObject.Find("Terrain").GetComponent<Terrain>();
			terrainAgua = GameObject.Find("TerrainAgua").GetComponent<Terrain>();

			// asignar los valores del terreno de inicio:
			iniciarTerreno();


			Debug.Log("alphamapHeight: " + tData.alphamapHeight + "\nalphamapWidth: " + tData.alphamapWidth
					+ "\nalphamapResolution: " + tData.alphamapResolution + "\nbasemapResolution: " + tData.baseMapResolution
					+ "\ndetailHeight: " + tData.detailHeight + "\ndetailResolution: " + tData.detailResolution
					+ "\ndetailWidth: " + tData.detailWidth + "\nheightmapHeight: " + tData.heightmapHeight
					+ "\nheightmapResolution: " + tData.heightmapResolution + "\nheightmapWidth: " + tData.heightmapWidth);

			Debug.Log("heightmapScale: " + tData.heightmapScale + "\nsize Vector3(x, y, z): " + tData.size);
			}

		// Update is called once per frame
		void Update ()
			{
			// simular marea:
			//terrainAgua.transform.position = new Vector3(-1000, nivelMar + (Mathf.Sin(Time.time) * multiplicadorMarea), -1000);
			TerrainData aguaData = terrainAgua.terrainData;
			float[,] heightsAgua = aguaData.GetHeights(0,0,33,33);
			for (int ax = 0; ax < 33; ax++)
			{
				for (int ay = 0; ay < 33; ay++)
				{
					heightsAgua[ax,ay] = (Mathf.Sin(Time.time) * multiplicadorMarea) + nivelMar;
				}
			}
			aguaData.SetHeights(0,0,heightsAgua);

			// Instanciar o destruir objetos:

			/* Tenemos un String llamado objetoActivo, en cuyo interior hay un valor que es igual al nombre del objeto que queremos tener instanciado.
			El objeto que se instancia se renombra a "objetoActivo". */

			if ( (string.Equals(objetoActivo, "bourkevisual") == true) && (GameObject.Find("objetoActivo") == null) ) { Instantiate(Resources.Load("BourkeVisualV2", typeof(BourkeVisualV2)) as BourkeVisualV2); }
			if ( (string.Equals(objetoActivo, "mpd3visual") == true) && (GameObject.Find("objetoActivo") == null) ) { Instantiate(Resources.Load("MPD3Visual", typeof(MidPointDisplacementV3visual)) as MidPointDisplacementV3visual); }
			if ( (string.Equals(objetoActivo, "resetear") == true) && (GameObject.Find("objetoActivo") == null) ) { Instantiate(Resources.Load("Resetear", typeof(Resetear)) as Resetear); }
			if ( (string.Equals(objetoActivo, "erosion") == true) && (GameObject.Find("objetoActivo") == null) ) { Instantiate(Resources.Load("Erosion", typeof(Erosion)) as Erosion); }
			if ( (string.Equals(objetoActivo, "cargadescarga") == true) && (GameObject.Find("objetoActivo") == null) ) { Instantiate(Resources.Load("CargaDescarga", typeof(CargaDescarga)) as CargaDescarga); }
			if ( (string.Equals(objetoActivo, "guardar") == true) && (GameObject.Find("objetoActivo") == null) ) { Instantiate(Resources.Load("Guardar", typeof(Guardar)) as Guardar); }
			if ( (string.Equals(objetoActivo, "cordilleraVisual") == true) && (GameObject.Find("objetoActivo") == null) ) { Instantiate(Resources.Load("CordilleraVisual", typeof(CordilleraVisual)) as CordilleraVisual); }
			if ( (string.Equals(objetoActivo, "perfiles") == true) && (GameObject.Find("objetoActivo") == null) ) { Instantiate(Resources.Load("Perfiles", typeof(Perfiles)) as Perfiles); }			
			if ( (string.Equals(objetoActivo, "libnoise") == true) && (GameObject.Find("objetoActivo") == null) ) { Instantiate(Resources.Load("InterfazLibNoise", typeof(InterfazLibNoise)) as InterfazLibNoise); }			
			if ( (string.Equals(objetoActivo, "ciudades") == true) && (GameObject.Find("objetoActivo") == null) ) { Instantiate(Resources.Load("Ciudades", typeof(Ciudades)) as Ciudades); }			
			if ( (string.Equals(objetoActivo, "secuenciadorLv2") == true) && (GameObject.Find("objetoActivo") == null) ) { Instantiate(Resources.Load("SecuenciadorLv2", typeof(SecuenciadorLv2)) as SecuenciadorLv2); }			
			
			
			if ( (string.Equals(objetoActivo, "") == true) && (GameObject.Find("objetoActivo") == true) ) { UnityEngine.Object.Destroy(GameObject.Find("objetoActivo")); }


			/*if (event1.Length > 0) { Debug.Log(event1); }
			event1 = "";*/
		}
		
		
		void OnGUI ()
		{
			GUI.skin.box.fontSize = 12;
			GUI.skin.label.fontSize = 12;
			GUI.skin.button.fontSize = 12;
			GUI.skin.textField.fontSize = 12;

			/*GUI.color = Color.yellow;
			GUI.Box(new Rect(10, Screen.height - 50, Screen.width - 20, 20), "Generador de terreno de Javier Aguirre Moreno, TFM MSTI URJC 2014-2015. Compilado el " + RetrieveLinkerTimestamp().ToString());
			GUI.color = Color.white;
			GUI.Box(new Rect(10, Screen.height - 30, Screen.width - 20, 20), "Pulse la tecla \",\" (coma simple, al ladito de la M) para esconder o descubrir el GUI");*/

			// Iniciar objetos:

			CordilleraV2 cordilleraV2;
			Rugosidad rugosidad;
			Voss voss;


			// artilugio para permitir al jugador volar:

			if (Event.current.Equals(Event.KeyboardEvent("LeftControl")) && (Event.current.Equals(Event.KeyboardEvent("f"))))
			{
				if (!volar) { volar = true; }
				else { volar = false; }
			}
			
			if (volar)
			{
				controlPersonaje.transform.Translate(new Vector3(0, 10f, 0) * Time.deltaTime);
			}


			if (mostrarGUI)
			{
				multAltBotones = 1;
				a = alturaBotones*multAltBotones;

				GUI.color = Color.yellow;
				GUI.Box(new Rect(10, a,220,20), "Generadores de terreno base"); // izda, dcha, ancho, alto
				GUI.color = Color.white;

				multAltBotones ++; a = alturaBotones*multAltBotones;


				/// Cambiar resolucion del terreno:
				GUI.color = Color.yellow;
				GUI.Box(new Rect(10, a, 100, 20), "Detalle terreno");
				GUI.color = Color.white;
				if (GUI.Button(new Rect(110, a, 30, 20), "<") && resolucionTerreno > 129) { resolucionTerreno--; resolucionTerreno /= 2; resolucionTerreno ++; iniciarTerreno(); }
				GUI.Box(new Rect(140, a, 60, 20), resolucionTerreno.ToString());
				if (GUI.Button(new Rect(200, a, 30, 20), ">") && resolucionTerreno < 1025) { resolucionTerreno--; resolucionTerreno *= 2; resolucionTerreno ++; iniciarTerreno(); }
				
				multAltBotones ++; a = alturaBotones*multAltBotones;
				

				// MPD visual:
				GUI.color = Color.yellow;
				GUI.Box(new Rect(10,a,100,20), "MPD");
				GUI.color = Color.white;

				if (string.Equals(objetoActivo, "mpd3visual") == true) { GUI.color = Color.yellow; }
				if(GUI.Button(new Rect(110,a,60,20), "Visual"))
				{
					Object.Destroy(GameObject.Find("objetoActivo"));
					
					if (string.Equals(objetoActivo, "mpd3visual") == false) { objetoActivo = "mpd3visual"; }
					else { objetoActivo = ""; }
				}
				if (string.Equals(objetoActivo, "mpd3visual") == true) { GUI.color = Color.white; }
				
				
				// MPD rapido:
				if(GUI.Button(new Rect(170,a,60,20), "Rapido"))
				{
					Object.Destroy(GameObject.Find("objetoActivo"));
					
					MidPointDisplacementV3 mpdsp3 = new MidPointDisplacementV3(tData);
					tData = mpdsp3.Crear(puntosAltura, multiplicadorAltura, alturaMinima, alturaMaxima, profundidad);
				}

				multAltBotones ++; a = alturaBotones*multAltBotones;



				// Bourke visual:
				GUI.color = Color.yellow;
				GUI.Box(new Rect(10,a,100,20), "Bourke");
				GUI.color = Color.white;

				if (string.Equals(objetoActivo, "bourkevisual") == true) { GUI.color = Color.yellow; }
				if(GUI.Button(new Rect(110,a,60,20), "Visual"))
				{
					Object.Destroy(GameObject.Find("objetoActivo"));
					
					if (string.Equals(objetoActivo, "bourkevisual") == false) { objetoActivo = "bourkevisual"; }
					else { objetoActivo = ""; }
				}
				if (string.Equals(objetoActivo, "bourkevisual") == true) { GUI.color = Color.white; }


				// Bourke rapido:
				if(GUI.Button(new Rect(170,a,60,20), "Rapido"))
				{
					Object.Destroy(GameObject.Find("objetoActivo"));
					
					BourkeV2 b2 = new BourkeV2(tData);
					tData = b2.Crear(ataqueBourke, iterBourke);
				}

				multAltBotones ++; a = alturaBotones*multAltBotones;



				// Interfaz con LibNoise:
				GUI.color = Color.yellow;
				GUI.Box(new Rect(10,a,100,20), "LibNoise");
				GUI.color = Color.white;

				if (string.Equals(objetoActivo, "libnoise") == true) { GUI.color = Color.yellow; }
				if(GUI.Button(new Rect(110,a,120,20), "LibNoise"))
				{
					Object.Destroy(GameObject.Find("objetoActivo"));
					
					if (string.Equals(objetoActivo, "libnoise") == false) { objetoActivo = "libnoise"; }
					else { objetoActivo = ""; }
				}
				if (string.Equals(objetoActivo, "libnoise") == true) { GUI.color = Color.white; }
				
				multAltBotones += 2; a = alturaBotones*multAltBotones;
				



				GUI.color = Color.yellow;
				GUI.Box(new Rect(10,a,220,20), "Generadores de terreno adicional");
				GUI.color = Color.white;
				
				multAltBotones ++; a = alturaBotones*multAltBotones;
				

				
				
				// Generador de montañas:
				GUI.color = Color.yellow;
				GUI.Box(new Rect(10,a,100,20), "Cordilleras");
				GUI.color = Color.white;
				
				if (string.Equals(objetoActivo, "cordilleraVisual")) { GUI.color = Color.yellow; }
				if(GUI.Button(new Rect(110,a,60,20), "Visual"))
				{
					Object.Destroy(GameObject.Find("objetoActivo"));
	
					for (int i = 0; i < numCordilleras; i++)
					{
						if (string.Equals(objetoActivo, "cordilleraVisual") == false) { objetoActivo = "cordilleraVisual"; }
						else { objetoActivo = ""; }
					}
				}
				GUI.color = Color.white;
				
				
				if(GUI.Button(new Rect(170,a,60,20), "Rapido"))
				{
					Object.Destroy(GameObject.Find("objetoActivo"));
					
					for (int i = 0; i < numCordilleras; i++)
					{
						cordilleraV2 = new AssemblyCSharp.CordilleraV2(tData);
						tData = cordilleraV2.Crear(numPicos, alturaPicos, ataqSuavPicos, hipotenusaMin, hipotenusaMax, radioInicio, radioFinal);
					}
				}

				multAltBotones += 2; a = alturaBotones*multAltBotones;
				

				GUI.color = Color.yellow;
				GUI.Box(new Rect(10,a,220,20), "Modificadores de terreno");
				GUI.color = Color.white;

				multAltBotones ++; a = alturaBotones*multAltBotones;
				
				

				// Generadores de ruidos:

				GUI.color = Color.yellow;
				GUI.Box(new Rect(10,a,100,20), "Ruido");
				GUI.color = Color.white;
				
				if(GUI.Button(new Rect(170,a,60,20), "Voss"))
				{
					Object.Destroy(GameObject.Find("objetoActivo"));
					
					voss = new Voss(tData);
					voss.Crear();
				}
				
				if(GUI.Button(new Rect(110,a,60,20), "Simple"))
				{
					Object.Destroy(GameObject.Find("objetoActivo"));

					rugosidad = new AssemblyCSharp.Rugosidad(tData);
					tData = rugosidad.Crear(rugosidadSuelo);
				}

				multAltBotones ++; a = alturaBotones*multAltBotones;
				



				// Generador de ciudades:
				
				GUI.color = Color.yellow;
				GUI.Box(new Rect(10,a,100,20), "Ciudades");
				GUI.color = Color.white;
	
				if (string.Equals(objetoActivo, "ciudades") == true) { GUI.color = Color.yellow; }
				if(GUI.Button(new Rect(110,a,120,20), "Bourke"))
				{
					Object.Destroy(GameObject.Find("objetoActivo"));
					
					if (string.Equals(objetoActivo, "ciudades") == false) { objetoActivo = "ciudades"; }
					else { objetoActivo = ""; }
				}
				if (string.Equals(objetoActivo, "ciudades") == true) { GUI.color = Color.white; }

				multAltBotones ++; a = alturaBotones*multAltBotones;
				


				// Erosionador:
				if (GameObject.Find("TerrainAgua") != null)
				{
					GUI.color = Color.yellow;
					GUI.Box(new Rect(10,a,100,20), "Erosionar");
					GUI.color = Color.white;
	
					if (string.Equals(objetoActivo, "erosion") == true) { GUI.color = Color.yellow; }
					if(GUI.Button(new Rect(110,a,60,20), "Simple"))
					{
						Object.Destroy(GameObject.Find("objetoActivo"));
						
						if (string.Equals(objetoActivo, "erosion") == false) { objetoActivo = "erosion"; }
						else { objetoActivo = ""; }
					}
				}
				else
				{
					GUI.color = Color.gray;
					GUI.Box(new Rect(10,a,100,20), "Erosionar");
					GUI.Box(new Rect(110,a,120,20), "No hay TerrainAgua");
					GUI.color = Color.white;
				}

				multAltBotones ++; a = alturaBotones*multAltBotones;
				


				// Texturizador:

				GUI.color = Color.yellow;
				GUI.Box(new Rect(10,a,100,20), "Texturas");
				GUI.color = Color.white;

				if (texturizado) { GUI.color = Color.yellow; }
				if(GUI.Button(new Rect(110,a,120,20), "Texturizar"))
				{
					if (!texturizado)
						{
							texturizar = new TexturizarV3(tData);
							texturizar.Aplicar(perfilTexturas);
						}
					else { texturizar.Eliminar(); }
				}
				GUI.color = Color.white;

				multAltBotones ++; a = alturaBotones*multAltBotones;



				// Reseteador del terreno:

				if (string.Equals(objetoActivo, "resetear") == true) { GUI.color = Color.yellow; }
				if (GUI.Button(new Rect(10,a,100,20), "Resetear"))
				{
					Object.Destroy(GameObject.Find("objetoActivo"));
					
					if (string.Equals(objetoActivo, "resetear") == false) { objetoActivo = "resetear"; }
					else { objetoActivo = ""; }
				}
				GUI.color = Color.white;

				if (string.Equals(objetoActivo, "resetear") == true)
				{
					if(GUI.Button(new Rect(110,a,60,20), "rapido"))
					{
						resetear = GameObject.Find("objetoActivo").GetComponent<Resetear>();
						resetear.ResetRapido();
						
					}
					if(GUI.Button(new Rect(170,a,60,20), "visual"))
					{
						resetear = GameObject.Find("objetoActivo").GetComponent<Resetear>();
						resetear.ResetVisual();
					}
				}

				multAltBotones += 2; a = alturaBotones*multAltBotones;
				


				// Generadores de terreno automaticos:
				
				GUI.color = Color.yellow;
				GUI.Box(new Rect(10,a,220,20), "Generador de terreno automatico: ");
				GUI.color = Color.white;

				multAltBotones ++; a = alturaBotones*multAltBotones;
				
				
				// Perfiles:
				if (string.Equals(objetoActivo, "perfiles") == true) { GUI.color = Color.yellow; }
				if (GUI.Button(new Rect(10,a,110,20), "Perfiles"))
				{
					Object.Destroy(GameObject.Find("objetoActivo"));
					
					if (string.Equals(objetoActivo, "perfiles") == false) { objetoActivo = "perfiles"; }
					else { objetoActivo = ""; }
					
				}
				GUI.color = Color.white;


				// Secuenciador L-System:
				if (string.Equals(objetoActivo, "secuenciadorLv2") == true) { GUI.color = Color.yellow; }
				if (GUI.Button(new Rect(120,a,110,20), "Secuenciador L"))
				{
					Object.Destroy(GameObject.Find("objetoActivo"));
					
					if (string.Equals(objetoActivo, "secuenciadorLv2") == false) { objetoActivo = "secuenciadorLv2"; }
					else { objetoActivo = ""; }
					
				}
				GUI.color = Color.white;

				multAltBotones += 2; a = alturaBotones*multAltBotones;
				



				// Cargar y guardar terrenos:

				GUI.color = Color.yellow;
				GUI.Box(new Rect(10,a,220,20), "Gestion de archivos: ");
				GUI.color = Color.white;

				multAltBotones ++; a = alturaBotones*multAltBotones;
				

				// Cargar:
				if (string.Equals(objetoActivo, "cargadescarga") == true) { GUI.color = Color.yellow; }
				if (GUI.Button(new Rect(10,a,110,20), "Cargar"))
				{
					Object.Destroy(GameObject.Find("objetoActivo"));
					
					if (string.Equals(objetoActivo, "cargadescarga") == false) { objetoActivo = "cargadescarga"; }
					else { objetoActivo = ""; }

				}
				GUI.color = Color.white;

				// Guardar:
				if (string.Equals(objetoActivo, "guardar") == true) { GUI.color = Color.yellow; }
				if (GUI.Button(new Rect(120,a,110,20), "Guardar"))
				{					
					Object.Destroy(GameObject.Find("objetoActivo"));
					
					if (string.Equals(objetoActivo, "guardar") == false) { objetoActivo = "guardar"; }
					else { objetoActivo = ""; }
					
				}
				GUI.color = Color.white;

				multAltBotones += 2; a = alturaBotones*multAltBotones;
				



				// Acerca del programa:
				
				GUI.color = Color.yellow;
				GUI.Box(new Rect(10,a,110,20), "Ayuda: ");
				GUI.color = Color.white;
				
				// Acerca de:
				if (mostrarAcercaDe == true) { GUI.color = Color.yellow; }
				if (GUI.Button(new Rect(120,a,110,20), "Acerca de"))
				{
					if (!mostrarAcercaDe)
						{
						mostrarAcercaDe = true;
						GUI.Window(0, tamanoVentana, ShowGUI, "Acerca de este programa ...");
						}
					else { mostrarAcercaDe = false; }
					
				}
				GUI.color = Color.white;



			}

			if (/*(Time.time < 20) && */(mostrarAcercaDe)) { GUI.Window(0, tamanoVentana, ShowGUI, "Acerca de este programa ..."); }


			// Mostrar menu:

			if (Event.current.Equals(Event.KeyboardEvent(teclaMenu)))
 			//if (Input.GetKey(KeyCode.LeftControl))
				{
				if (mostrarGUI)
						{
						mostrarGUI = false;
						
						controlRaton.enabled = true;
				}
				else if (!mostrarGUI)
						{
						mostrarGUI = true;
						controlRaton.enabled = false;
						mostrarAcercaDe = false;
						}
				}

				/*Event e = Event.current;
				{
					string tmpstr = "" + e.keyCode;
					if (tmpstr == "None")
					{
						return;
					}
					event1 = tmpstr;
				}*/
		}
		
		void ShowGUI(int windowID)
		{
			GUI.Label(new Rect(20, 30, 240, (tamanoVentana.height - 20)), "Generador procedural de terreno\n\nJavier Aguirre Moreno\nTrabajo Fin de Master, curso 2014-2015\nMaster Sistemas Telematicos e Informaticos\nUniversidad Rey Juan Carlos");

			GUI.color = Color.yellow;
			GUI.Label(new Rect(20, 130, (tamanoVentana.width - 80), (tamanoVentana.height - 20)), "Para ocultar y descubir el menu principal, pulse Tabulador.\n\nUse el raton para enfocar el terreno.");
			GUI.color = Color.white;

			GUI.Label(new Rect(20, 200, (tamanoVentana.width - 40), (tamanoVentana.height - 20)), "Fecha compilacion: " + RetrieveLinkerTimestamp().ToString());
			//GUI.TextField(new Rect(130, 230, 50, 20), (20 - Time.time).ToString());		
			if (GUI.Button(new Rect(20, 230, 100, 20), "Que si, que vale."))
			{
				mostrarAcercaDe = false;
			}
		}

		// Codigo para conseguir el numero de compilacion en C# por un tal Joe Spivey, publicado en StackOverflow: http://stackoverflow.com/questions/1600962/displaying-the-build-date
		private System.DateTime RetrieveLinkerTimestamp()
		{
			string filePath = System.Reflection.Assembly.GetCallingAssembly().Location;
			const int c_PeHeaderOffset = 60;
			const int c_LinkerTimestampOffset = 8;
			byte[] b = new byte[2048];
			System.IO.Stream s = null;
			
			try
			{
				s = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
				s.Read(b, 0, 2048);
			}
			finally
			{
				if (s != null)
				{
					s.Close();
				}
			}
			
			int i = System.BitConverter.ToInt32(b, c_PeHeaderOffset);
			int secondsSince1970 = System.BitConverter.ToInt32(b, i + c_LinkerTimestampOffset);
			System.DateTime dt = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
			dt = dt.AddSeconds(secondsSince1970);
			dt = dt.ToLocalTime();
			return dt;
		}
		

			
			
		/* Codigo para calcular la FFT:
		http://www.codeproject.com/Articles/9388/How-to-implement-the-FFT-algorithm
		*/
		/*
		//data -> float array that represent the array of complex samples
		//number_of_complex_samples -> number of samples (N^2 order number) 
		//isign -> 1 to calculate FFT and -1 to calculate Reverse FFT
		float FFT (float[] data, int number_of_complex_samples, int isign)
		{
	
			//variables for trigonometric recurrences
			int istep, j;
			float wtemp, wr, wpr, wpi, wi, theta, tempr, tempi;
			
			//the complex array is real+complex so the array 
			//as a size n = 2* number of complex samples
			// real part is the data[index] and the complex part is the data[index+1]
			int n = number_of_complex_samples * 2;
	
			
			//Danielson-Lanzcos routine:
			
			int mmax = 2;
			
			//external loop
			while (n > mmax)
			{
				istep = mmax << 1;
				theta = Mathf.Sign((2f * 3.1415f) / mmax);
				wtemp = Mathf.Sin(0.5f * theta);
				wpr = -2.0f * wtemp * wtemp;
				wpi = Mathf.Sin(theta);
				wr = 1.0f;
				wi = 0.0f;
				
				//internal loops
				for (int m = 1; m < mmax; m += 2)
				{
					for (int i = m; i <= n; i += istep)
					{
						j = i + mmax;
						tempr = wr * data[j-1] - wi * data[j];
						tempi = wr * data[j] + wi * data[j-1];
						data[j-1] = data[i-1] - tempr;
						data[j] = data[i] - tempi;
						data[i-1] += tempr;
						data[i] += tempi;
					}
					
					wr = (wtemp = wr) * wpr - wi * wpi + wr;
					wi = wi * wpr + wtemp * wpi + wi;
				}
				mmax = istep;
			}
		}*/
		
		
		
	}
}