using System;
using UnityEngine;
using System.Collections;

//using GeneradorMundos;

namespace AssemblyCSharp
{
		public class MidPointDisplacementV3
		{

				// Valores generales:
		
				TerrainData tData;
				float[,] heights;
				int xRes, yRes;			
		
				int puntosAltura;// = 8;
				int _puntosAltura;
				int longitudPasos;// = 0;
				int numPasadas;// = 0;
				int profundidad;
				float multiplicadorAltura;
				float _multiplicadorAltura;
				
				float alturaInicial;
				float alturaMinima;
				float alturaMaxima;

				bool[,] marcas;

				Main main = GameObject.Find("Terrain").GetComponent<Main>();
		


				// Constructor:
				public MidPointDisplacementV3 (TerrainData _tData)
				{
						tData = _tData;
						xRes = tData.heightmapWidth;
						yRes = tData.heightmapHeight;
						heights = tData.GetHeights(0, 0, xRes, yRes);
						marcas = new bool[xRes, yRes];

						alturaInicial = main.alturaInicialMDP;
				}


				public TerrainData Crear(int puntosAltura, float multiplicadorAltura, float alturaMinima, float alturaMaxima, int profundidad)
				{
						tData = AlgoritmoMPDisp(puntosAltura, multiplicadorAltura, alturaMinima, alturaMaxima, profundidad);
						return tData;
				}


				public TerrainData Crear()
				{
						tData = AlgoritmoMPDisp(UnityEngine.Random.Range(8, 64), 1f /*UnityEngine.Random.Range(0.5f, 2f)*/, UnityEngine.Random.Range(0.2f, 0.5f), UnityEngine.Random.Range(0.8f, 1f), 10);
						return tData;
				}


				/*public TerrainData Crear_L()
				{
					tData = AlgoritmoMPD_L();
					return tData;
				}*/


				public float[,] heightsMap
				{
					get { return heights; }
				}


				// Algoritmo adaptado al L-System:
				public TerrainData AlgoritmoMPD_L(int x, int xTotal, int puntoAltura, int puntosAltura, float multiplicadorAltura, float alturaMinima, float alturaMaxima)
				{
					//numPasadas = (int) Mathf.Log(puntosAltura, 2); // 2 elevado a puntosAltura
					try { longitudPasos = ((xRes-1) / (xTotal - 1)); }
					catch(IndexOutOfRangeException) { Debug.Log("Division entre 0 en AlgoritmoMPD_L en la operacion: longitudPasos = (xRes-1) / (xTotal-1)"); }

					x *= longitudPasos; // x es la posicion en el eje X en donde esta la linea del eje Y en donde se crearan los puntos de altura.

					// Crear puntos de altura:
					Debug.Log ("pA: " + puntoAltura + "\tpsA: " + puntosAltura + "\tx: " + x + "\tpA/psA-1 * yRes-1: " + ((int) (((float) puntoAltura / (float) (puntosAltura -1)) * (yRes-1))));

					heights[x, ((int) (((float) puntoAltura / (float) (puntosAltura -1)) * (yRes-1)))] += UnityEngine.Random.Range(alturaMinima, alturaMaxima) * multiplicadorAltura;
					marcas[x, ((int) (((float) puntoAltura / (float) (puntosAltura -1)) * (yRes-1)))] = true;
						
					heights = rellenadoHorizontal(heights, x);
						
					//_multiplicadorAltura = _multiplicadorAltura * 0.3f;
					//_puntosAltura = _puntosAltura * 2;
					
					heights = rellenadoVertical(heights);

					
					tData.SetHeights(0, 0, heights);
					
					return tData;
				}


				public TerrainData rellenadoH_L(int x)
				{
					heights = rellenadoHorizontal(heights, x);
					
					//_multiplicadorAltura = _multiplicadorAltura * 0.3f;
					//_puntosAltura = _puntosAltura * 2;
					
					tData.SetHeights(0,0, heights);

					return tData;
				}

				public TerrainData rellenadoV_L()
				{
					heights = rellenadoVertical(heights);
					tData.SetHeights(0,0, heights);
					return tData;
				}




				// Algoritmo:
				TerrainData AlgoritmoMPDisp(int puntosAltura, float multiplicadorAltura, float alturaMinima, float alturaMaxima, int profundidad)
				{

						/*
						Tecnica de Fournier, fussel y Carpenter en el SIGGRAPH de 1982
						
						http://www.gameprogrammer.com/fractal.html
						http://en.wikipedia.org/wiki/Diamond-square_algorithm
						*/
						
						/*if (profundidad > 20)
						{
								Debug.Log("Parametro 'profundidad' demasiado alto. Se queda en profundidad = 20.");
								profundidad = 20;
						}*/


						numPasadas = (int) Mathf.Log(puntosAltura, 2); // 2 elevado a puntosAltura
						longitudPasos = ((xRes-1) / puntosAltura);
						
						//Debug.Log("puntos de altura: " + puntosAltura + "\tlongitud de pasos: " + longitudPasos);
						// esta sentencia probablemente este de mas, ya que en el constructor ya rellenamos heights:
						//float[,] heights = tData.GetHeights(0, 0, xRes, yRes);
						
						
						// Reiniciar la altura del terreno a 0:
						
						if (main.generarNuevoMDP)
						{
							for (int x = 0; x < xRes; x = x + longitudPasos)
							{
									for (int y = 0; y < yRes; y++)
									{
											heights[x,y] = alturaInicial;
									}
							}
						}
						
						// Paso 1: crear puntos de altura:
						
						for (int x = 0; x < xRes; x = x + longitudPasos)
						{
								_multiplicadorAltura = multiplicadorAltura;
								_puntosAltura = puntosAltura;		
	
	
								//for (int i = 0; i < profundidad; i++)
								{
										for (int y = 0; y < (_puntosAltura+1); y++)
										{
												//Debug.Log("x: " + x + "; profundidad: " + i + "; puntosAltura: " + _puntosAltura + "; y: " + y + "; yRes: " + yRes + "; valorFinal: " + (int) (((float) y / (float) _puntosAltura) * (yRes-1)));
												heights[x, ((int) (((float) y / (float) _puntosAltura) * (yRes-1)))] += UnityEngine.Random.Range(alturaMinima, alturaMaxima) * _multiplicadorAltura;
												marcas[x, ((int) (((float) y / (float) _puntosAltura) * (yRes-1)))] = true;
										}
			
										heights = rellenadoHorizontal(heights, x);
							
										_multiplicadorAltura = _multiplicadorAltura * 0.3f;
										_puntosAltura = _puntosAltura * 2;
								}
	
								heights = rellenadoVertical(heights);
						}
						
						tData.SetHeights(0, 0, heights);


						if (main.texturizado)
						{
							Texturizar2 texturizar = new Texturizar2(tData);
							texturizar.Aplicar("campo");
						}

						return tData;
				}


				public float[,] rellenadoHorizontal(float[,] heights, int x)
				{
						//for (int x = 0; x < xRes; x = x + longitudPasos)
						{
								int cursor = 0;
								
								for (int y = 0; y < yRes; y++)
								{
										if (marcas[x,y]) //(heights[x,y] != 0.5f) //(heights[x,y] > 0f)
										{							
												if (heights[x,cursor] < heights[x,y])
												{
														float B = Mathf.Atan2((heights[x,y] - heights[x,cursor]), y - cursor);
														//heights[x,y] = heights[x,cursor] + Mathf.Tan(B) * (y - cursor);
														for (int _y = cursor; _y < y; _y++)
														{
																heights[x,_y] = heights[x,cursor] + Mathf.Tan(B) * (_y - cursor);
														}
												}
												
												if (heights[x,cursor] > heights[x,y])
												{
														float B = -1 * Mathf.Atan2((heights[x,cursor] - heights[x,y]), y - cursor);
														//heights[x,y] = heights[x,cursor] + (Mathf.Tan(B) * (y - cursor));
														for (int _y = cursor; _y < y; _y++)
														{
																heights[x,_y] = heights[x,cursor] + (Mathf.Tan(B) * (_y - cursor));
														}
												}
												cursor = y;
										}
								}
						}
				return heights;
				}


				public float[,] rellenadoVertical(float[,] heights)
				{
						for (int y = 0; y < yRes; y++)
						{
								int cursor = 0;
							
								for (int x = 0; x < xRes; x = x + longitudPasos)
								{
										//if (heights[x,y] > 0f)
										{
												if (heights[cursor,y] < heights[x,y]) // (heights[x,y] > heights[cursor,y])
												{
														float B = Mathf.Atan2((heights[x,y] - heights[cursor,y]), x - cursor);
														
														for (int _x = cursor; _x < x; _x++)
														{
																heights[_x,y] = heights[cursor,y] + Mathf.Tan(B) * (_x - cursor);
														}
												}
										
												if (heights[cursor,y] > heights[x,y]) // (heights[x,y] < heights[cursor,y])
												{
														float B = -1 * Mathf.Atan2((heights[cursor,y] - heights[x,y]), x - cursor);
														
														for (int _x = cursor; _x < x; _x++)
														{
																heights[_x,y] = heights[cursor,y] + (Mathf.Tan(B) * (_x - cursor));
														}
												}
										
												cursor = x;
										}
								}
						}
				return heights;
				}
	
		}
}


