using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;
using AssemblyCSharp;


public class CargaDescarga : MonoBehaviour
{

		int alturaBotones = 20;
		string[] s;
		int indiceArchivos;
		bool mostrarGUI = true;

		IFormatter formatter;
		Main main;

		float[,] heights;
		Terrain terrain;
		Terrain terrainAgua;
		TerrainData tData;
		int xRes, yRes;


		// Use this for initialization
		void Start ()
		{
			//Debug.Log("Modulo de Carga y Descarga cargado");
			this.name = "objetoActivo";

			terrain = GameObject.Find("Terrain").GetComponent<Terrain>();
			terrainAgua = GameObject.Find("TerrainAgua").GetComponent<Terrain>();
			main = GameObject.Find("Terrain").GetComponent<Main>();
			
			tData = terrain.terrainData;
			xRes = tData.heightmapWidth;
			yRes = tData.heightmapHeight;

			s = System.IO.Directory.GetFiles(System.Environment.CurrentDirectory, "*.terreno");
		
			formatter = new BinaryFormatter();
		
		}

	
		// Update is called once per frame
		void Update ()
		{
	
		}


		void OnGUI ()
		{
			if (mostrarGUI)
			{

			GUI.color = Color.yellow;
			GUI.Box(new Rect(250,10+(alturaBotones*0),220,20), "Asistente de carga:");
			GUI.color = Color.white;

			/*if (cargar) { GUI.color = Color.yellow; }
			if (GUI.Button(new Rect(250, 10 + (alturaBotones*2), 120, 20), "Cargar"))
			{
				s = System.IO.Directory.GetFiles(System.Environment.CurrentDirectory, "Terreno?.bin");

				cargar = true;
				/* foreach (string archivo in s)	{ Debug.Log(Path.GetFileName(archivo));	}*/

				/* for (int i = 0; i < s.Length; i ++) { Debug.Log(Path.GetFileName(s[i]));	}

				/*string[] files = Directory.GetFiles(dir);
				foreach(string file in files)
				Console.WriteLine(Path.GetFileName(file));*/

				/*for (int indiceArchivos = 0; indiceArchivos < 10; indiceArchivos ++)
				{
					if (System.IO.File.Exists("Terreno" + indiceArchivos + ".terreno"))
					{
						//do stuff
					}
				}*/
			/*}
			GUI.color = Color.white;*/


			if (s != null)
			{
				for (int i = 0; i < s.Length; i ++)
				{
					if (s[i] != null)
					{
						// listar archivo:
						GUI.Box(new Rect(250, 50 + (alturaBotones*i), 220, 20), Path.GetFileName(s[i]));

						// cargar archivo:
						if (GUI.Button(new Rect(470, 50 + (alturaBotones*i), 30, 20), "Ok"))
						{

							IFormatter formatter = new BinaryFormatter();
							//Stream stream = new FileStream(System.Environment.CurrentDirectory + "\\" + "Terreno" + i + ".bin", FileMode.Open, FileAccess.Read, FileShare.Read);
							Stream stream = new FileStream(s[i], FileMode.Open, FileAccess.Read, FileShare.Read);
							heights = (float[,]) formatter.Deserialize(stream);
							stream.Close();
						
							terrain.terrainData.heightmapResolution = heights.GetLength(0);
							terrainAgua.terrainData.heightmapResolution = heights.GetLength(0);
							//main.resolucionTerreno = heights.GetLength(0).ToString();
							main.resolucionTerreno = heights.GetLength(0);

							main.iniciarTerreno();

							tData.SetHeights(0, 0, heights);
							main.objetoActivo = "";
						}

						// borrar archivo:
						if (GUI.Button(new Rect(500, 50 + (alturaBotones*i), 30, 20), "X"))
						{
							//System.IO.File.Delete(System.Environment.CurrentDirectory + "\\" + Path.GetFileName(s[i]));
							System.IO.File.Delete(s[i]);
							s[i] = null;
							main.objetoActivo = "";
						}
					}
				}
			}

			}
			
			if (Event.current.Equals(Event.KeyboardEvent(main.teclaMenu)))
			{
				if (mostrarGUI)
				{
					mostrarGUI = false;
					// GameObject.Find("objetoActivo") == true
					//controlRaton.enabled = true;
				}
				else if (!mostrarGUI)
				{
					mostrarGUI = true;
					//controlRaton.enabled = false;
				}
			}
		}
}

