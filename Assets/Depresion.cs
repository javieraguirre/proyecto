using System;
using UnityEngine;
using System.Collections;

//using GeneradorMundos;


namespace AssemblyCSharp
{
		public class Depresion
		{
				// Valores generales:
		
				TerrainData tData;
				float[,] heights;
				int xRes, yRes, x, y;
		
				AssemblyCSharp.Suavizado suavizado;

				int numHoyos;
				int espaciadoHoyos;
				int despLatHoyos;
				int pasadasSuavHoyos;
				float profHoyos;
				float ataqSuavHoyos;
				float gradoAtenDepr;
				
		
				// Constructor:
				public Depresion (TerrainData _tData)
				{
					tData = _tData;
					xRes = tData.heightmapWidth;
					yRes = tData.heightmapHeight;
					heights = tData.GetHeights(0, 0, xRes, yRes);
				}
		

				public TerrainData Crear()
				{
					// escogemos un punto del mapa al azar:
					int x = UnityEngine.Random.Range(50, (xRes-50));
					int y = UnityEngine.Random.Range(50, (yRes-50));

					tData = AlgoritmoDepresion(x, y, UnityEngine.Random.Range(30, 300), UnityEngine.Random.Range(1, 3), UnityEngine.Random.Range(1, 3), UnityEngine.Random.Range(0.1f, 0.3f), 1, UnityEngine.Random.Range(0.5f, 0.9f), UnityEngine.Random.Range(0.01f, 0.1f));
			        return tData;
				}

			    public TerrainData Crear(int numHoyos, int espaciadoHoyos, int despLatHoyos, float profHoyos, int pasadasSuavHoyos, float ataqSuavHoyos, float gradoAtenDepr)
				{
					// escogemos un punto del mapa al azar:
					int x = UnityEngine.Random.Range(50, (xRes-50));
					int y = UnityEngine.Random.Range(50, (yRes-50));

					tData = AlgoritmoDepresion(x, y, numHoyos, espaciadoHoyos, despLatHoyos, profHoyos, pasadasSuavHoyos, ataqSuavHoyos, gradoAtenDepr);
					return tData;
				}

				public TerrainData Crear(int x, int y) // Esta sobrecarga solo la estamos usando para crear lagos despues de rios.
				{
					tData = AlgoritmoDepresion(x, y, UnityEngine.Random.Range(30, 80), 1, 1, UnityEngine.Random.Range(0.0001f, 0.001f), 1, UnityEngine.Random.Range(0.5f, 0.9f), UnityEngine.Random.Range(0.1f, 0.5f));
					return tData;
				}


		
				TerrainData AlgoritmoDepresion(int x, int y, int numHoyos, int espaciadoHoyos, int despLatHoyos, float profHoyos, int pasadasSuavHoyos, float ataqSuavHoyos, float gradoAtenDepr)
				{
			
					suavizado = new AssemblyCSharp.Suavizado(tData);
					
					// escogemos un punto del mapa al azar:
					/*int x = UnityEngine.Random.Range(50, (xRes-50));
					int y = UnityEngine.Random.Range(50, (yRes-50));*/
					
					//escogemos una direccion en donde hacer crecer la cordillera:
					int _x = UnityEngine.Random.Range(-1, 1);
					int _y = UnityEngine.Random.Range(-1, 1);
					if ((_x == 0) && (_y == 0)) { _x = 1; } // Evitar que todo sean 0s.
			
					
					for (int i=0; i<numHoyos; i++)
					{
						x += UnityEngine.Random.Range(1, espaciadoHoyos) * _x + UnityEngine.Random.Range(0, despLatHoyos);
						y += UnityEngine.Random.Range(1, espaciadoHoyos) * _y + UnityEngine.Random.Range(0, despLatHoyos);
						
						if ((x < xRes) && (x > 0) && (y < yRes) && (y > 0))
						{
							heights[x, y] -= UnityEngine.Random.Range(0, profHoyos);
							heights = suavizado.SuavizarHoyo(heights, x, y, (ataqSuavHoyos * (-1)), gradoAtenDepr, pasadasSuavHoyos);
						}	
					}
	
					tData.SetHeights(0, 0, heights);
					return tData;
				}
				
		}
}

