using UnityEngine;
using System.Collections;
using AssemblyCSharp;


public class Ciudades : MonoBehaviour
{
		Terrain terrain;
		TerrainData tData;
		float[,] heights;
		int xRes, yRes;
		
		public float alturaInicial;				string sAltura;
		public int iterBourke;					string sIter;
		public float ataqueBourke;				string sAtaque;
		public bool generarNuevoBourke = false;	string sNuevo;
		public float incrBourke;				string sIncr;
		public float decrBourke;				string sDecr;
		public int dependenciaInclinacion;		string sDependenciaInclinacion;
		float potenciaInclinacion = 1f;			string sPotenciaInclinacion;
		public float alturaUmbral = 0f;			string sAlturaUmbral;
		public float alturaCota;				string sAlturaCota;
		public int velocidad = 1;				string sVelocidad;
		
		int x, y, dir_x, dir_y, counter = 0;
		bool mostrarGUI = true;
		
		Contador contadorUniversal;
		bool generar = false;
		int generarLento = 0;					string sGenLento;
		
		string tituloGUI = "Generador de ciudades basado en Bourke:";
		
		Main main;
	

		// Use this for initialization
		void Start ()
		{
			this.name = "objetoActivo";
			
			terrain = GameObject.Find("Terrain").GetComponent<Terrain>();
			
			tData = terrain.terrainData;
			xRes = tData.heightmapWidth;
			yRes = tData.heightmapHeight;
			heights = tData.GetHeights(0, 0, xRes, yRes);
			
			contadorUniversal = GameObject.Find("Contador").GetComponent<Contador>();
			contadorUniversal.contador = 0;
			
			// referencia a Main para guardar y recuperar parametros:
			main = GameObject.Find("Terrain").GetComponent<Main>();
			// recuperar valores memorizados en main:
			iterBourke = main.iterBourke;
			alturaInicial = main.alturaInicialBourke;
			ataqueBourke = main.ataqueBourke;
			generarNuevoBourke = main.generarNuevoBourke;
			incrBourke = main.incrBourke;
			decrBourke = main.decrBourke;
			dependenciaInclinacion = main.dependenciaInclinacion;
			potenciaInclinacion = main.potenciaInclinacion;
			alturaUmbral = main.alturaUmbral;
			alturaCota = main.alturaCota;
			velocidad = main.velocidad;
			
			sIter = iterBourke.ToString();
			sAltura = alturaInicial.ToString();
			sAtaque = ataqueBourke.ToString();
			sIncr = incrBourke.ToString();
			sDecr = decrBourke.ToString();
			sDependenciaInclinacion = dependenciaInclinacion.ToString();
			sPotenciaInclinacion = potenciaInclinacion.ToString();
			sAlturaUmbral = alturaUmbral.ToString();
			sAlturaCota = alturaCota.ToString();
			sVelocidad = velocidad.ToString();
			if (generarNuevoBourke) { sNuevo = "1"; } else { sNuevo = "0"; }
			sGenLento = generarLento.ToString();
			
		}
	
		// Update is called once per frame
		void Update ()
		{
			for(int i = 0; i < velocidad; i++)
			{
				if (generar)
				{
					if(generarNuevoBourke && (counter == 0))
					{
						for (y = 0; y < yRes; y++)
						{
							for (x = 0; x < xRes; x++)
							{
								heights[x,y] = alturaInicial; //0.5f;
							}
						}
					}
					

					// Algoritmo:

					// primera instancia de noise2d:
					LibNoise.Noise2D noise2d = new LibNoise.Noise2D(xRes, yRes, (LibNoise.ModuleBase) new LibNoise.Generator.Checker());
					noise2d.GeneratePlanar(-15f, 15f, -15f, 15f, false);
					
					float[,] preHeights1 = new float[xRes, yRes];
					preHeights1 = noise2d.GetData(true, 0, 0, true);


					// segunda instancia de noise2d:
					noise2d = new LibNoise.Noise2D(xRes, yRes, (LibNoise.ModuleBase) new LibNoise.Generator.Voronoi());
					noise2d.GeneratePlanar(-5f, 5f, -5f, 5f, true);

					float[,] preHeights2 = new float[xRes, yRes];
					preHeights2 = noise2d.GetData(true, 0, 0, true);


					// tercera instancia de noise3d:
					/*noise2d = new LibNoise.Noise2D(xRes, yRes, (LibNoise.ModuleBase) new LibNoise.Generator.Voronoi());
					noise2d.GeneratePlanar(-10f, 10f, -10f, 10f, true);
					
					float[,] preHeights3 = new float[xRes, yRes];
					preHeights3 = noise2d.GetData(true, 0, 0, true);*/
				
				
					BourkeV2 bourke = new BourkeV2(tData);
					//bourke.Crear(0.01f, 100, true);

					float[,] preHeights3 = new float[xRes, yRes];
					//for (x = 0; x < xRes; x ++) { for (y = 0; y < yRes; y ++) { preHeights3[x,y] = bourke.heightsMap[x,y]; } }
					preHeights3 = bourke.heightsMap;
					bourke = null;


					// recuperar otra vez el control del Terrain principal:
					tData = terrain.terrainData;
					xRes = tData.heightmapWidth;
					yRes = tData.heightmapHeight;
					heights = tData.GetHeights(0, 0, xRes, yRes);



					for (x = 0; x < xRes; x ++)
					{
						for (y = 0; y < yRes; y ++)
						{
							if ((heights[x, y] >= 0.1f) && (heights[x, y] <= 0.3f))
								{
								heights[x, y] =	0.1f + (((
													(preHeights1[x, y] * 0.3f) +
													(preHeights2[x, y] * 1.5f) +
													(preHeights3[x, y] * 1.5f)
													) * 0.3f));
								}
						}
					}



					
					tData.SetHeights(0, 0, heights);
					

					// memorizar en main los valores elegidos por el usuario, antes de destruir el objeto:
					main.ataqueBourke = ataqueBourke;
					main.iterBourke = iterBourke;
					main.generarNuevoBourke = generarNuevoBourke;
					main.ataqueBourke = ataqueBourke;
					main.decrBourke = decrBourke;
					main.incrBourke = incrBourke;
					main.alturaTerrenoInicial = alturaInicial;
					main.dependenciaInclinacion = dependenciaInclinacion;
					main.alturaUmbral = alturaUmbral;
					main.alturaCota = alturaCota;
					main.velocidad = velocidad;
					
					Object.Destroy(this.gameObject);
				}
			}
			
		}


	void OnGUI()
	{
		if (mostrarGUI)
		{
			int m = 20;
			
			GUI.Box(new Rect(250, 10, tituloGUI.Length*6+20, 25), tituloGUI);
			
			GUI.Box(new Rect(250, m*2, 140, m), "Nº iteraciones: ");
			sIter = GUI.TextField(new Rect(390, m*2, 40, m), sIter, 3);
			GUI.Box(new Rect(430, m*2, 140, m), "Velocidad: ");
			sVelocidad = GUI.TextField(new Rect(570, m*2, 40, m), sVelocidad, 3);
			
			GUI.Box(new Rect(250, m*3, 180, m), "Altura inicial: ");
			sAltura = GUI.TextField(new Rect(430, m*3, 40, m), sAltura, 5);
			GUI.Box(new Rect(470, m*3, 40, m), "x /\\: ");
			sIncr = GUI.TextField(new Rect(510, m*3, 30, m), sIncr, 3);
			GUI.Box(new Rect(540, m*3, 40, m), "x \\/: ");
			sDecr = GUI.TextField(new Rect(580, m*3, 30, m), sDecr, 3);
			
			GUI.Box(new Rect(250, m*4, 180, m), "Ataque: ");
			sAtaque = GUI.TextField(new Rect(430, m*4, 50, m), sAtaque, 7);
			
			GUI.Box(new Rect(250, m*5, 140, m), "Umbral altura: ");
			sAlturaUmbral = GUI.TextField(new Rect(390, m*5, 40, m), sAlturaUmbral, 7);
			GUI.Box(new Rect(430, m*5, 140, m), "Cota altura: ");
			sAlturaCota = GUI.TextField(new Rect(570, m*5, 40, m), sAlturaCota, 7);
			
			GUI.Box(new Rect(250, m*6, 180, m), "Dependencia inclinacion: ");
			sDependenciaInclinacion = GUI.TextField(new Rect(430, m*6, 40, m), sDependenciaInclinacion, 7);
			GUI.Box(new Rect(470, m*6, 80, m), "y = x \u005E n: ");
			sPotenciaInclinacion = GUI.TextField(new Rect(550, m*6, 40, m), sPotenciaInclinacion, 3);
			
			GUI.Box(new Rect(250, m*7, 180, m), "Descartar terreno previo: ");
			sNuevo = GUI.TextField(new Rect(430, m*7, 40, m), sNuevo, 1);
			
			GUI.Box(new Rect(250, m*8, 180, m), "Mostrar progreso: ");
			sGenLento = GUI.TextField(new Rect(430, m*8, 40, m), sGenLento, 1);
			
			if (!generar)
			{
				if (GUI.Button(new Rect(250, m*9, 120, m), "Generar"))
				{
					iterBourke = int.Parse(sIter);
					alturaInicial = float.Parse(sAltura);
					ataqueBourke = float.Parse(sAtaque);
					incrBourke = float.Parse(sIncr);
					decrBourke = float.Parse(sDecr);
					if ( (string.Equals(sNuevo, "0")) || (string.Equals(sNuevo, "f")) || (string.Equals(sNuevo, "F")) || (string.Equals(sNuevo, "n")) || (string.Equals(sNuevo, "N")) ) { generarNuevoBourke = false; }
					if ( (string.Equals(sNuevo, "1")) || (string.Equals(sNuevo, "t")) || (string.Equals(sNuevo, "T")) || (string.Equals(sNuevo, "v")) || (string.Equals(sNuevo, "V")) || (string.Equals(sNuevo, "s")) || (string.Equals(sNuevo, "S"))) { generarNuevoBourke = true; }
					generar = true;
					dependenciaInclinacion = int.Parse(sDependenciaInclinacion);
					potenciaInclinacion = float.Parse(sPotenciaInclinacion);
					alturaUmbral = float.Parse(sAlturaUmbral);
					alturaCota = float.Parse(sAlturaCota);
					velocidad = int.Parse(sVelocidad);
					generarLento = int.Parse(sGenLento);
				}
			}
			else if (generar)
			{
				//if ((generarLento == 1))
				{
					GUI.color = Color.yellow;
					GUI.Box(new Rect(250, m*9, 120, m), "Generando..." + contadorUniversal.contador + " %");
					GUI.color = Color.white;
				}
				/*else if ((generarLento == 0))
				{
					generar = false;
					BourkeV2 bourke = new BourkeV2(tData);
					tData = bourke.Crear(ataqueBourke, iterBourke);
					//tData.SetHeights(0,0,heights);
				}
				else
				{
					Debug.Log("Que numero has puesto en el campo 'Generar lentamente'?? Solo vale 0 o 1");
				}*/
			}
		}
		
		if (Event.current.Equals(Event.KeyboardEvent(main.teclaMenu)))
		{
			if (mostrarGUI)
			{
				mostrarGUI = false;
				// GameObject.Find("objetoActivo") == true
				//controlRaton.enabled = true;
			}
			else if (!mostrarGUI)
			{
				mostrarGUI = true;
				//controlRaton.enabled = false;
			}
		}
	}
	
}

