using UnityEngine;
using System.Collections;
using System.Linq; // used for Sum of array
using AssemblyCSharp;

public class Texturizar : MonoBehaviour
{

		public Terrain terrain;
		TerrainData terrainData;
		float[,] heights;
		int xRes, yRes;

		Main main;


		public Texturizar (TerrainData _tData/*, Texture2D[] _TerrainTextures*/)
		{
			terrainData = _tData;
			xRes = terrainData.heightmapWidth;
			yRes = terrainData.heightmapHeight;
			heights = terrainData.GetHeights(0, 0, xRes, yRes);

			main = GameObject.Find("Terrain").GetComponent<Main>();
			terrain = GameObject.Find("Terrain").GetComponent<Terrain>();
		}

		// Use this for initialization
		//void Start ()
		public void Aplicar()
		{
			// Debug.Log(terrainData.alphamapLayers);
			
			// http://alastaira.wordpress.com/2013/11/14/procedural-terrain-splatmapping/						
			
			// Splatmap data is stored internally as a 3d array of floats, so declare a new empty array ready for your custom splatmap data:
			float[, ,] splatmapData = new float[terrainData.alphamapWidth, terrainData.alphamapHeight, terrainData.alphamapLayers];
			
			for (int y = 0; y < terrainData.alphamapHeight; y++)
			{
				for (int x = 0; x < terrainData.alphamapWidth; x++)
				{
					// Normalise x/y coordinates to range 0-1 
					float y_01 = (float) y / (float) terrainData.alphamapHeight;
					float x_01 = (float) x / (float) terrainData.alphamapWidth;
					
					// Sample the height at this location (note GetHeight expects int coordinates corresponding to locations in the heightmap array)
					float height = terrainData.GetHeight(Mathf.RoundToInt(y_01 * terrainData.heightmapHeight),Mathf.RoundToInt(x_01 * terrainData.heightmapWidth) );
					
					// Calculate the normal of the terrain (note this is in normalised coordinates relative to the overall terrain dimensions)
					Vector3 normal = terrainData.GetInterpolatedNormal(y_01,x_01);
					
					// Calculate the steepness of the terrain
					float steepness = terrainData.GetSteepness(y_01,x_01);
					
					// Setup an array to record the mix of texture weights at this point
					float[] splatWeights = new float[terrainData.alphamapLayers];
					
					// CHANGE THE RULES BELOW TO SET THE WEIGHTS OF EACH TEXTURE ON WHATEVER RULES YOU WANT
					
					// Texture[0] has constant influence
					splatWeights[0] = 0.5f;
					
					// Texture[1] is stronger at lower altitudes
					splatWeights[1] = Mathf.Clamp01((terrainData.heightmapHeight - height));
					
					// Texture[2] stronger on flatter terrain
					// Note "steepness" is unbounded, so we "normalise" it by dividing by the extent of heightmap height and scale factor
					// Subtract result from 1.0 to give greater weighting to flat surfaces
					splatWeights[2] = 1.0f - Mathf.Clamp01(steepness*steepness/(terrainData.heightmapHeight/5.0f));
					
					// Texture[3] increases with height but only on surfaces facing positive Y axis 
					splatWeights[3] = height * 0.005f * Mathf.Clamp01(normal.y);
					
					// Textura[4] para el agua, a nivel de altitud 0:
					/*if (height == 0)
					{
						for (int i = 0; i < 4; i++)
						{
							splatWeights[i] = 0f;
						}
						splatWeights[4] = 1f;
					}*/
					
					// Sum of all textures weights must add to 1, so calculate normalization factor from sum of weights
					float z = splatWeights.Sum();
					
					// Loop through each terrain texture
					for(int i = 0; i < terrainData.alphamapLayers; i++)
					{
						// Normalize so that sum of all texture weights = 1
						splatWeights[i] /= z;
						
						// Assign this point to the splatmap array
						splatmapData[x, y, i] = splatWeights[i];
					}
				}
			}
			
			// Finally assign the new splatmap to the terrainData:
			terrainData.SetAlphamaps(0, 0, splatmapData);
			main.texturizado = true;
		}


		public void Eliminar()
		{
			float[, ,] splatmapData = new float[terrainData.alphamapWidth, terrainData.alphamapHeight, terrainData.alphamapLayers];

			float[] splatWeights = new float[terrainData.alphamapLayers];
			for (int i = 0; i < (terrainData.alphamapLayers-1); i++)
				{
				splatWeights[i] = 0f;
				}
			splatWeights[(terrainData.alphamapLayers-1)] = 1f;

			for (int x = 0; x < terrainData.alphamapWidth; x++)
			{
				for (int y = 0; y < terrainData.alphamapHeight; y++)
				{
					for (int z = 0; z < terrainData.alphamapLayers; z++)
					{
						splatmapData[x,y,z] = splatWeights[z];
					}
				}
			}
			
			terrainData.SetAlphamaps(0, 0, splatmapData);
			main.texturizado = false;
		}
	
		// Update is called once per frame
		/*void Update ()
		{
	
		}*/
}

