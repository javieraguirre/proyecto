using System;
using UnityEngine;
using System.Collections;


namespace AssemblyCSharp
{
		public class Voss
		{

				TerrainData terrainData;
				float[,] heights;
				int xRes, yRes;

				float alturaTerrenoInicial = 0.3f;
				bool generarNuevo = false;


				public Voss (TerrainData _tData)
				{
						terrainData = _tData;
						xRes = terrainData.heightmapWidth;
						yRes = terrainData.heightmapHeight;
						heights = terrainData.GetHeights(0, 0, xRes, yRes);
				}

				public TerrainData Crear()
				{
						/*
							Generador de ruido de Martin Gardner de 1978 basado en la tecnica de Voss.
							
							http://musimat.com/MusimatChapter9/html/_c091704d_8cpp.html
						*/

						// Reiniciar la altura del terreno a 0:
						
						if(generarNuevo)
						{
							for (int y = 0; y < yRes; y++)
							{
								for (int x = 0; x < xRes; x++)
								{
									heights[x,y] = alturaTerrenoInicial;
								}
							}
						}
						
						
						float[] L = new float[4]; //RealList L(Random(), Random(), Random(), Random());
						float[,] R = new float[xRes, yRes]; // RealList R;
						
						for (int x = 0; x < xRes; x++)
						{
							for (int y = 0; y < yRes; y++)
							{					
								heights[x,y] += VossFracRand(y, L);
							}
						}
						
						terrainData.SetHeights(0, 0, heights);			



						return terrainData;
				}


				float VossFracRand(int n, float[] L)
				{
						float sum = 0.0f;
						int N = L.Length;
						for(int k = 0; k < N; k = k + 1)
						{
								if ((n % Mathf.Pow(2, k)) == 0)
								{
									L[k] = UnityEngine.Random.Range(-0.01f, 0.01f);
								}
								sum = sum + L[ k ];
						}
						return sum;
				}
		}
}

