using UnityEngine;
using System.Collections;
using System; // la libreria que incluye el resolvedor de excepciones

public class Resetear : MonoBehaviour
{
		Terrain terrain;
		TerrainData tData;
		float[,] heights;
		int xRes, yRes;

		bool resetVisual = false;
		int xvis = 0;


		public Resetear ()
		{
		}


		public void ResetRapido()
		{
			for (int y = 0; y < yRes; y++)
			{
				for (int x = 0; x < xRes; x++)
				{				
					heights[x,y] = 0f;
				}
			}
			
			tData.SetHeights(0, 0, heights);
		}


		public void ResetVisual()
		{
			resetVisual = true;
		}


		// Use this for initialization
		void Start ()
		{
			this.name = "objetoActivo";
			terrain = GameObject.Find("Terrain").GetComponent<Terrain>();
			tData = terrain.terrainData;
			xRes = tData.heightmapWidth;
			yRes = tData.heightmapHeight;
			heights = tData.GetHeights(0, 0, xRes, yRes);
			xvis = 0;
		}


		// Update is called once per frame
		void Update ()
		{
			try
			{
				if (resetVisual)
				{
					for (int yvis = 0; yvis < yRes; yvis++)
					{
						heights[xvis,yvis] = 0f;
					}
				xvis++;
				}
				tData.SetHeights(0, 0, heights);
			}
			catch (IndexOutOfRangeException)
			{
				resetVisual = false;
				xvis = 0;
			}
		}
}

