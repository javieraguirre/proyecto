using System;
using UnityEngine;
using System.Collections;


namespace AssemblyCSharp
{
		public class Bourke
		{
				TerrainData tData;
				float[,] heights;
				int xRes, yRes;

				float altura;
				int iterBourke; // = 75;
				float ataqueBourke; // = 0.01f;
				bool generarNuevo = true;


				public Bourke (TerrainData _tData)
				{
						tData = _tData;
						xRes = tData.heightmapWidth;
						yRes = tData.heightmapHeight;
						heights = tData.GetHeights(0, 0, xRes, yRes);
				}


				public TerrainData Crear()
				{
						tData = AlgoritmoBourke(1f, 75, 0.01f);
						return tData;
				}


				public TerrainData Crear(float altura, int iterBourke, float ataqueBourke)
				{
						tData = AlgoritmoBourke(altura, iterBourke, ataqueBourke);
						return tData;	
				}



				TerrainData AlgoritmoBourke(float altura, int iterBourke, float ataqueBourke)
				{

						
						/*
						Generador de terreno basado en el "generador de mundos falsos" de Paul Bourke
							
						http://paulbourke.net/fractals/noise/
						*/
						
						// Reiniciar la altura del terreno a 0:
						
						if(generarNuevo)
						{
							for (int y = 0; y < yRes; y++)
							{
								for (int x = 0; x < xRes; x++)
								{
									heights[x,y] = altura; //0.5f;
								}
							}
						}
						
						for (int i = 0; i < iterBourke; i++)
						{
							
							// elegir punto aleatorio:
							int posX = UnityEngine.Random.Range(0, xRes);
							int posY = UnityEngine.Random.Range(0, yRes);
							
							// elegir direccion aleatoria:
							int dirX = UnityEngine.Random.Range(-1, 2); //if (dirX == 0) { dirX = 1; }
							int dirY = UnityEngine.Random.Range(-1, 2); //if (dirY == 0) { dirY = 1; }

							Debug.Log("posX : " + posX + "; posY: " + posY + "; dirX: " + dirX + "; dirY: " + dirY);

							// Pasada en angulo recto:						
							for (int x = 0; x < xRes; x++)
							{
								for (int y = 0; y < yRes; y++)
								{
									if ((Mathf.Sign(x - posX) == dirX) && (Mathf.Sign(y - posY) == dirY)) { heights[x,y] += ataqueBourke; }
									else { heights[x,y] -= ataqueBourke; }

								}
							}

							// Pasada en oblicuo hacia arriba:
							{
								int y; int _posY = posY;				
								for (int x = posX; x < xRes; x = x + UnityEngine.Random.Range(1, 3))
								{
									y = 0;
									while (y < _posY)
									{
										heights[x,y] += ataqueBourke * dirX;
										y++;
									}
									while (y < yRes)
									{
										heights[x,y] -= ataqueBourke * dirX;
										y++;
									}
									if (_posY < yRes) { _posY++; }
								}
							}
							// Pasada en oblicuo hacia abajo:
							{
								int y; int _posY = posY;			
								for (int x = (posX-1); x >= 0; x = x - UnityEngine.Random.Range(1, 3))
								{
									y = 0;
									while (y < _posY)
									{
										heights[x,y] += ataqueBourke * dirY;
										y++;
									}
									while (y < yRes)
									{
										heights[x,y] -= ataqueBourke * dirY;
										y++;
									}
									if (_posY > 0) { _posY--; }
								}
							}

							// Pasada en oblicuo al otro lado hacia arriba:
							{
								int y; int _posY = posY;				
								for (int x = posX; x < xRes; x = x + UnityEngine.Random.Range(1, 3))
								{
									y = 0;
									while (y < _posY)
									{
										heights[x,y] += ataqueBourke * dirX;
										y++;
									}
									while (y < yRes)
									{
										heights[x,y] -= ataqueBourke * dirX;
										y++;
									}
									if (_posY > 0) { _posY--; }
								}
							}
							// Pasada en oblicuo al otro lado hacia abajo:
							{
								int y; int _posY = posY;			
								for (int x = (posX-1); x >= 0; x = x - UnityEngine.Random.Range(1, 3))
								{
									y = 0;
									while (y < _posY)
									{
										heights[x,y] += ataqueBourke * dirY;
										y++;
									}
									while (y < yRes)
									{
										heights[x,y] -= ataqueBourke * dirY;
										y++;
									}
									if (_posY < yRes) { _posY++; }
								}
							}
						}						
						
						tData.SetHeights(0, 0, heights);
						return tData;
				}
		}
}

