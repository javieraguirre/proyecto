using UnityEngine;
using System.Collections;
using AssemblyCSharp;

namespace AssemblyCSharp
{
		public class MidPointDisplacementV3visual : MonoBehaviour
		{
				Terrain terrain;
				TerrainData tData;
				float[,] heights;
				int xRes, yRes;			
				
				int puntosAltura = 8;			string sPuntosAltura;
				int _puntosAltura;
				int longitudPasos = 0;
				int numPasadas = 0;
				int profundidad = 1;			string sProfundidad;
				float multiplicadorAltura = 1f;	string sMultAltura;
				float _multiplicadorAltura;
				
				float alturaMinima;				string sAltMinima;
				float alturaMaxima;				string sAltMaxima;
				float alturaInicial;			string sAltInicial;
				
				bool[,] marcas;

				bool mostrarGUI = true;
				int pasos = 0;
				bool generarNuevoMDP = true;	string sNuevo;
				bool generar = false;

				Contador contadorUniversal;
				Main main;

				string tituloGUI = "Generador de terreno mediante la tecnica Mid-Point Displacement:";
		


				void Start()
				{
						this.name = "objetoActivo";

						// referencia a Main para guardar y recuperar parametros:
						main = GameObject.Find("Terrain").GetComponent<Main>();

						// referencia a Main para acceder al terreno:
						terrain = GameObject.Find("Terrain").GetComponent<Terrain>();
						tData = terrain.terrainData;

						xRes = tData.heightmapWidth;
						yRes = tData.heightmapHeight;
						heights = tData.GetHeights(0, 0, xRes, yRes);
						marcas = new bool[xRes, yRes];

						contadorUniversal = GameObject.Find("Contador").GetComponent<Contador>();
						contadorUniversal.contador = 0;

						// recuperar valores establecidos por el usuario en otras instancias del objeto:
						generarNuevoMDP = main.generarNuevoMDP;
						puntosAltura = main.puntosAltura;
						profundidad = main.profundidad;
						multiplicadorAltura = main.multiplicadorAltura;
						alturaMinima = main.alturaMinima;
						alturaMaxima = main.alturaMaxima;
						alturaInicial = main.alturaTerrenoInicial;

						// colocar los parametros en los "labels" del GUI:
						if (generarNuevoMDP) { sNuevo = "1"; } else { sNuevo = "0"; }
						sPuntosAltura = puntosAltura.ToString();
						sProfundidad = profundidad.ToString();
						sMultAltura = multiplicadorAltura.ToString();
						sAltMinima = alturaMinima.ToString();
						sAltMaxima = alturaMaxima.ToString();
						sAltInicial = alturaInicial.ToString();
				}

				void Update()
				{
						if (generar)
						{
								if ((generarNuevoMDP == true) && (pasos == 0))
								{
										for (int x = 0; x < xRes; x++)
										{
												for (int y = 0; y < yRes; y++)
												{
													heights[x,y] = alturaInicial;
												}
										}
								}


								if (pasos < xRes)
								{
									/* Tecnica de Fournier, Fussel y Carpenter en el SIGGRAPH de 1982
									
									http://www.gameprogrammer.com/fractal.html
									http://en.wikipedia.org/wiki/Diamond-square_algorithm */

									
									//for (int x = 0; x < xRes; x = x + longitudPasos)
									//{
										//heights = tData.GetHeights(0, 0, xRes, yRes);

										_multiplicadorAltura = multiplicadorAltura;
										_puntosAltura = puntosAltura;		
										
										
										for (int i = 0; i < profundidad; i++)
										{
											for (int y = 0; y < (_puntosAltura+1); y++)
											{
												//Debug.Log("x: " + x + "; profundidad: " + i + "; puntosAltura: " + _puntosAltura + "; y: " + y + "; yRes: " + yRes + "; valorFinal: " + (int) (((float) y / (float) _puntosAltura) * (yRes-1)));
												heights[pasos, ((int) (((float) y / (float) _puntosAltura) * (yRes-1)))] += UnityEngine.Random.Range(alturaMinima, alturaMaxima) * _multiplicadorAltura;
												marcas[pasos, ((int) (((float) y / (float) _puntosAltura) * (yRes-1)))] = true;
											}
											
											heights = rellenadoHorizontal(heights, pasos);							
											
											_multiplicadorAltura = _multiplicadorAltura * 0.3f;
											_puntosAltura = _puntosAltura * 2;
										}
										
										heights = rellenadoVertical(heights);
									//}
									
									tData.SetHeights(0, 0, heights);
								}

								else
								{
										// memorizar parametros establecidos por el usuario:
										main.puntosAltura = puntosAltura;
										main.profundidad = profundidad;
										main.multiplicadorAltura = multiplicadorAltura;
										main.alturaMinima = alturaMinima;
										main.alturaMaxima = alturaMaxima;
										main.alturaTerrenoInicial = alturaInicial;
										main.generarNuevoMDP = generarNuevoMDP;

										Object.Destroy(this.gameObject);
					
								}
								
								pasos = pasos + longitudPasos;
								contadorUniversal.contador = (int) (((double) pasos / (double) xRes) * 100);
						}
				}


				void OnGUI()
				{
						if (mostrarGUI)
						{
								GUI.Box(new Rect(250, 10, /*400*/ tituloGUI.Length*6+20, 20), tituloGUI);
		
								GUI.Box(new Rect(250, 40, 180, 20), "Descartar terreno previo: ");
								sNuevo = GUI.TextField(new Rect(430, 40, 40, 20), sNuevo, 1);
		
								GUI.Box(new Rect(250, 60, 180, 20), "Nº puntos altura: ");
								sPuntosAltura = GUI.TextField(new Rect(430, 60, 40, 20), sPuntosAltura, 2);
								
								GUI.Box(new Rect(250, 80, 180, 20), "Multiplicador de altura: ");
								sMultAltura = GUI.TextField(new Rect(430, 80, 40, 20), sMultAltura, 5);
								
								GUI.Box(new Rect(250, 100, 180, 20), "Altura terreno inicial: ");
								sAltInicial = GUI.TextField(new Rect(430, 100, 40, 20), sAltInicial, 5);

								GUI.Box(new Rect(250, 120, 70, 20), "Alt. min: ");
								sAltMinima = GUI.TextField(new Rect(320, 120, 40, 20), sAltMinima, 5);
		
								GUI.Box(new Rect(360, 120, 70, 20), "Alt. Max: ");
								sAltMaxima = GUI.TextField(new Rect(430, 120, 40, 20), sAltMaxima, 5);
		
								GUI.Box(new Rect(250, 140, 180, 20), "Profundidad del algoritmo: ");
								sProfundidad = GUI.TextField(new Rect(430, 140, 40, 20), sProfundidad, 2);
		
								if (!generar)
								{
										if(GUI.Button(new Rect(250, 160, 120, 20), "Generar"))
										{
												if ( (string.Equals(sNuevo, "0")) || (string.Equals(sNuevo, "f")) || (string.Equals(sNuevo, "F")) || (string.Equals(sNuevo, "n")) || (string.Equals(sNuevo, "N")) ) { generarNuevoMDP = false; }
												if ( (string.Equals(sNuevo, "1")) || (string.Equals(sNuevo, "t")) || (string.Equals(sNuevo, "T")) || (string.Equals(sNuevo, "v")) || (string.Equals(sNuevo, "V")) || (string.Equals(sNuevo, "s")) || (string.Equals(sNuevo, "S"))) { generarNuevoMDP = true; }
												generar = true;
												puntosAltura = int.Parse(sPuntosAltura);
												multiplicadorAltura = float.Parse(sMultAltura);
												alturaMinima = float.Parse(sAltMinima);
												alturaMaxima = float.Parse(sAltMaxima);
												profundidad = int.Parse(sProfundidad);
												alturaInicial = float.Parse(sAltInicial);
		
												if (profundidad > 20)
												{
													Debug.Log("Parametro 'profundidad' no puede ser mas de 20.");
													profundidad = 20;
												}
												
												numPasadas = (int) Mathf.Log(puntosAltura, 2); // 2 elevado a puntosAltura
												longitudPasos = ((xRes-1) / puntosAltura);
												
												float[,] heights = tData.GetHeights(0, 0, xRes, yRes);
										}
								}
								else if (generar)
								{
										GUI.color = Color.yellow;
										GUI.Box(new Rect(250, 160, 120, 20), "Generando..." + contadorUniversal.contador + " %");
										GUI.color = Color.white;
								}
						}

						if (Event.current.Equals(Event.KeyboardEvent("m")))
						{
								if (mostrarGUI)
								{
									mostrarGUI = false;
									// GameObject.Find("objetoActivo") == true
									//controlRaton.enabled = true;
								}
								else if (!mostrarGUI)
								{
									mostrarGUI = true;
									//controlRaton.enabled = false;
								}
						}
				}



				float[,] rellenadoHorizontal(float[,] heights, int x)
				{
					//for (int x = 0; x < xRes; x = x + longitudPasos)
					{
						int cursor = 0;
						
						for (int y = 0; y < yRes; y++)
						{
							if (marcas[x,y]) //(heights[x,y] != 0.5f) //(heights[x,y] > 0f)
							{							
								if (heights[x,cursor] < heights[x,y])
								{
									float B = Mathf.Atan2((heights[x,y] - heights[x,cursor]), y - cursor);
									//heights[x,y] = heights[x,cursor] + Mathf.Tan(B) * (y - cursor);
									for (int _y = cursor; _y < y; _y++)
									{
										heights[x,_y] = heights[x,cursor] + Mathf.Tan(B) * (_y - cursor);
									}
								}
								
								if (heights[x,cursor] > heights[x,y])
								{
									float B = -1 * Mathf.Atan2((heights[x,cursor] - heights[x,y]), y - cursor);
									//heights[x,y] = heights[x,cursor] + (Mathf.Tan(B) * (y - cursor));
									for (int _y = cursor; _y < y; _y++)
									{
										heights[x,_y] = heights[x,cursor] + (Mathf.Tan(B) * (_y - cursor));
									}
								}
								cursor = y;
							}
						}
					}
					return heights;
				}
				
				
				float[,] rellenadoVertical(float[,] heights)
				{
					for (int y = 0; y < yRes; y++)
					{
						int cursor = 0;
						
						for (int x = 0; x < xRes; x = x + longitudPasos)
						{
							//if (heights[x,y] > 0f)
							{
								if (heights[cursor,y] < heights[x,y]) // (heights[x,y] > heights[cursor,y])
								{
									float B = Mathf.Atan2((heights[x,y] - heights[cursor,y]), x - cursor);
									
									for (int _x = cursor; _x < x; _x++)
									{
										heights[_x,y] = heights[cursor,y] + Mathf.Tan(B) * (_x - cursor);
									}
								}
								
								if (heights[cursor,y] > heights[x,y]) // (heights[x,y] < heights[cursor,y])
								{
									float B = -1 * Mathf.Atan2((heights[cursor,y] - heights[x,y]), x - cursor);
									
									for (int _x = cursor; _x < x; _x++)
									{
										heights[_x,y] = heights[cursor,y] + (Mathf.Tan(B) * (_x - cursor));
									}
								}
								
								cursor = x;
							}
						}
					}
					return heights;
				}
		
		}
}

