using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;
using AssemblyCSharp;


public class Guardar : MonoBehaviour
{
		int alturaBotones = 20;
		string[] s;
		int indiceArchivos;
		string nuevoArchivo = "Nuevo archivo de terreno";
		bool mostrarGUI = true;

		IFormatter formatter;
		Main main;

		float[,] heights;
		Terrain terrain;
		TerrainData tData;
		int xRes, yRes;


		// Use this for initialization
		void Start ()
		{
			this.name = "objetoActivo";

			terrain = GameObject.Find("Terrain").GetComponent<Terrain>();
			main = GameObject.Find("Terrain").GetComponent<Main>();
		
			tData = terrain.terrainData;
			xRes = tData.heightmapWidth;
			yRes = tData.heightmapHeight;
			heights = tData.GetHeights(0, 0, xRes, yRes);

			s = System.IO.Directory.GetFiles(System.Environment.CurrentDirectory, "*.terreno");

			formatter = new BinaryFormatter();
		}
	

		// Update is called once per frame
		void Update ()
		{
	
		}


		void OnGUI ()
		{
			if (mostrarGUI)
			{
				GUI.color = Color.yellow;
				GUI.Box(new Rect(250,10+(alturaBotones*0),220,20), "Asistente de guardado:");
				GUI.color = Color.white;
	
	
				// guardado nuevo:
				nuevoArchivo = GUI.TextField(new Rect(250, 50, 220, 20), nuevoArchivo, 100);
				if (GUI.Button(new Rect(470, 50, 60, 20), "Guardar"))
				{
					//IFormatter formatter = new BinaryFormatter();
					Stream stream = new FileStream(nuevoArchivo + ".terreno", FileMode.Create, FileAccess.Write, FileShare.None);
					formatter.Serialize(stream, heights);
					stream.Close();
					main.objetoActivo = "";
				}
	
	
				if (s != null)
				{
					for (int i = 0; i < s.Length; i ++)
					{
						if (s[i] != null)
						{
							// listar:
							GUI.Box(new Rect(250, 70 + (alturaBotones*i), 220, 20), Path.GetFileName(s[i]));
							
							// guardado nuevo:
							/*nuevoArchivo = GUI.TextField(new Rect(250, 50, 220, 20), nuevoArchivo, 100);
							if (GUI.Button(new Rect(470, 50, 60, 20), "Guardar"))
							{
								//IFormatter formatter = new BinaryFormatter();
								Stream stream = new FileStream(nuevoArchivo + ".terreno", FileMode.Create, FileAccess.Write, FileShare.None);
								formatter.Serialize(stream, heights);
								stream.Close();
								main.objetoActivo = "";
							}*/
	
	
							// guardado reemplazando:
							if (GUI.Button(new Rect(470, 70 + (alturaBotones*i), 30, 20), "Ok"))
							{
								//IFormatter formatter = new BinaryFormatter();
								Stream stream = new FileStream(Path.GetFileName(s[i]), FileMode.Create, FileAccess.Write, FileShare.None);
								formatter.Serialize(stream, heights);
								stream.Close();
							}
	
							// borrar:
							if (GUI.Button(new Rect(500, 70 + (alturaBotones*i), 30, 20), "X"))
							{
								System.IO.File.Delete(s[i]);
								s[i] = null;
								main.objetoActivo = "";
							}
						}
					}
				}
		
				if (Event.current.Equals(Event.KeyboardEvent(main.teclaMenu)))
				{
					if (mostrarGUI)
					{
						mostrarGUI = false;
						// GameObject.Find("objetoActivo") == true
						//controlRaton.enabled = true;
					}
					else if (!mostrarGUI)
					{
						mostrarGUI = true;
						//controlRaton.enabled = false;
					}
				}
			}
		}
}

