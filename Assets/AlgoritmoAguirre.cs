using System;
using UnityEngine;
using System.Collections;


namespace AssemblyCSharp
{
		public class AlgoritmoAguirre
		{

				TerrainData tData;
				float[,] heights;
				int xRes, yRes;

				float alturaTerrenoInicial = 0.3f;


				public AlgoritmoAguirre (TerrainData _tData)
				{
						tData = _tData;
						xRes = tData.heightmapWidth;
						yRes = tData.heightmapHeight;
						heights = tData.GetHeights(0, 0, xRes, yRes);
				}


				public TerrainData Crear()
				{
						tData = AlgoritmoCrear(true, UnityEngine.Random.Range(1,10), UnityEngine.Random.Range(1,3), UnityEngine.Random.Range(0.005f,0.01f));
						return tData;
				}


				TerrainData AlgoritmoCrear(bool generarNuevo, int numCordilleras, int numDepresiones, float rugosidadSuelo)
				{
						Cordillera cordillera;// = new Cordillera(tData);
						Depresion depresion;// = new Depresion(tData);
						Rugosidad rugosidad;// = new Rugosidad(tData);
			

						// Reiniciar la altura del terreno a 0:
						
						if(generarNuevo)
						{				
							for (int y = 0; y < yRes; y++)
							{
								for (int x = 0; x < xRes; x++)
								{
									heights[x,y] = alturaTerrenoInicial;
								}
							}

							tData.SetHeights(0, 0, heights);
				
						}						


						if (numCordilleras > 0)
						{
							for (int i = 0; i < numCordilleras; i++)
							{
								cordillera = new Cordillera(tData);
								tData = cordillera.Crear();
							}
						}
						
						if (numDepresiones > 0)
						{
							for (int i = 0; i < numDepresiones; i++)
							{
								depresion = new Depresion(tData);
								tData = depresion.Crear();
							}
						}
						
						
						if (rugosidadSuelo != 0)
						{
							rugosidad = new Rugosidad(tData);			
							tData = rugosidad.Crear();
						}
						
						/*if (cordillera)
						{
						// Cordillera aleatoria simple:
						//x = Random.Range(10, xRes);
						altura = 0.8f;
						
						for (y = Random.Range(10, yRes); y < yRes - 10; y++)
						{
							//int r = Random.Range(0, 5);
							//x = Random.Range((x-r), (x+r));
							x = Random.Range(10, xRes);
							
							heights[x,y] = Random.Range(0.001f, altura);
						}
						
						heights = SuavizarPicos(heights, ataque);
						}*/
						
						
						/*if (grieta)
						{
						heights = DepresionAleatoriaSimple(heights);
						}*/
						
						
						/*if (grieta2)
						{
						heights = DepresionAleatoria2(heights);
						}*/
						
						
						/*if (socavon)
						{
						heights = DepresionAleatoria2(heights);
						}*/
						
						
						/*for (int i = 0; i<suavizado; i++)
						{
						heights = SuavizarTerreno(heights, factor);
						}*/
						
						
						
						/*if (pasadasSuavFinal > 0)
						{
							for (int i = 0; i < pasadasSuavFinal; i++)
							{
								heights = SuavizarPicos(heights, 0, 0, ataqSuavFinal);
							}
						}*/
						
						// Aplicar cambios al terreno:
						//tData.SetHeights(0, 0, heights);
						return tData;
				}


		}
}

