using System;
using UnityEngine;
using System.Collections;

//using GeneradorMundos;

namespace AssemblyCSharp
{
		public class MidPointDisplacementV2
		{

				// Valores generales:
		
				TerrainData tData;
				float[,] heights;
				int xRes, yRes;			
		
				int puntosAltura;// = 8;
				int longitudPasos;// = 0;
				int numPasadas;// = 0;
				float multiplicadorAltura;// = 2f;
				
				float alturaMinima;// = 0.1f;
				float alturaMaxima;// = 0.6f;

				bool[,] marcas;// = new bool[xRes,yRes];


				// Constructor:
				public MidPointDisplacementV2 (TerrainData _tData)
				{
					tData = _tData;
					xRes = tData.heightmapWidth;
					yRes = tData.heightmapHeight;
					heights = tData.GetHeights(0, 0, xRes, yRes);
					marcas = new bool[xRes,yRes];
				}


				public TerrainData Crear(int puntosAltura, float multiplicadorAltura, float alturaMinima, float alturaMaxima)
				{
					tData = AlgoritmoMPDispV2(puntosAltura, multiplicadorAltura, alturaMinima, alturaMaxima);
					return tData;
				}


				public TerrainData Crear()
				{
					tData = AlgoritmoMPDispV2(UnityEngine.Random.Range(8, 64), UnityEngine.Random.Range(0.5f, 2f), UnityEngine.Random.Range(0.2f, 0.5f), UnityEngine.Random.Range(0.8f, 1f));
					return tData;
				}


				// Algoritmo:
				TerrainData AlgoritmoMPDispV2(int puntosAltura, float multiplicadorAltura, float alturaMinima, float alturaMaxima)
				{

					/*
					Tecnica de Fournier, fussel y Carpenter en el SIGGRAPH de 1982
					
					http://www.gameprogrammer.com/fractal.html
					http://en.wikipedia.org/wiki/Diamond-square_algorithm
					http://nbickford.wordpress.com/2012/12/21/creating-fake-landscapes/
					*/
					
					numPasadas = (int) Mathf.Log(puntosAltura, 2); // 2 elevado a puntosAltura
					longitudPasos = ((xRes-1) / puntosAltura);
					//Debug.Log("xRes: " + (xRes-1) + ";\tnumPasadas: " + numPasadas + ";\tlongitudSaltos: " + longitudPasos);
					
					float[,] heights = tData.GetHeights(0, 0, xRes, yRes);
					
					/*float[,] heightsMultiplicador;
					if(!generarNuevo) { heightsMultiplicador = tData.GetHeights(0,0,xRes,yRes); }*/
					
					
					// Reiniciar la altura del terreno a 0:
					
					for (int y = 0; y < yRes; y++)
					{
						for (int x = 0; x < xRes; x++)
						{
							heights[x,y] = 0f;
						}
					}
					
					int cursor = 0;
					float alturaActual = 1f;
					
					
					// Paso 1: crear puntos de altura:
					
					for (int x = 0; x < xRes; x = x + longitudPasos)
					{
						cursor = 0;
						
						// Puntos inicial y final de cada fila:
						heights[x, yRes-1] = 0.1f;
						marcas[x, yRes-1] = true;
						heights[x, 0] = 0.1f;
						marcas[x, 0] = true;
						
						for (int i = 0; i < numPasadas; i++)
						{
							cursor = 0;
							
							
							for (int y = 0; y < yRes; y++)
							{
								if ( marcas[x,y] ) //if (heights[x,y] > 0.01f)
								{
									//Debug.Log("x: " + x + "; y: " + y + "; cursor: " + cursor + "; y - cursor: " + (y - cursor) + "; y - cursor / 2: " + (int) (y - cursor)/2);
									heights = rellenado(heights, false);
									alturaActual = UnityEngine.Random.Range((heights[x,y] - alturaActual), (heights[x,y] + alturaActual)) * multiplicadorAltura;
									heights[x, (int) (cursor + ((y - cursor) / 2))] = alturaActual;
									marcas[x, (int) (cursor + ((y - cursor) / 2))] = true;
									cursor = y;
								}
							}	
						}

						heights = rellenado(heights, true);
					}
					
					

					
					
					tData.SetHeights(0, 0, heights);
					return tData;
				}


				float[,] rellenado(float[,] heights, bool rellenoCompleto)
				{
						// PAso 2: rellenar huecos entre puntos de altura:
						// Pasada horizontal:

						int cursor;

						for (int x = 0; x < xRes; x = x + longitudPasos)
						{
							cursor = 0;
							
							for (int y = 0; y < yRes; y++)
							{
								if (heights[x,y] > 0f) // if ( marcas[x,y] )
								{				
									if (heights[x,cursor] <= heights[x,y])
									{
										float B = Mathf.Atan2((heights[x,y] - heights[x,cursor]), y - cursor);
										
										if (rellenoCompleto)
										{
											for (int _y = cursor; _y < y; _y++)
											{
												heights[x,_y] = heights[x,cursor] + Mathf.Tan(B) * (_y - cursor);
											}
										}
										else { heights[x,y] = heights[x,cursor] + Mathf.Tan(B) * (y - cursor); }
									}
									
									if (heights[x,cursor] > heights[x,y])
									{
										float B = -1 * Mathf.Atan2((heights[x,cursor] - heights[x,y]), y - cursor);

										if (rellenoCompleto)
										{
											for (int _y = cursor; _y < y; _y++)
											{
												heights[x,_y] = heights[x,cursor] + (Mathf.Tan(B) * (_y - cursor));
											}
										}
										else { heights[x,y] = heights[x,cursor] + (Mathf.Tan(B) * (y - cursor)); }
									}
									
									cursor = y;
								}
								
							}
							
						}
						
						// Pasada vertical:

						for (int y = 0; y < yRes; y++)
						{
							cursor = 0;
							
							for (int x = 0; x < xRes; x++)
							{
								if (heights[x,y] > 0f) // if ( marcas[x,y] )
								{
									if (heights[cursor,y] <= heights[x,y]) // (heights[x,y] > heights[cursor,y])
									{
										float B = Mathf.Atan2((heights[x,y] - heights[cursor,y]), x - cursor);

										if (rellenoCompleto)
										{	
											for (int _x = cursor; _x < x; _x++)
											{
												heights[_x,y] = heights[cursor,y] + Mathf.Tan(B) * (_x - cursor);
											}
										}
										//else { heights[x,y] = heights[cursor,y] + Mathf.Tan(B) * (x - cursor); }
							
									}
									
									if (heights[cursor,y] > heights[x,y]) // (heights[x,y] < heights[cursor,y])
									{
										float B = -1 * Mathf.Atan2((heights[cursor,y] - heights[x,y]), x - cursor);
										
										if (rellenoCompleto)
										{	
											for (int _x = cursor; _x < x; _x++)
											{
												heights[_x,y] = heights[cursor,y] + (Mathf.Tan(B) * (_x - cursor));
											}
										}
										//else { heights[x,y] = heights[cursor,y] + (Mathf.Tan(B) * (x - cursor)); }
							
									}
									
									cursor = x;					
								}
							}
						}

				return heights;

				}
		


			}
	
		}


