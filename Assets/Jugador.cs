using UnityEngine;
using System.Collections;
using AssemblyCSharp;
using System.IO;


public class Jugador : MonoBehaviour
{
		Terrain terrain;
		TerrainData tData;
		float[,] heights;
		int xRes, yRes;

		//GameObject personaje;
		Transform posicionPersonaje;

		bool camaraSubjetiva = false;

		// Use this for initialization
		void Start ()
		{
			terrain = GameObject.Find("Terrain").GetComponent<Terrain>();
			tData = terrain.terrainData;
			xRes = tData.heightmapWidth;
			yRes = tData.heightmapHeight;
			heights = tData.GetHeights(0, 0, xRes, yRes);
			
			//personaje = GameObject.Find("First Person Controller").GetComponent<CharacterMotor>();
			posicionPersonaje = GameObject.Find("First Person Controller").GetComponent<Transform>();
		}
	
		void OnGUI ()
		{
			if (Event.current.Equals(Event.KeyboardEvent(".")))
			{
				if (!camaraSubjetiva)
				{
					GUI.Box(new Rect(Screen.width - 120, Screen.height - 100, 80, 20), "Camara subjetiva");				
					camaraSubjetiva = true;
					posicionPersonaje.transform.position = new Vector3((tData.heightmapWidth / 2), tData.heightmapHeight, (tData.heightmapWidth / 2));
					
				}
				else if (camaraSubjetiva)
				{
					camaraSubjetiva = false;
					posicionPersonaje.transform.position = new Vector3(0, 1000, -1200);
				}
			}
		}
}
