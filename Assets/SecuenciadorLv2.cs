using UnityEngine;
using System.Collections;
using AssemblyCSharp;
using System;


public class SecuenciadorLv2 : MonoBehaviour
{
		bool mostrarGUI = true;
		string tituloGUI = "Interprete y secuenciador de comandos L-System v2";
		bool generar = false;
		
		Main main;
		MidPointDisplacementV3 mpd;

		Terrain terrain;
		TerrainData tData;
		float[,] heights;
		int xRes, yRes;	

		int iteraciones = 1;	string sIter = "";

		string secuencia = "ab0;b0a;0ab";	string sSecuencia;

		char[] vars;
		string[] reglas;

		int contador = 0, x = 0, excepciones = 0;
		int m = 20;
		int count = 0, countMax = 0;
	
		string[] arrayFrase;
		string[][] arrayReglas;
		string[] arrayFinal;
	
		//string[] arrayFrases = null;
		string[][] arraySimbolos = null;
		char [][] arrayCaracteres = null;
		int filas0 = 0, columnas0 = 0;

		float altura = 0;
		float altura0 = 1.5f;			string sAltura0 = "1.5";


		// Use this for initialization
		void Start ()
		{
			this.name = "objetoActivo";
		
			main = GameObject.Find("Terrain").GetComponent<Main>();
			main.generarNuevoMDP = false;

			terrain = GameObject.Find("Terrain").GetComponent<Terrain>();
			tData = terrain.terrainData;
			
			xRes = tData.heightmapWidth;
			yRes = tData.heightmapHeight;
			heights = tData.GetHeights(0, 0, xRes, yRes);

			vars = new char[3];
			reglas = new string[vars.Length];

			vars[0] = 'a';		reglas[0] = "a00;aa0;aaa";
			vars[1] = 'b';		reglas[1] = "b00;bb0";
			vars[2] = '0';		reglas[2] = "000";
/*			vars[3] = "+";		reglas[3] = "";
			vars[4] = "-";		reglas[4] = "";
			vars[5] = "*";		reglas[5] = "";*/

			sSecuencia = secuencia;
			sIter = "2";
		}

	
		// Update is called once per frame
		void Update ()
		{
			if (generar)
			{
				count = 0;
				countMax = 0;
			
				// Crear el array de la secuencia:
				//string[] arrayFrase;// = new string[secuencia.Split (';').Length]; // = secuencia.Split(';');

				arrayFrase = secuencia.Split(';');				

				/*for (int i = 0; i < arrayFrase.Length; i ++)
				{
					for (int j = 0; j < arrayFrase[i].Length; j ++)
					{
						Debug.Log("[" + i + "," + j + "]" + " = " + arrayFrase[i][j]);
					}
				}*/


				// declarar los arrays de reglas:
				// ya conozco una de las dimensiones del array: el numero de reglas que hay
				arrayReglas = new string[reglas.Length][];

				// convertir los strings de reglas en arrays:
				for (int i = 0; i < reglas.Length; i ++)
				{
					// contar numero de lineas separadas por ';' especificadas en cada regla
					count = reglas[i].Split(';').Length;

					arrayReglas[i] = reglas[i].Split(';');

					if (count > countMax) { countMax = count; }
				}

				arrayFinal = new string[arrayFrase.Length * countMax];

				// Ya tenemos un array con todas las reglas separadas en un string distinto para cada linea MPD, y un array final con una linea reservada para cada MPD
				/*for (int i = 0; i < arrayReglas.Length; i ++)
				{
					for (int j = 0; j < arrayReglas[i].Length; j ++)
					{
						Debug.Log("arrayReglas[" + i + "]" + "[" + j + "] = " + arrayReglas[i][j].ToString ());
					}
				}*/
				//Debug.Log ("arrayFinal.Length: " + arrayFinal.Length + "\tarrayFrase.Length: " + arrayFrase.Length + "\tcountMax: " + countMax);


				// explorar el arrayFinal, y anadir en cada indice la linea de MPD que corresponde de cada regla
				for (int i = 0; i < arrayFinal.Length; i ++)
				{
					// asignar al arrayFinal la linea de MPD que corresponde:

					for (int j = 0; j < arrayFrase[(int) (i * (1 / (float) arrayFrase.Length))].Length; j ++)
					{
						//arrayFinal[i] += arrayFrase[(int) (i * (1 / (float) arrayFrase.Length))][j].ToString();
						arrayFinal[i] += devolverLetras(arrayFrase[(int) (i * (1 / (float) arrayFrase.Length))][j].ToString(), ((float) i % (float) arrayFrase.Length));
						//Debug.Log ("indice: " + (int) (i * (1/(float) arrayFrase.Length)));
					}

					Debug.Log ("i = " + i + "\t" + arrayFinal[i]);
				}
			

				// Plasmar las instrucciones de produccion en el terreno, a medida que exploro el array de simbolos:
				for (int x = 0; x < arrayFinal.Length; x++)
				{
					for (int y = 0; y < arrayFinal[x].Length; y++)
					{
						try
						{
							if (arrayFinal[x][y] == 'a')
							{
								altura = altura0 / iteraciones;
								//Debug.Log("A\tx: " + x + "; y: " + y + "; altura: " + altura + "; arraux2[x][y]: " + arraux2[x][y]);
							}
							else if (arrayFinal[x][y] == 'b')
							{
								altura = (- altura0) / iteraciones;
								//Debug.Log("B\tx: " + x + "; y: " + y + "; altura: " + altura + "; arraux2[x][y]: " + arraux2[x][y]);
							}
							else if (arrayFinal[x][y] == '0')
							{
								altura = 0;
								//Debug.Log("0\tx: " + x + "; y: " + y + "; altura: " + altura + "; arraux2[x][y]: " + arraux2[x][y]);
							}
							else if (arrayFinal[x][y] == '-') { altura = -1f; }
						}
						catch (IndexOutOfRangeException e)
						{
							Debug.Log("Excepcion indice fuera de rango interpretando array0, con x: " + x + ", y: " + y + ", siendo columnas0: " + columnas0 + ", filas0: " + filas0 + "\n" + e.ToString());
						}


						if (altura != -1f)
						{
							Debug.Log ("x: " + (int) (x * (xRes / arrayFinal.Length)) + "\ty: " + (int) (y * (yRes / arrayFinal[x].Length)) + "\tarrayFinal[x].Length: " + arrayFinal[x].Length + "\taltura: " + altura);

							heights[(int) (x * (xRes / arrayFinal.Length)), (int) (y * (yRes / arrayFinal[x].Length))] = altura;
						}
						else { continue; }
						//Debug.Log("arraySimbolos[x].Length: " + arraySimbolos[x].Length);
						///mpd.AlgoritmoMPD_L((x * (xRes/arrayFinal.Length)), xRes, y, (int) (arrayFinal[x].Length * 0.75f), 1f, altura, altura);
					}
				}
			
				//Debug.Log ("Excepciones encontradas: " + excepciones);


				Debug.Log ("Iniciando rellenado");
				mpd = new MidPointDisplacementV3(tData);
				for (int x = 0; x < arrayFinal.Length; x++)
				{
					heights = mpd.rellenadoHorizontal(heights, (int) (x * (xRes / arrayFinal.Length)));
				}
				heights = mpd.rellenadoVertical(heights);

				tData.SetHeights(0, 0, heights);
			
				generar = false;
			
				iteraciones ++;
				
				//Debug.Log("iteraciones: " + iteraciones + ";\tsIter: " + sIter.ToString());
				if (iteraciones > int.Parse(sIter))
				{
					generar = false;
				}
			}
		}

		string devolverLetras(string letra, float indice)
		{
			string letrasParaDevolver = "";

			for (int i = 0; i < vars.Length; i ++)
			{
				if (letra.Equals(vars[i].ToString()))
					{
					//indice = indice * (1 / (float) arrayReglas[i].Length);
					//Debug.Log ("regla: " + vars[i] + "\tindice: " + indice + "\ti.redim.arrayReglas[i]: " + (int) (indice * ((float) ((arrayReglas[i].Length - 1)/ (float) arrayFrase.Length))));

					// Esto ensancha las reglas; no es lo que quieres:
					//letrasParaDevolver = arrayReglas[i][(int) (indice * ((float) ((arrayReglas[i].Length - 1)/ (float) arrayFrase.Length)))];

					// Esto intercala las reglas en el array Final:
					//letrasParaDevolver = arrayReglas[i][(int) (indice * (((float) ((arrayReglas[i].Length - 1)/ (float) arrayFrase.Length))) / arrayFrase.Length)];
					letrasParaDevolver = intercalar (i, indice);
					}
			}

			//letrasParaDevolver += ",";

			if (letrasParaDevolver.Equals("")) { Debug.Log ("Algun error hay al comparar y devolver las letras"); }
			return letrasParaDevolver;
		}


		string intercalar(int indiceRegla, float subIndiceRegla)
		{
			string s = "";
		
			/* En un principio sera tan facil como hacer esto:
			s = arrayReglas[indiceRegla][(int) subIndiceRegla];
			pero no es posibe, ya que necesitas convertir subIndiceRegla al tamano real de la regla.
			De ahi la necesidad de la funcion intercalar(); */

			float indiceGrande = 0, indicePequeno = 0;
			int indiceEquivalente = 0;
			
			indiceGrande = arrayFrase.Length;
			indicePequeno = arrayReglas[indiceRegla].Length;

			for (int i = 0; i < indiceGrande; i ++)
			{
				try
				{
					indiceEquivalente = (int) (i * (float) ((float) arrayReglas[indiceRegla].Length / (float) indiceGrande));
			
					if 	((i >= ((0.5f * indiceGrande) - (0.5f * indicePequeno)) + indiceEquivalente)
						 && (i < ((0.5f * indiceGrande) + (0.5f * indicePequeno) + 0))
						 && (i == subIndiceRegla))
					{
						//Debug.Log ("i: " + i + "\tsubIndiceRegla: " + subIndiceRegla + "\tdifA: " + ((0.5f * indiceGrande) - (0.5f * indicePequeno)) + "\tdifB: " + ((0.5f * indiceGrande) + (0.5f * indicePequeno)) + "\tP: " + arrayReglas[indiceRegla].Length);
					
						/*Debug.Log ("indiceEquiv: " + indiceEquivalente + "\tCondiciones de entrada:\t" + i + " >= "
						           + (((0.5f * indiceGrande) - (0.5f * indicePequeno)) + indiceEquivalente) + "\t" + i + " < " + 
						           + (((0.5f * indiceGrande) + (0.5f * indicePequeno) + 0)) + "\t" + i + " == " +
						           + subIndiceRegla);*/

						s = arrayReglas[indiceRegla][indiceEquivalente];

						//Debug.Log ("s: " + s);
						break;
					}
					else { s = "---"; }
				}
				catch (IndexOutOfRangeException e)
				{
					//Debug.Log ("Excepcion en i: " + i + "\tindiceEquivalente: " + indiceEquivalente);
					excepciones ++;
				}
			}
			//Debug.Log ("Intercalar_s: " + s);
			return s;
		}




		void OnGUI()
		{
			if (mostrarGUI)
			{
				GUI.Box(new Rect(250, 10, tituloGUI.Length*6+20, 25), tituloGUI);

				GUI.Box(new Rect(250, m*2, 120, m), "Axioma: ");
				secuencia = GUI.TextField(new Rect(370, m*2, 170, m), secuencia, int.MaxValue);

				for (int i = 0; i < vars.Length; i ++)
				{
					GUI.Box(new Rect(250, m*(i + 3), 120, m), "Variable " + i + ": ");
					vars[i] = GUI.TextField(new Rect(370, m*(i + 3), 25, m), vars[i].ToString(), 1)[0];
					GUI.Box(new Rect(395, m*(i + 3), 25, m), "--> ");
					reglas[i] = GUI.TextField(new Rect(420, m*(i + 3), 120, m), reglas[i], 50);
				}

				GUI.Box(new Rect(250, m*(vars.Length + 3), 120, m), "Altura inicial: ");
				sAltura0 = GUI.TextField(new Rect(370, m*(vars.Length + 3), 170, m), sAltura0, 5);
			
				GUI.Box(new Rect(250, m*(vars.Length + 4), 140, m), "Nº iteraciones: ");
				sIter = GUI.TextField(new Rect(390, m*(vars.Length + 4), 40, m), sIter, 3);

				
				if (!generar)
				{
					if (GUI.Button(new Rect(250, m*(vars.Length + 6), 120, m), "Generar"))
					{
						altura0 = float.Parse(sAltura0);
					
						generar = true;
					}
				}
				else if (generar)
				{			
					if (GUI.Button(new Rect(250, m*(vars.Length + 6), 120, m), "Detener"))
					{
						mpd = null;
					
						generar = false;
					}
				}
			}
		
			if (Event.current.Equals(Event.KeyboardEvent(main.teclaMenu)))
			{
				if (mostrarGUI) { mostrarGUI = false; }
				else if (!mostrarGUI) {	mostrarGUI = true; }
			}
		}


	}

