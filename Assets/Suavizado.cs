using UnityEngine;
using System.Collections;
using System;
//using GeneradorMundos;

namespace AssemblyCSharp
{
		public class Suavizado
		{
			// Valores generales:
			
			TerrainData tData;
			float[,] heights;
			int xRes, yRes;
		
			int pasadasSuavizado;

			float gradoAtenuacion;
			float ataque;

			Contador contador;
			

			public Suavizado (TerrainData _tData)
			{
				tData = _tData;
				xRes = tData.heightmapWidth;
				yRes = tData.heightmapHeight;
				heights = tData.GetHeights(0, 0, xRes, yRes);
			}


			public float[,] SuavizarPico(float[,] heights, int _x, int _y)
			{
				heights = AlgoritmoSuavizarPico(heights, _x, _y, UnityEngine.Random.Range(0.5f, 0.9f), UnityEngine.Random.Range(0.01f, 0.05f), 1);
				return heights;
			}


			public float[,] SuavizarPico(float[,] heights, int _x, int _y, float ataque, float gradoAtenuacion, int pasadasSuavizado)
			{
				heights = AlgoritmoSuavizarPico(heights, _x, _y, ataque, gradoAtenuacion, pasadasSuavizado);
				return heights;
			}

			public float[,] SuavizarPico2(float[,] heights, int _x, int _y, float ataque, float gradoAtenuacion, int pasadasSuavizado)
			{
				heights = AlgoritmoSuavizarPico2(heights, _x, _y, ataque, gradoAtenuacion, pasadasSuavizado);
				return heights;
			}

			public float[,] SuavizarHoyo(float[,] heights, int _x, int _y)
			{
				heights = AlgoritmoSuavizarHoyo(heights, _x, _y, UnityEngine.Random.Range(0.5f, 0.9f), UnityEngine.Random.Range(0.01f, 0.05f), 1);
				return heights;
			}
			
			
			public float[,] SuavizarHoyo(float[,] heights, int _x, int _y, float ataque, float gradoAtenuacion, int pasadasSuavizado)
			{
				heights = AlgoritmoSuavizarHoyo(heights, _x, _y, ataque, gradoAtenuacion, pasadasSuavizado);
				return heights;
			}

			public float[,] SuavizarTodo(float[,] heights, float ataque, float gradoAtenuacion, int pasadasSuavizado)
			{
				heights = AlgoritmoSuavizarPico(heights, 0, 0, UnityEngine.Random.Range(0.5f, 0.9f), UnityEngine.Random.Range(0.0f, 0.01f), 1);
				//heights = AlgoritmoSuavizarHoyo(heights, 0, 0, UnityEngine.Random.Range(0.5f, 0.9f), UnityEngine.Random.Range(0.0f, 0.01f), 1);
				return heights;
			}
		


			float[,] AlgoritmoSuavizarPico2(float[,] heights, int x, int y, float ataque, float gradoAtenuacion, int pasadasSuavizado)
			{
				contador = new Contador();

				// parto de la pos inicial

				int _x = x;
				int _y = y;
				float alturaActual = heights[x, y];


				for (int radio = 1; radio < 20; radio++)
				{
						// calculo la nueva altura: altura original * 0.95
						alturaActual = alturaActual * 0.95f;
		
						for (int angulo = 0; angulo < 359; angulo++)
						{
								x = _x + radio * (int) Mathf.Cos(angulo);
								y = _y + radio * (int) Mathf.Sin(angulo);

								if (heights[x,y] < alturaActual) { heights[x, y] = alturaActual; }
						}

				contador.contador = (radio * 10) / 2;
				}	

				// si la altura de este nuevo punto	es mayor que la calculada, se termina el proceso

				// si la altura de este nuevo punto es menor, se asigna la altura calculada (sin tener en cuenta la altura previa de este punto)

				//tData.SetHeights(0, 0, heights);
				return heights;
			}
		



			// obsoleto, dejar de usarlo:
			float[,] AlgoritmoSuavizarPico(float[,] heights, int _x, int _y, float ataque, float gradoAtenuacion, int pasadasSuavizado)
			{
				int x, y;
				float atenuador = 1f;

				for (int i = 0; i < pasadasSuavizado; i++)
				{
					
					// Pasadas rectas:
					
					for (y = _y; y < yRes; y++)
					{
						atenuador = 1f;
						for (x = _x; x < (xRes-1); x++)
						{
							if (heights[x, y] > heights[x+1,y]) { heights[x+1, y] += (heights[x,y] - heights[x+1, y]) * ataque * atenuador; }
							atenuador -= gradoAtenuacion; if (atenuador <= 0) { break; }
						}
						
						atenuador = 1f;
						for (x = _x/*(xRes-1)*/; x >= 1; x--)
						{
							if (heights[x, y] > heights[x-1,y]) { heights[x-1, y] += (heights[x,y] - heights[x-1, y]) * ataque * atenuador; };
							atenuador -= gradoAtenuacion; if (atenuador <= 0) { break; }
						}
					}
					
					for (x = _x; x < xRes; x++)
					{
						atenuador = 1f;
						for (y = _y; y < (yRes-1); y++)
						{
							if (heights[x, y] > heights[x,y+1]) { heights[x, y+1] += (heights[x,y] - heights[x, y+1]) * ataque * atenuador; }
							atenuador -= gradoAtenuacion; if (atenuador <= 0) { break; }
						}
						
						atenuador = 1f;
						for (y = _y/*(yRes-1)*/; y >= 1; y--)
						{
							if (heights[x, y] > heights[x,y-1]) { heights[x, y-1] += (heights[x,y] - heights[x, y-1]) * ataque * atenuador; }
							atenuador -= gradoAtenuacion; if (atenuador <= 0) { break; }
						}
					}
					
					
					// Pasadas oblicuas:
					
					for (y = _y; y < (yRes-1); y++)
					{
						atenuador = 1f;
						for (x = _x; x < (xRes-1); x++)
						{
							if (heights[x, y] > heights[x+1,y+1]) { heights[x+1, y+1] += (heights[x,y] - heights[x+1, y+1]) * ataque * atenuador * 0.59f; }
							atenuador -= gradoAtenuacion; if (atenuador <= 0) { break; }
						}
					}
					
					for (y = _y/*(yRes-1)*/; y >= 1; y--)
					{
						atenuador = 1f;
						for (x = _x/*(xRes-1)*/; x >= 1; x--)
						{
							if (heights[x, y] > heights[x-1,y-1]) { heights[x-1, y-1] += (heights[x,y] - heights[x-1, y-1]) * ataque * atenuador * 0.59f; }
							atenuador -= gradoAtenuacion; if (atenuador <= 0) { break; }
						}
					}
					
					for (x = _x; x < (xRes-1); x++)
					{
						atenuador = 1f;
						for (y = _y/*(yRes-1)*/; y >= 1; y--)
						{
							if (heights[x, y] > heights[x+1,y-1]) { heights[x+1, y-1] += (heights[x,y] - heights[x+1, y-1]) * ataque * atenuador * 0.59f; }
							atenuador -= gradoAtenuacion; if (atenuador <= 0) { break; }
						}
					}
					
					for (x = _x/*(xRes-1)*/; x >= 1; x--)
					{
						atenuador = 1f;
						for (y = _y; y < (yRes-1); y++)
						{
							if (heights[x, y] > heights[x-1,y+1]) { heights[x-1, y+1] += (heights[x,y] - heights[x-1, y+1]) * ataque * atenuador * 0.59f; }
							atenuador -= gradoAtenuacion; if (atenuador <= 0) { break; }
						}
					}
				}

			return heights;
		}
			
			
			
			
		float[,] AlgoritmoSuavizarHoyo(float[,] heights, int _x, int _y, float ataque, float gradoAtenuacion, int pasadasSuavizado)
		{
			int x, y;
			float atenuador = 1f;

			for (int i = 0; i < pasadasSuavizado; i++)
			{
				
				// Pasadas rectas:
				
				for (y = _y; y < yRes; y++)
				{
					atenuador = 1f;
					for (x = _x; x < (xRes-1); x++)
					{
						if (heights[x, y] < heights[x+1,y]) { heights[x+1, y] -= (heights[x,y] - heights[x+1, y]) * ataque * atenuador; }
						atenuador -= gradoAtenuacion; if (atenuador <= 0) { break; }
					}
					
					atenuador = 1f;
					for (x = _x/*(xRes-1)*/; x >= 1; x--)
					{
						if (heights[x, y] < heights[x-1,y]) { heights[x-1, y] -= (heights[x,y] - heights[x-1, y]) * ataque * atenuador; };
						atenuador -= gradoAtenuacion; if (atenuador <= 0) { break; }
					}
				}
				
				for (x = _x; x < xRes; x++)
				{
					atenuador = 1f;
					for (y = _y; y < (yRes-1); y++)
					{
						if (heights[x, y] < heights[x,y+1]) { heights[x, y+1] -= (heights[x,y] - heights[x, y+1]) * ataque * atenuador; }
						atenuador -= gradoAtenuacion; if (atenuador <= 0) { break; }
					}
					
					atenuador = 1f;
					for (y = _y/*(yRes-1)*/; y >= 1; y--)
					{
						if (heights[x, y] < heights[x,y-1]) { heights[x, y-1] -= (heights[x,y] - heights[x, y-1]) * ataque * atenuador; }
						atenuador -= gradoAtenuacion; if (atenuador <= 0) { break; }
					}
				}
				
				
				// Pasadas oblicuas:
				
				for (y = _y; y < (yRes-1); y++)
				{
					atenuador = 1f;
					for (x = _x; x < (xRes-1); x++)
					{
						if (heights[x, y] < heights[x+1,y+1]) { heights[x+1, y+1] -= (heights[x,y] - heights[x+1, y+1]) * ataque * atenuador * 0.59f; }
						atenuador -= gradoAtenuacion; if (atenuador <= 0) { break; }
					}
				}
				
				for (y = _y/*(yRes-1)*/; y >= 1; y--)
				{
					atenuador = 1f;
					for (x = _x/*(xRes-1)*/; x >= 1; x--)
					{
						if (heights[x, y] < heights[x-1,y-1]) { heights[x-1, y-1] -= (heights[x,y] - heights[x-1, y-1]) * ataque * atenuador * 0.59f; }
						atenuador -= gradoAtenuacion; if (atenuador <= 0) { break; }
					}
				}
				
				for (x = _x; x < (xRes-1); x++)
				{
					atenuador = 1f;
					for (y = _y/*(yRes-1)*/; y >= 1; y--)
					{
						if (heights[x, y] < heights[x+1,y-1]) { heights[x+1, y-1] -= (heights[x,y] - heights[x+1, y-1]) * ataque * atenuador * 0.59f; }
						atenuador -= gradoAtenuacion; if (atenuador <= 0) { break; }
					}
				}
				
				for (x = _x/*(xRes-1)*/; x >= 1; x--)
				{
					atenuador = 1f;
					for (y = _y; y < (yRes-1); y++)
					{
						if (heights[x, y] < heights[x-1,y+1]) { heights[x-1, y+1] -= (heights[x,y] - heights[x-1, y+1]) * ataque * atenuador * 0.59f; }
						atenuador -= gradoAtenuacion; if (atenuador <= 0) { break; }
					}
				}
			}
		return heights;
			
		}
		

		/*float[,] AlgoritmoSuavizarTerreno(float[,] heights, float f)
		{
			int x, y;
			
			for (y = 0; y < yRes; y++)
			{
				for (x = 1; x < (xRes-2); x++)
				{
					if ((heights[x,y] < heights[x-1,y]) && (heights[x,y] > heights[x+1,y]))  // pendiente hacia abajo
					{
						heights[x-1,y] -= (heights[x-1,y] - heights[x,y])/2;
						heights[x+1,y] += (heights[x,y] - heights[x+1,y])/2;
					}
					if ((heights[x,y] > heights[x-1,y]) && (heights[x,y] < heights[x+1,y])) // pendiente hacia arriba
					{
						heights[x-1,y] += (heights[x,y] - heights[x-1,y])/2;
						heights[x+1,y] -= (heights[x+1,y] - heights[x,y])/2;
					}
					if ((heights[x,y] < heights[x-1,y]) && (heights[x,y] < heights[x+1,y]))  // zanja hacia abajo
					{
						heights[x-1,y] -= (heights[x-1,y] - heights[x,y])/2;
						heights[x+1,y] += (heights[x,y] - heights[x+1,y])/2;
					}
					if ((heights[x,y] > heights[x-1,y]) && (heights[x,y] > heights[x+1,y])) // monticulo hacia arriba
					{
						heights[x-1,y] += (heights[x,y] - heights[x-1,y])/2;
						heights[x+1,y] -= (heights[x+1,y] - heights[x,y])/2;
					}
				}
				
				for (x = xRes-1; x >= 1; x--)
				{
					if ((heights[x,y] < heights[x-1,y]) && (heights[x,y] > heights[x+1,y])) { heights[x-1,y] -= (heights[x-1,y] - heights[x,y])/2; }
					if ((heights[x,y] > heights[x-1,y]) && (heights[x,y] > heights[x+1,y])) { heights[x-1, y] += (heights[x,y] - heights[x-1,y])/2; }
				}
			}
			
			for (x = 0; x < xRes; x++)
			{
				for (y = 1; y < (yRes-2); y++)
				{
					if ((heights[x,y] < heights[x,y-1]) && (heights[x,y] > heights[x,y+1]))  // escalon hacia abajo
					{
						heights[x,y-1] -= (heights[x,y-1] - heights[x,y])/2;
						heights[x,y+1] += (heights[x,y] - heights[x,y+1])/2;
					}
					if ((heights[x,y] > heights[x,y-1]) && (heights[x,y] < heights[x,y+1])) // escalon hacia arriba
					{
						heights[x,y-1] += (heights[x,y] - heights[x,y-1])/2;
						heights[x,y+1] -= (heights[x,y+1] - heights[x,y])/2;
					}
				}
				
				for (y = yRes-1; y >= 1; y--)
				{
					if ((heights[x, y] < heights[x,y-1]) && (heights[x,y] > heights[x+1,y])) { heights[x,y-1] -= (heights[x,y-1] - heights[x,y])/2; }
					if ((heights[x, y] > heights[x,y-1]) && (heights[x,y] > heights[x+1,y])) { heights[x,y-1] += (heights[x,y] - heights[x,y-1])/2; }
				}
			}
			
			return heights;
		}*/
		
				
				
	}
}

