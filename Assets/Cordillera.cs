using System;
using UnityEngine;
using System.Collections;

//using GeneradorMundos;

namespace AssemblyCSharp
{
		public class Cordillera
		{
			TerrainData tData;
			float[,] heights;
			int xRes, yRes;
			
			int numPicos;
			int espaciadoPicos;
			int despLatPicos;
			int pasadasSuavPicos;
			float alturaPicos;
			float ataqSuavPicos;
			float gradoAtenCord;
			
			//AssemblyCSharp.Suavizado suavizado; // = new AssemblyCSharp.Suavizado(tData);
			BourkeV2 bourkev2;
		

			
			public Cordillera (TerrainData _tData)
			{
				tData = _tData;
				xRes = tData.heightmapWidth;
				yRes = tData.heightmapHeight;
				heights = tData.GetHeights(0, 0, xRes, yRes);
			}


			public TerrainData Crear()
			{
				tData = AlgoritmoCordillera(UnityEngine.Random.Range(10,50), UnityEngine.Random.Range(1,5), UnityEngine.Random.Range(1,5), UnityEngine.Random.Range(0.001f,0.01f), UnityEngine.Random.Range(0.2f,0.7f), 0.1f, 1);
				return tData;
			}


			public TerrainData Crear(int numPicos, int espaciadoPicos, int despLatPicos, float alturaPicos, float ataqSuavPicos, float gradoAtenCord, int pasadasSuavPicos)
			{
				tData = AlgoritmoCordillera(numPicos, espaciadoPicos, despLatPicos, alturaPicos, ataqSuavPicos, gradoAtenCord, pasadasSuavPicos);
				return tData;
			}



			
			TerrainData AlgoritmoCordillera(int numPicos, int espaciadoPicos, int despLatPicos, float alturaPicos, float ataqSuavPicos, float gradoAtenCord, int pasadasSuavPicos)
			{
				/*Debug.Log("numPicos: " + numPicos
						+ "; espaciado: " + espaciadoPicos
						+ "; desplLateral: " + despLatPicos
						+ "; alturaPicos: " + alturaPicos
						+ "; ataqueSuavizado: " + ataqSuavPicos
						+ "; gradoAtenuacion: " + gradoAtenCord
						+ "; pasadas Suav: " + pasadasSuavPicos);*/


				//suavizado = new AssemblyCSharp.Suavizado(tData);	
				
				// escogemos un punto del mapa al azar:
				int x = UnityEngine.Random.Range(50, (xRes-50));
				int y = UnityEngine.Random.Range(50, (yRes-50));
				//Debug.Log("Semilla: " + x + ", " + y);
				
				//escogemos una direccion en donde hacer crecer la cordillera:
				int dir_x = UnityEngine.Random.Range(-1, 1);
				int dir_y = UnityEngine.Random.Range(-1, 1);
				if ((dir_x == 0) && (dir_y == 0)) { dir_x = 1; } // Evitar que todo sean 0s.
				
				// Estocastico
				for (int i = 0; i < numPicos; i++)
				{
					x += UnityEngine.Random.Range(1, espaciadoPicos) * dir_x + UnityEngine.Random.Range(1, despLatPicos);
					y += UnityEngine.Random.Range(1, espaciadoPicos) * dir_y + UnityEngine.Random.Range(1, despLatPicos);
					
					if ((x < xRes) && (x > 0) && (y < yRes) && (y > 0))
					{
						bourkev2 = new BourkeV2(tData);
						tData = bourkev2.Crear(); // heights[x, y] += UnityEngine.Random.Range(alturaPicos/2, alturaPicos);
					
						// Aplicar un suavizado del pico generado:
						//heights = suavizado.SuavizarPico(heights, x, y, ataqSuavPicos, gradoAtenCord, pasadasSuavPicos);
					}
				}
					
				// terminar y devolver el nuevo mapa a main():
				tData.SetHeights(0, 0, heights);
				return tData;
						
			}
		}
}

