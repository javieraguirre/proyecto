using System;
using UnityEngine;
using System.Collections;

//using GeneradorMundos;


namespace AssemblyCSharp
{
		public class Rio
		{
				// Valores generales:
		
				TerrainData tData;
				float[,] heights;
				int xRes, yRes;
		
				AssemblyCSharp.Suavizado suavizado;

				int numHoyos;
				int espaciadoHoyos;
				int despLatHoyos;
				int pasadasSuavHoyos;
				float profHoyos;
				float ataqSuavHoyos;
				float gradoAtenDepr;

				int longitudRio;
				public float[,] puntoMasAlto;
		
				// Constructor:
				public Rio (TerrainData _tData)
				{
					tData = _tData;
					xRes = tData.heightmapWidth;
					yRes = tData.heightmapHeight;
					heights = tData.GetHeights(0, 0, xRes, yRes);

					longitudRio = UnityEngine.Random.Range(10, xRes-10);
				}
		

				public TerrainData Crear()
				{
					tData = AlgoritmoRio(UnityEngine.Random.Range(200, 300), UnityEngine.Random.Range(1, 2), UnityEngine.Random.Range(1, 2), UnityEngine.Random.Range(0.01f, 0.1f), 1, UnityEngine.Random.Range(0.5f, 0.9f), UnityEngine.Random.Range(0.01f, 0.1f));

					Depresion depresion = new Depresion(tData);
					depresion.Crear((int) puntoMasAlto[(longitudRio-2),0], (int) puntoMasAlto[(longitudRio-2),1]);

			        return tData;
				}

			    public TerrainData Crear(int numHoyos, int espaciadoHoyos, int despLatHoyos, float profHoyos, int pasadasSuavHoyos, float ataqSuavHoyos, float gradoAtenDepr)
				{
					tData = AlgoritmoRio(numHoyos, espaciadoHoyos, despLatHoyos, profHoyos, pasadasSuavHoyos, ataqSuavHoyos, gradoAtenDepr);
					return tData;
				}


		
				TerrainData AlgoritmoRio(int numHoyos, int espaciadoHoyos, int despLatHoyos, float profHoyos, int pasadasSuavHoyos, float ataqSuavHoyos, float gradoAtenDepr)
				{
			
					suavizado = new AssemblyCSharp.Suavizado(tData);
					
					// Primero buscar punto mas alto:

					puntoMasAlto = new float[xRes,3];

					/*for (int x = 0; x < xRes; x ++)
					{
							for (int y = 0; y < yRes; y ++)
							{
							if (heights[x,y] > puntoMasAlto[0,2])
									{
									puntoMasAlto[0,0] = x;
									puntoMasAlto[0,1] = y;
									puntoMasAlto[0,2] = heights[x,y];
									}
							}
					}
					Debug.Log("punto mas alto: x: " + puntoMasAlto[0,0] + "; y: " + puntoMasAlto[0,1] + "; z: " + puntoMasAlto[0,2]);
					*/

					// Escoger punto al azar:

					puntoMasAlto[0,0] = UnityEngine.Random.Range(longitudRio+1, (xRes-longitudRio-1));
					puntoMasAlto[0,1] = UnityEngine.Random.Range(longitudRio+1, (yRes-longitudRio-1));
			

					// Segundo, buscar puntos mas bajos colindantes con el punto anterior:

					for (int z = 1; z < longitudRio-1; z++)
					{
						//Debug.Log("nuevo punto: x: " + puntoMasAlto[z-1,0] + "; y: " + puntoMasAlto[z-1,1]);
						puntoMasAlto[z,2] = 3f;

						int i = UnityEngine.Random.Range(1, 10);
						if (i == 10)
							{
							Rio rio = new Rio(tData);
							rio.Crear(numHoyos, espaciadoHoyos, despLatHoyos, profHoyos, pasadasSuavHoyos, ataqSuavHoyos, gradoAtenDepr);
							}
				
						for (int x = ((int) puntoMasAlto[z-1,0]-1); x < ((int) puntoMasAlto[z-1,0]+2); x++)
						{
							for (int y = ((int) puntoMasAlto[z-1,1]-1); y < ((int) puntoMasAlto[z-1,1]+2); y++)
							{
							//Debug.Log("x: " + x + "; y: " + y + "; z: " + z);
						
							if (heights[x,y] < puntoMasAlto[z,2])
									{
									puntoMasAlto[z,0] = x;
									puntoMasAlto[z,1] = y;
									puntoMasAlto[z,2] = heights[x,y];						
									}
							}
						}
					}

					// Aplicar cambios:

					for (int z = 0; z < xRes; z++)
					{
							heights[(int)puntoMasAlto[z,0],(int)puntoMasAlto[z,1]] -= UnityEngine.Random.Range(0.01f, 0.05f);
					}
	
					tData.SetHeights(0, 0, heights);
					return tData;
				}
				
		}
}

