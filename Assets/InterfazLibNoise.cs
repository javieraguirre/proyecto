using UnityEngine;
using System.Collections;
using AssemblyCSharp;
using System;

public class InterfazLibNoise : MonoBehaviour
{
		Terrain terrain;
		TerrainData tData;
		float[,] heights;
		int xRes, yRes;

		bool mostrarGUI = true;
		string tituloGUI = "Generadores de ruido coherente de LibNoise";

		Main main;

		object tecnicaGeneradora;

		float ataque = 0.3f;		string sAtaque;
		//int conservar = 0;			string sConservar;
		int operacion = 0;			string sOperacion;
		float variabilidad = 1f;	string sVariabilidad;
	
	
	
		// Use this for initialization
		void Start ()
		{
			this.name = "objetoActivo";
			
			// referencia a Main para guardar y recuperar parametros:
			main = GameObject.Find("Terrain").GetComponent<Main>();
			// referencia a Main para acceder al terreno:
			terrain = GameObject.Find("Terrain").GetComponent<Terrain>();
			
			tData = terrain.terrainData;
			xRes = tData.heightmapWidth;
			yRes = tData.heightmapHeight;
			heights = tData.GetHeights(0, 0, xRes, yRes);

			tecnicaGeneradora = null;

			sAtaque = ataque.ToString();
			//sConservar = conservar.ToString();
			sOperacion = operacion.ToString();
			sVariabilidad = variabilidad.ToString();
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		void OnGUI()
		{
			if (Event.current.Equals(Event.KeyboardEvent(main.teclaMenu)))
			{
				if (mostrarGUI)
				{
					mostrarGUI = false;
				}
				else if (!mostrarGUI)
				{
					mostrarGUI = true;
				}
			}
		
			if (mostrarGUI)
			{
					int m = 20;

					GUI.color = Color.yellow;
					GUI.Box(new Rect(250, 10, tituloGUI.Length*6+20, 20), tituloGUI);
					GUI.color = Color.white;

			
					if (GUI.Button(new Rect(250, m*2, 120, m), "Billow (ondas)"))
					{
						tecnicaGeneradora = new LibNoise.Generator.Billow();
						iniciarNoise2d();
					}
					if (GUI.Button(new Rect(250, m*3, 120, m), "Checker"))
					{
						tecnicaGeneradora = new LibNoise.Generator.Checker();
						iniciarNoise2d();		
					}
					if (GUI.Button(new Rect(250, m*4, 120, m), "Const"))
					{
						tecnicaGeneradora = new LibNoise.Generator.Const();	
						iniciarNoise2d();
					}
					if (GUI.Button(new Rect(250, m*5, 120, m), "Cylinders"))
					{
						tecnicaGeneradora = new LibNoise.Generator.Cylinders();
						iniciarNoise2d();
					}
					if (GUI.Button(new Rect(250, m*6, 120, m), "Perlin"))
					{
						tecnicaGeneradora = new LibNoise.Generator.Perlin();
						iniciarNoise2d();
					}
					if (GUI.Button(new Rect(250, m*7, 120, m), "RidgedMultifractal"))
					{
						tecnicaGeneradora = new LibNoise.Generator.RidgedMultifractal();
						iniciarNoise2d();
					}
					if (GUI.Button(new Rect(250, m*8, 120, m), "Spheres"))
					{
						tecnicaGeneradora = new LibNoise.Generator.Spheres();
						iniciarNoise2d();
					}
					if (GUI.Button(new Rect(250, m*9, 120, m), "Voronoi"))
					{
						tecnicaGeneradora = new LibNoise.Generator.Voronoi();
						iniciarNoise2d();
					}

					GUI.Box(new Rect(390, m*2, 200, 20), "Parametros: ");

					GUI.Box(new Rect(390, m*3, 160, 20), "Multiplicador de altura: ");
					sAtaque = GUI.TextField(new Rect(550, m*3, 40, 20), sAtaque, 3);

					GUI.Box(new Rect(390, m*4, 160, 20), "Variabilidad: ");
					sVariabilidad = GUI.TextField(new Rect(550, m*4, 40, 20), sVariabilidad, 3);

					/*GUI.Box(new Rect(390, m*6, 160, 20), "Conservar terreno previo: ");
					sConservar = GUI.TextField(new Rect(550, m*6, 40, 20), sConservar, 3);*/

					GUI.Box(new Rect(390, m*6, 160, 20), "Combinar terrenos: ");
					sOperacion = GUI.TextField(new Rect(550, m*6, 40, 20), sOperacion, 3);
					GUI.Box(new Rect(390, m*7, 200, 20), "0: no; 1: suma; 2: mult");
			
			
			
			
			
			}
		}


		public void iniciarNoise2d(float _ataque, int _operacion, float _variabilidad)
		{
			ataque = _ataque;
			operacion = _operacion;
			variabilidad = _variabilidad;

			iniciarNoise2d();
		}



		void iniciarNoise2d()
		{
			// Consolidar parametros escritos por el usuario:
			ataque = float.Parse(sAtaque);
			//conservar = int.Parse(sConservar);
			operacion = int.Parse(sOperacion);
			variabilidad = float.Parse(sVariabilidad);

			LibNoise.Noise2D noise2d = new LibNoise.Noise2D(xRes, yRes, (LibNoise.ModuleBase) tecnicaGeneradora);
			//noise2d.GeneratePlanar(UnityEngine.Random.Range(-variabilidad, 0f), UnityEngine.Random.Range(0f, variabilidad), UnityEngine.Random.Range(-variabilidad, 0f), UnityEngine.Random.Range(0f, variabilidad), true);
			noise2d.GeneratePlanar(-variabilidad, variabilidad, -variabilidad, variabilidad, true);

			// guardar el mapa de ruido creado con Noise2D en un heightmap auxiliar:
			float[,] preHeights = new float[xRes, yRes];
			preHeights = noise2d.GetData(true, 0, 0, true);

			// aplicar un multiplicador al heightmap:
			for (int x = 0; x < xRes; x ++)
			{
				for (int y = 0; y < yRes; y ++)
				{
					if (operacion == 0) { heights[x,y] = preHeights[x,y] * ataque; }
					if (operacion == 1) { heights[x,y] += preHeights[x,y] * ataque; }
					if (operacion == 2) { heights[x,y] *= preHeights[x,y] * ataque; }

					/*if (conservar == 0) { heights[x,y] = preHeights[x,y] * ataque; }
					else if ((conservar == 1) && (operacion == 0)) { heights[x,y] += preHeights[x,y] * ataque; }
					else if ((conservar == 1) && (operacion == 1)) { heights[x,y] *= preHeights[x,y] * ataque; }*/
				}
			}

			// consolidar heightmap:
			tData.SetHeights(0,0, heights);
			
			tecnicaGeneradora = null; noise2d = null; GC.Collect();
		}
}

