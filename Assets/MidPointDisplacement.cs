using System;
using UnityEngine;
using System.Collections;

//using GeneradorMundos;

namespace AssemblyCSharp
{
		public class MidPointDisplacement
		{

				// Valores generales:
		
				TerrainData tData;
				float[,] heights;
				int xRes, yRes;			
		
				int puntosAltura;// = 8;
				int longitudPasos;// = 0;
				int numPasadas;// = 0;
				float multiplicadorAltura;// = 2f;
				
				float alturaMinima;// = 0.1f;
				float alturaMaxima;// = 0.6f;


				// Constructor:
				public MidPointDisplacement (TerrainData _tData)
				{
					tData = _tData;
					xRes = tData.heightmapWidth;
					yRes = tData.heightmapHeight;
					heights = tData.GetHeights(0, 0, xRes, yRes);
				}


				public TerrainData Crear(int puntosAltura, float multiplicadorAltura, float alturaMinima, float alturaMaxima)
				{
					tData = AlgoritmoMPDisp(puntosAltura, multiplicadorAltura, alturaMinima, alturaMaxima);
					return tData;
				}


				public TerrainData Crear()
				{
					tData = AlgoritmoMPDisp(UnityEngine.Random.Range(8, 64), UnityEngine.Random.Range(0.5f, 2f), UnityEngine.Random.Range(0.2f, 0.5f), UnityEngine.Random.Range(0.8f, 1f));
					return tData;
				}


				// Algoritmo:
				TerrainData AlgoritmoMPDisp(int puntosAltura, float multiplicadorAltura, float alturaMinima, float alturaMaxima)
				{

					/*
					Tecnica de Fournier, fussel y Carpenter en el SIGGRAPH de 1982
					
					http://www.gameprogrammer.com/fractal.html
					http://en.wikipedia.org/wiki/Diamond-square_algorithm
					*/
					
					numPasadas = (int) Mathf.Log(puntosAltura, 2); // 2 elevado a puntosAltura
					longitudPasos = ((xRes-1) / puntosAltura);
					//Debug.Log("xRes: " + (xRes-1) + ";\tnumPasadas: " + numPasadas + ";\tlongitudSaltos: " + longitudPasos);
					
					float[,] heights = tData.GetHeights(0, 0, xRes, yRes);
					
					/*float[,] heightsMultiplicador;
					if(!generarNuevo) { heightsMultiplicador = tData.GetHeights(0,0,xRes,yRes); }*/
					
					
					// Reiniciar la altura del terreno a 0:
					
					for (int y = 0; y < yRes; y++)
					{
						for (int x = 0; x < xRes; x++)
						{
							heights[x,y] = 0f;
						}
					}
					
					int cursor = 0;
					float alturaActual = 0.01f;
					
					
					// Paso 1: crear puntos de altura:
					
					for (int x = 0; x < xRes; x = x + longitudPasos)
					{
						cursor = 0;
						
						// Puntos inicial y final de cada fila:
						heights[x, yRes-1] = 0.1f;
						heights[x, 0] = 0.1f;				
						
						for (int i = 0; i < numPasadas; i++)
						{
							cursor = 0;
					
							
							for (int y = 0; y < yRes; y++)
							{
								if (heights[x,y] > 0.01f)
								{
									//Debug.Log("x: " + x + "; y: " + y + "; cursor: " + cursor + "; y - cursor: " + (y - cursor) + "; y - cursor / 2: " + (int) (y - cursor)/2);
									alturaActual = UnityEngine.Random.Range(alturaMinima, alturaMaxima) * multiplicadorAltura;
									heights[x, (int) (cursor + ((y - cursor) / 2))] = alturaActual;
									cursor = y;
								}
							}	
						}
					}
					
					
					// PAso 2: rellenar huecos entre puntos de altura:
					// Pasada horizontal:
					
					for (int x = 0; x < xRes; x = x + longitudPasos)
					{
						cursor = 0;
						
						for (int y = 0; y < yRes; y++)
						{
							if (heights[x,y] > 0.01f)
							{
								/*	cursor: pos inicial
										y: pos final
										
										tg B = altura / base
									
										heights[x,y] = altura
									*/	
								// B = arctg (heights[x,y] / y - cursor); // ¿grados, gradianes, radianes?
								
								
								if (heights[x,cursor] < heights[x,y])
								{
									float B = Mathf.Atan2((heights[x,y] - heights[x,cursor]), y - cursor);
									
									for (int _y = cursor /*(cursor+1)*/; _y < y /*(y-1)*/; _y++)
									{
										heights[x,_y] = heights[x,cursor] + Mathf.Tan(B) * (_y - cursor);
									}
								}
								
								if (heights[x,cursor] > heights[x,y])
								{
									float B = -1 * Mathf.Atan2((heights[x,cursor] - heights[x,y]), y - cursor);
									
									for (int _y = cursor /*(cursor+1)*/; _y < y/*(y-1)*/; _y++)
									{
										heights[x,_y] = heights[x,cursor] + (Mathf.Tan(B) * (_y - cursor));
									}
								}
								//if (heights[x,cursor] == heights[x,y])	{	B = 0f; /*Debug.Log("x,y: " + x + ", " + y + "\tB: " + B + "\tTan(B): " + Mathf.Tan(B));*/ }
								
								cursor = y;
							}
							
						}
						
					}
					
					// Pasada vertical:
					
					for (int y = 0; y < yRes; y++)
					{
						cursor = 0;
						
						for (int x = 0; x < xRes; x++)
						{
							if (heights[x,y] > 0.01f)
							{
								if (heights[cursor,y] < heights[x,y]) // (heights[x,y] > heights[cursor,y])
								{
									float B = Mathf.Atan2((heights[x,y] - heights[cursor,y]), x - cursor);
									
									for (int _x = cursor; _x < x; _x++)
									{
										heights[_x,y] = heights[cursor,y] + Mathf.Tan(B) * (_x - cursor);
									}
								}
								
								if (heights[cursor,y] > heights[x,y]) // (heights[x,y] < heights[cursor,y])
								{
									float B = -1 * Mathf.Atan2((heights[cursor,y] - heights[x,y]), x - cursor);
									
									for (int _x = cursor; _x < x; _x++)
									{
										heights[_x,y] = heights[cursor,y] + (Mathf.Tan(B) * (_x - cursor));
									}
								}
								
								cursor = x;					
							}
						}
					}
					
					
					tData.SetHeights(0, 0, heights);
					return tData;
				}
			}
	
		}


