using UnityEngine;
using System.Collections;
using AssemblyCSharp;
using System;


public class SecuenciadorL : MonoBehaviour
{
		bool mostrarGUI = true;
		string tituloGUI = "Interprete y secuenciador de comandos L-System";
		bool generar = false;
		
		Main main;
		MidPointDisplacementV3 mpd;

		Terrain terrain;
		TerrainData tData;
		float[,] heights;
		int xRes, yRes;	

		int iteraciones = 1;	string sIter = "";

		string secuencia = "aba;bab;aba";	string sSecuencia;

		char[] vars;
		string[] reglas;

		int contador = 0, x = 0;
		int m = 20;
	
	
		string[] arrayFrases = null;
		string[][] arraySimbolos = null;
		char [][] arrayCaracteres = null;
		int filas0 = 0, columnas0 = 0;

		float altura = 0;
		float altura0 = 0.5f;			string sAltura0 = "0.5";


		// Use this for initialization
		void Start ()
		{
			this.name = "objetoActivo";
		
			main = GameObject.Find("Terrain").GetComponent<Main>();
			main.generarNuevoMDP = false;

			terrain = GameObject.Find("Terrain").GetComponent<Terrain>();
			tData = terrain.terrainData;
			
			xRes = tData.heightmapWidth;
			yRes = tData.heightmapHeight;
			heights = tData.GetHeights(0, 0, xRes, yRes);

			vars = new char[3];
			reglas = new string[vars.Length];

			vars[0] = 'a';		reglas[0] = "a0a";
			vars[1] = 'b';		reglas[1] = "b0b";
			vars[2] = '0';		reglas[2] = "0ab";
/*			vars[3] = "+";		reglas[3] = "";
			vars[4] = "-";		reglas[4] = "";
			vars[5] = "*";		reglas[5] = "";*/

			sSecuencia = secuencia;
			sIter = "2";
		}

	
		// Update is called once per frame
		void Update ()
		{
			if (generar)
			{
				float it = iteraciones*iteraciones;

				// Plasmar las instrucciones de produccion en el terreno, a medida que exploro el array de simbolos:
				if (x < filas0)
				{
					columnas0 = arrayCaracteres[x].Length;
					for (int y = 0; y < columnas0; y ++)
					{
						try
						{
							if (arrayCaracteres[x][y] == 'a')
							{
								altura = altura0 / it;
								//Debug.Log("A\tx: " + x + "; y: " + y + "; altura: " + altura + "; arraux2[x][y]: " + arraux2[x][y]);
							}
							else if (arrayCaracteres[x][y] == 'b')
							{
								altura = (- altura0) / it;
								//Debug.Log("B\tx: " + x + "; y: " + y + "; altura: " + altura + "; arraux2[x][y]: " + arraux2[x][y]);
							}
							else if (arrayCaracteres[x][y] == '0')
							{
								altura = 0;
								//Debug.Log("0\tx: " + x + "; y: " + y + "; altura: " + altura + "; arraux2[x][y]: " + arraux2[x][y]);
							}
						}
						catch (IndexOutOfRangeException e)
						{
							Debug.Log("Excepcion indice fuera de rango interpretando array0, con x: " + x + ", y: " + y + ", siendo columnas0: " + columnas0 + ", filas0: " + filas0 + "\n" + e.ToString());
						}
						//Debug.Log("arraySimbolos[x].Length: " + arraySimbolos[x].Length);
						mpd.AlgoritmoMPD_L(x, arrayCaracteres.Length, y, arrayCaracteres[x].Length, 1f, altura*0.9f, altura*1.1f);
					}		
					x ++;
				}
				// Interpretar las reglas e ir creando la cadena a su vez:
				else
				{
					//mpd.rellenadoV_L();
					// Ahora toca crear el nuevo array para la siguiente iteracion:
					
					//int filas1 = (int) Mathf.Pow(filas0,2f);
					int filas1 = filas0 * 2;
					int columnas1;// = (int) Mathf.Pow(columnas0,2f);

					//string[,] array1 = new string[filas1, columnas1];
					char[][] array1 = new char[filas1] [];
					
					//Debug.Log("filas1: " + filas1 + ";\tcolumnas1: " + columnas1);
					int _z = 0, _y = 0;
					if (filas1 > xRes) { filas1 = (xRes+1); }

					for (int z = 0; z < filas1; z ++)
					{
						//columnas1 = (int) Mathf.Pow(arrayCaracteres[(int) (z / arrayCaracteres.Length)].Length,2);
						columnas1 = (int) arrayCaracteres[(int) (z / arrayCaracteres.Length)].Length * 2;
						array1[z] = new char[columnas1];

						if (columnas1 > yRes) { columnas1 = (yRes+1); }
						//Debug.Log("columnas1 : " + columnas1);
						for (int y = 0; y < columnas1; y ++)
						{
							/*Debug.Log("z: " + z + "\ny: " + y + "\nfilas0: " + filas0 + "\ncolumnas0: " + columnas0 + "\nfilas1: " + filas1 + "\ncolumnas1: " + columnas1 + "\ny/filas0: " + ((float) y / filas0)
									+ "\nz/filas0: " + ((int) z / filas0) + "\ny/columnas0: " + ((int) y / columnas0) + "\narraySimbolos.Length: " + arraySimbolos.Length
										+ "\narraySimbolos[z/filas0][y/arraySimbolos[z].Length: " + arraySimbolos[z/filas0] [(int) (y / arraySimbolos[z/filas0].Length)]);*/
							//Debug.Log(arraySimbolos[0] [(int) (y/columnas0)].ToString());
							//array1[x,y] = arraux2[(int) (x / filas0)] [(int) (y / columnas0)]; // Esta linea hace la magia de duplicar el valor de un elemento de array0 en muchos elementos en array1.
							array1[z][y] = InterpretarReglas( ((float) y / filas0), arrayCaracteres[(int) (z/filas0)] [(int) (y / arrayCaracteres[z/filas0].Length)]);
							
							secuencia = secuencia.Insert(secuencia.Length, (array1[z][y]).ToString());
							//Debug.Log("y : " + y + "\t_y: " + _y);
							_y ++;
						}
						secuencia = secuencia.Insert(secuencia.Length, ';'.ToString()); // añado la ';' que separa las frases.

						_z ++;
					}

					//Debug.Log("_z: " + _z + "; _y/_z: " + (_y / _z));

					// Terminar la iteracion:

					x = 0;
					//Debug.Log("Secuencia:\n" + secuencia);
	
					iteraciones ++;

					//Debug.Log("iteraciones: " + iteraciones + ";\tsIter: " + sIter.ToString());
					if (iteraciones > int.Parse(sIter))
					{
						generar = false;
					}
					else { IniciarVariables(); }
				}
			}
			else { sSecuencia = secuencia; }
		}


		void OnGUI()
		{
			if (mostrarGUI)
			{
				GUI.Box(new Rect(250, 10, tituloGUI.Length*6+20, 25), tituloGUI);

				GUI.Box(new Rect(250, m*2, 120, m), "Axioma: ");
				secuencia = GUI.TextField(new Rect(370, m*2, 170, m), secuencia, int.MaxValue);

				for (int i = 0; i < vars.Length; i ++)
				{
					GUI.Box(new Rect(250, m*(i + 3), 120, m), "Variable " + i + ": ");
					vars[i] = GUI.TextField(new Rect(370, m*(i + 3), 25, m), vars[i].ToString(), 1)[0];
					GUI.Box(new Rect(395, m*(i + 3), 25, m), "--> ");
					reglas[i] = GUI.TextField(new Rect(420, m*(i + 3), 120, m), reglas[i], 50);
				}

				GUI.Box(new Rect(250, m*(vars.Length + 3), 120, m), "Altura inicial: ");
				sAltura0 = GUI.TextField(new Rect(370, m*(vars.Length + 3), 170, m), sAltura0, 5);
			
				GUI.Box(new Rect(250, m*(vars.Length + 4), 140, m), "Nº iteraciones: ");
				sIter = GUI.TextField(new Rect(390, m*(vars.Length + 4), 40, m), sIter, 3);


				/*if (GUI.Button(new Rect(250, m*(vars.Length + 7), 120, m), "prototipo TerrainGenerator"))
				{
					TerrainGenerator tg = new TerrainGenerator();
				}*/

				
				if (!generar)
				{
					if (GUI.Button(new Rect(250, m*(vars.Length + 6), 120, m), "Generar"))
					{
						altura0 = float.Parse(sAltura0);

						mpd = new MidPointDisplacementV3(tData);
					
						IniciarVariables();
						
						x = 0; iteraciones = 1;

						generar = true;
					}
				}
				else if (generar)
				{			
					if (GUI.Button(new Rect(250, m*(vars.Length + 6), 120, m), "Detener"))
					{
						mpd = null;

						generar = false;
					}
				}
			}
		
			if (Event.current.Equals(Event.KeyboardEvent(main.teclaMenu)))
			{
				if (mostrarGUI) { mostrarGUI = false; }
				else if (!mostrarGUI) {	mostrarGUI = true; }
			}
		}
	


		/*
		Funcion al que se le entrega la letra que se esta contemplando en el array,
		 y te devuelve a cambio la letra que se supone que se debe de poner.
		Es aqui donde se exploran las variables y sus reglas de cambio.
		*/

		char InterpretarReglas(float numero, char letraActual)
		{
			char letraParaDevolver = '0';
		
			if ((numero % 1) == 0) { contador = 0; }
	
			try
			{
				for (int i = 0; i < vars.Length; i ++)
				{
					if (vars[i] == letraActual)
						{
						//string[] letrasRegla = reglas[i].Split(',');
						char[] letrasRegla = reglas[i].ToCharArray();
	
						//Debug.Log("letrasRegla: " + letrasRegla.Length + "\tcontador: " + contador);
						letraParaDevolver = letrasRegla[contador];
	
						if (contador < (letrasRegla.Length-1)) { contador ++; }
						else { contador = 0; }
						}
				}
			}
			catch (IndexOutOfRangeException e)
			{
				Debug.Log(e);
			}

			return letraParaDevolver;
		}


		void IniciarVariables()
		{
			// Quitamos el ultimo ";" de la secuencia, para que el string.Split(";") no me genere una fila de mas.
			//Debug.Log(secuencia.ToString());
			try { secuencia = secuencia.Remove(secuencia.Length - 1); }
			catch (ArgumentOutOfRangeException e) { Debug.Log("secuencia.Length: " + secuencia.Length + "\n" + e.ToString() + "\n" + secuencia.ToString()); }

			// Separo la secuencia en tantas subsecuencias como lineas horizontales hay:
			arrayFrases = secuencia.Split(';');
			// ahora tengo un array de strings, cada string con una subsecuencia distinta dedicada a una linea horizontal distinta.
			secuencia = "";
			
			// En este momento ya sabemos que dimensiones tendra el array principal: tantas lineas horizontales como subsecuencias, y tantas lineas verticales como lineas horizontales tenga, para que sea simetrico (la simetria es un requisito impuesto por el programador).
			arrayCaracteres = new char[arrayFrases.Length] [];
			
			for (int i = 0; i < arrayFrases.Length; i ++)
			{
				arrayCaracteres[i] = arrayFrases[i].ToCharArray();
			}
			
			// arraux2 es el array0 que contiene las instrucciones a interpretar, en forma de strings de un solo caracter.
			filas0 = arrayCaracteres.Length; // tambien podria ser: arraySimbolos.Length
			//columnas0 = arraySimbolos[0].Length;
		}


	}

