using System;
using UnityEngine;
using System.Collections;


namespace AssemblyCSharp
{
	public class CordilleraV2 : MonoBehaviour
	{
		TerrainData tData;
		float[,] heights;
		float[,] heights2;
		int xRes, yRes;
		
		int numPicos;
		int pasadasSuavPicos;
		float alturaPicos;
		float ataque;
		float gradoAtenCord;

		int x, y;
		int angulo0;
		float angulo;
		int excepciones = 0;

		public CordilleraV2 (TerrainData _tData)
		{
			tData = _tData;
			xRes = tData.heightmapWidth;
			yRes = tData.heightmapHeight;
			heights2 = new float[xRes, yRes]; //= tData.GetHeights(0, 0, xRes, yRes);
			heights = tData.GetHeights(0,0,xRes,yRes);


		}
		
		
		public TerrainData Crear()
		{
			tData = AlgoritmoCordilleraV2(UnityEngine.Random.Range(1,2), UnityEngine.Random.Range(0.01f,0.01f), UnityEngine.Random.Range(0.2f,0.7f), 10f, 15f, 2, 0, 150);
			tData.SetHeights(0, 0, heights);
			return tData;
		}
		
		
		public TerrainData Crear(int numPicos, float alturaPicos, float ataqSuavPicos, float hipMin, float hipMax, int radioInicio, int radioFinal)
		{
			tData = AlgoritmoCordilleraV2(numPicos, alturaPicos, ataqSuavPicos, hipMin, hipMax, 2, radioInicio, radioFinal);
			tData.SetHeights(0, 0, heights);
			return tData;
		}
		
		
		
		
		TerrainData AlgoritmoCordilleraV2(int numPicos, float alturaPicos, float ataque, float hipMin, float hipMax, int ramas, int radioInicio, int radioFinal)
		{
			// escogemos un punto del mapa al azar:
			x = UnityEngine.Random.Range(50, (xRes-50));
			y = UnityEngine.Random.Range(50, (yRes-50));
			
			// escogemos un angulo al azar:
			angulo = UnityEngine.Random.Range(1, 90);

			//Debug.Log("x: " + x + "; y: " + y + "; angulo: " + angulo);
			

			/*Action work = delegate	// metodo anonimo
			{*/
				// algoritmo per se:
				for (int i = 0; i < numPicos; i++)
				{
					// escoger hipotenusa y lado adyacente aleatorios:
					float hipotenusa = UnityEngine.Random.Range(hipMin, hipMax);

					float adyacente = Mathf.Cos (angulo) * hipotenusa;
					x = x + (int) adyacente;
					float opuesto = Mathf.Sin (angulo) * hipotenusa;
					y = y + (int) opuesto;

					try
						{
						if ((heights2[x,y] < alturaPicos))
							{
							heights2[x,y] += alturaPicos * UnityEngine.Random.Range(0.8f, 1.2f);
							heights2 = AlgoritmoSuavizarPico2(heights2, x, y, ataque, radioInicio, UnityEngine.Random.Range((radioFinal/3), radioFinal), angulo);
							}
						}
					catch (IndexOutOfRangeException) { excepciones ++; }


					// escogemos si crear una ramificacion o no en la cordillera:
					//if (UnityEngine.Random.Range(1, 3) == 1)
					/*{
						//Debug.Log("Creando nueva rama");
						try
						{
							AlgoritmoCordilleraV2(x, y, (angulo + UnityEngine.Random.Range(-1f, 1f)), (numPicos-1), (alturaPicos*0.7f), ataque, hipMin, hipMax, 0, radioInicio, radioFinal);
							Debug.Log("Ok");
						}
						catch (IndexOutOfRangeException) { excepciones++; }
					}*/

					// escogemos un angulo al azar:
					angulo += UnityEngine.Random.Range(-0.5f, 0.5f);
				}
			/*};
			work();*/

			//Debug.Log("Excepciones encontradas: " + excepciones);

			// sumar los 2 planos:
			for(int i = 0; i < xRes; i ++)
			{
				for (int j = 0; j < yRes; j ++)
				{
					heights[i,j] += heights2[i,j];
					//heights[i,j] += heights2[i,j];
				}
			}


			// terminar y devolver el nuevo mapa a main():
			return tData;
			
		}


		float[,] AlgoritmoSuavizarPico2(float[,] heights2, int x, int y, float ataque, int radioInicio, int radioFinal, float anguloInicial)
		{
			// metodo de suavizado en el que se ataca al terreno desde el centro hacia afuera en linea recta, para todos los angulos alrededor del centro

			int _x = x;
			int _y = y;
			float alturaInicial = heights2[x,y];
			float alturaActual;

			// suavizar desde el centro hacia afuera:

			for (float ang = 0; ang < 359f; ang = ang + 1f)
			{
				alturaActual = alturaInicial;
				

				for (int radio = radioInicio; radio < UnityEngine.Random.Range((int) (radioFinal/3),radioFinal); radio++)
				{
					x = (int) (_x + radio * Mathf.Cos(ang)); // obtener lado adyacente
					y = (int) (_y + radio * Mathf.Sin(ang));

					try
					{
						if ((heights2[x,y] < alturaActual))
							{
								alturaActual = alturaActual * UnityEngine.Random.Range(ataque-0.0001f, ataque+0.0001f);// * (0.5f + (Mathf.Sin(ang)*(Mathf.Cos(ang))));
								heights2[x,y] = alturaActual;
							}
					}
					catch (IndexOutOfRangeException)
					{
						excepciones++;
					}
				}
			}

			// suavizar desde el fuera hacia adentro (para crear las calderas de los crateres):

			for (float ang = 0; ang < 359; ang = ang + 0.1f)
			{
				alturaActual = alturaInicial;
				
				
				for (int radio = radioInicio; radio > 0; radio--)
				{
					x = (int) (_x + radio * Mathf.Cos(ang));
					y = (int) (_y + radio * Mathf.Sin(ang));
					
					try
					{
						if ((heights2[x,y] < alturaActual))
						{
							alturaActual = alturaActual * UnityEngine.Random.Range(ataque-0.001f, ataque+0.001f);
							heights2[x,y] = alturaActual;
						}
					}
					catch (IndexOutOfRangeException)
					{
						excepciones++;
					}
				}
			}



			/*x = x;
			_y = y;
			alturaActual = heights[x, y];

			for (int radio = radioInicio; radio < radioFinal; radio++)	// distancia al punto
				{
					// calculo la nueva altura: altura original * 0.95
					alturaActual = alturaActual * ataque;
					
					for (int angulo = 0; angulo < 359; angulo++)	// girar alrededor del punto
					{
						x = (int) (_x + radio * Mathf.Cos(angulo));
						y = (int) (_y + radio * Mathf.Sin(angulo));

						try
						{
							if ((heights[x,y] < alturaActual))
							{
								heights[x,y] = alturaActual;
							}
							else { continue; }

						}
						catch (IndexOutOfRangeException) { excepciones ++; }
					}
				}*/


			//tData.SetHeights(0, 0, heights);
			return heights2;
		}
	}
}

