using System;
using UnityEngine;
using System.Collections;

namespace AssemblyCSharp
{
		public class Rugosidad
		{

				TerrainData tData;
				float[,] heights;
				int xRes, yRes;

				float rugosidadSuelo;


				public Rugosidad (TerrainData _tData)
				{
						tData = _tData;
						xRes = tData.heightmapWidth;
						yRes = tData.heightmapHeight;
						heights = tData.GetHeights(0, 0, xRes, yRes);
				}


				public TerrainData Crear()
				{
						
						tData = AlgoritmoRugosidad(UnityEngine.Random.Range(0.005f, 0.01f));
						return tData;
				}


				public TerrainData Crear(float rugosidadSuelo)
				{
						tData = AlgoritmoRugosidad(rugosidadSuelo);
						return tData;
					
				}


				TerrainData AlgoritmoRugosidad(float rugosidadSuelo)
				{

						for (int y = 0; y < yRes; y++)
						{
							for (int x = 0; x < xRes; x++)
							{				
								heights[x,y] += UnityEngine.Random.Range(0, rugosidadSuelo);
							}
						}

				tData.SetHeights(0, 0, heights);
				return tData;

				}
		}
}

