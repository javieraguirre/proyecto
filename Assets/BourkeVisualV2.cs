﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class BourkeVisualV2 : MonoBehaviour
	{
	//public Terrain terrain;
	Terrain terrain;
	TerrainData tData;
	float[,] heights;
	int xRes, yRes;
	
	public float alturaInicial;				string sAltura;
	public int iterBourke;					string sIter;
	public float ataqueBourke;				string sAtaque;
	public bool generarNuevoBourke = false;	string sNuevo;
	public float incrBourke;				string sIncr;
	public float decrBourke;				string sDecr;
	public int dependenciaInclinacion;		string sDependenciaInclinacion;
	float potenciaInclinacion = 1f;			string sPotenciaInclinacion;
	float factorInclinacion = 1f;
	public float alturaUmbral = 0f;			string sAlturaUmbral;
	public float alturaCota;				string sAlturaCota;
	public int velocidad = 1;				string sVelocidad;

	int x, y, dir_x, dir_y, counter = 0;
	bool mostrarGUI = true;

	Contador contadorUniversal;
	bool generar = false;

	string tituloGUI = "Generador tipo Bourke ver.2:";

	Main main;


	// Use this for initialization
	void Start ()
	{
		this.name = "objetoActivo";

		terrain = GameObject.Find("Terrain").GetComponent<Terrain>();

		tData = terrain.terrainData;
		xRes = tData.heightmapWidth;
		yRes = tData.heightmapHeight;
		heights = tData.GetHeights(0, 0, xRes, yRes);

		contadorUniversal = GameObject.Find("Contador").GetComponent<Contador>();
		contadorUniversal.contador = 0;

		// referencia a Main para guardar y recuperar parametros:
		main = GameObject.Find("Terrain").GetComponent<Main>();
		// recuperar valores memorizados en main:
		iterBourke = main.iterBourke;
		alturaInicial = main.alturaInicialBourke;
		ataqueBourke = main.ataqueBourke;
		generarNuevoBourke = main.generarNuevoBourke;
		incrBourke = main.incrBourke;
		decrBourke = main.decrBourke;
		dependenciaInclinacion = main.dependenciaInclinacion;
		potenciaInclinacion = main.potenciaInclinacion;
		alturaUmbral = main.alturaUmbral;
		alturaCota = main.alturaCota;
		velocidad = main.velocidad;

		sIter = iterBourke.ToString();
		sAltura = alturaInicial.ToString();
		sAtaque = ataqueBourke.ToString();
		sIncr = incrBourke.ToString();
		sDecr = decrBourke.ToString();
		sDependenciaInclinacion = dependenciaInclinacion.ToString();
		sPotenciaInclinacion = potenciaInclinacion.ToString();
		sAlturaUmbral = alturaUmbral.ToString();
		sAlturaCota = alturaCota.ToString();
		sVelocidad = velocidad.ToString();
		if (generarNuevoBourke) { sNuevo = "1"; } else { sNuevo = "0"; }
	}
	
	// Update is called once per frame
	void Update ()
	{
		for(int i = 0; i < velocidad; i++)
		{
			if (generar)
			{
				if(generarNuevoBourke && (counter == 0))
				{
					for (y = 0; y < yRes; y++)
					{
						for (x = 0; x < xRes; x++)
						{
							heights[x,y] = alturaInicial; //0.5f;
						}
					}
				}
	
				if (counter < iterBourke)
				{
					// escogemos un punto del mapa al azar:
					x = UnityEngine.Random.Range(50, (xRes-50));
					y = UnityEngine.Random.Range(50, (yRes-50));
					
					// escogemos un angulo al azar:
					float angulo = UnityEngine.Random.Range(1, 90);
	
					int signo = Random.Range(1,2);
					if (signo == 1) { ataqueBourke = - ataqueBourke; }
					
					//Debug.Log("1. x: " + x + "; y: " + y + "; angulo: " + angulo);
	
					//float alturaUmbral = 0.5f;;
	
					// triangangulo superior contando el eje Y hacia arriba:
					
					float b2 = xRes - x;
					float a2 = Mathf.Tan(angulo) * b2;
					
					for (int _y = y; _y < yRes; _y++)
					{
						for (int _x = 0; _x < xRes; _x++)
						{
							if (heights[_x,_y] >= alturaUmbral)
							{
								// normalizar la altura de heights[_x,_y] para poderlo aplicar como factor multiplicativo del ataque:
								/*float f = heights[_x,_y] - alturaUmbral;
								f = f/tData.size.y;*/ float f = 1f;
	
								// normalizar las coordenadas al rango [0..1]:
								if (dependenciaInclinacion == 1)
								{
									float y01 = (float) _y / (float) tData.size.x;
									float x01 = (float) _x / (float) tData.size.z;
									factorInclinacion = tData.GetSteepness(x01,y01); // la entrada de GetSteepness requiere valores de x,y normalizados en el rango 0..1, pero su salida varia de 0 (totalmente plano) a 90 (totalmente vertical)
									factorInclinacion = Mathf.Pow((factorInclinacion/90),potenciaInclinacion);
									f = 1f; // anular la dependencia con la altura
									//ataqueBourke *= 2;
								}
								else { factorInclinacion = 1f; }
	
								if (_x < (int) b2)	{ heights[_x, _y] -= ataqueBourke * decrBourke * factorInclinacion * f; }
								else 				{ heights[_x, _y] += ataqueBourke * incrBourke * factorInclinacion * f; }
							}
						}
						a2--;
						b2 = a2 / Mathf.Tan(angulo);
					}
					
					
					// triangangulo inferior y el eje Y contando hacia abajo:
					
					b2 = xRes - x;
					a2 = Mathf.Tan(-angulo) * b2;
					
					for (int _y = (y-1); _y >= 0; _y--)
					{
						for (int _x = 0; _x < xRes; _x++)
						{
							if (heights[_x,_y] >= alturaUmbral)
							{
								// normalizar la altura de heights[_x,_y] para poderlo aplicar como factor multiplicativo del ataque:
								float f = heights[_x,_y] - alturaUmbral;
								f = f/tData.size.y;
	
								// normalizar las coordenadas al rango [0..1]:
								if (dependenciaInclinacion == 1)
								{
									float y01 = (float) _y / (float) tData.alphamapHeight;
									float x01 = (float) _x / (float) tData.alphamapWidth;
									factorInclinacion = tData.GetSteepness(x01,y01);
									factorInclinacion = Mathf.Pow((factorInclinacion/90),potenciaInclinacion);
									f = 1f; // anular la dependencia con la altura
								}
								
								if (_x < (int) b2)	{ heights[_x, _y] -= ataqueBourke * decrBourke * factorInclinacion * f; }
								else 				{ heights[_x, _y] += ataqueBourke * incrBourke * factorInclinacion * f; }
							}
						}
						a2--;
						b2 = a2 / Mathf.Tan(-angulo);
					}
	
					tData.SetHeights(0, 0, heights);
				}
	
				else
				{
					// memorizar en main los valores elegidos por el usuario, antes de destruir el objeto:
					main.ataqueBourke = ataqueBourke;
					main.iterBourke = iterBourke;
					main.generarNuevoBourke = generarNuevoBourke;
					main.ataqueBourke = ataqueBourke;
					main.decrBourke = decrBourke;
					main.incrBourke = incrBourke;
					main.alturaTerrenoInicial = alturaInicial;
					main.dependenciaInclinacion = dependenciaInclinacion;
					main.alturaUmbral = alturaUmbral;
					main.alturaCota = alturaCota;
					main.velocidad = velocidad;
	
					Object.Destroy(this.gameObject);
					/*this.name = "objetoFinalizado";
					generar = false;
					counter = 0;*/
				}
				/*turno++;
				if (turno > 5) { turno = 0; }*/
	
				counter++;
				contadorUniversal.contador = (int) (((double) counter / (double) iterBourke) * 100);
				//Debug.Log((int) (((double) counter / (double) iterBourke) * 100));
			}
		}
	}

	void OnGUI()
	{
		if (mostrarGUI)
		{
			int m = 20;

			GUI.Box(new Rect(250, 10, tituloGUI.Length*6+20, 25), tituloGUI);
			
			GUI.Box(new Rect(250, m*2, 140, m), "Nº iteraciones: ");
			sIter = GUI.TextField(new Rect(390, m*2, 40, m), sIter, 3);
			GUI.Box(new Rect(430, m*2, 140, m), "Velocidad: ");
			sVelocidad = GUI.TextField(new Rect(570, m*2, 40, m), sVelocidad, 3);
	
			GUI.Box(new Rect(250, m*3, 180, m), "Altura inicial: ");
			sAltura = GUI.TextField(new Rect(430, m*3, 40, m), sAltura, 5);
			GUI.Box(new Rect(470, m*3, 40, m), "x /\\: ");
			sIncr = GUI.TextField(new Rect(510, m*3, 30, m), sIncr, 3);
			GUI.Box(new Rect(540, m*3, 40, m), "x \\/: ");
			sDecr = GUI.TextField(new Rect(580, m*3, 30, m), sDecr, 3);
	
			GUI.Box(new Rect(250, m*4, 180, m), "Ataque: ");
			sAtaque = GUI.TextField(new Rect(430, m*4, 50, m), sAtaque, 7);

			GUI.Box(new Rect(250, m*5, 140, m), "Umbral altura: ");
			sAlturaUmbral = GUI.TextField(new Rect(390, m*5, 40, m), sAlturaUmbral, 7);
			GUI.Box(new Rect(430, m*5, 140, m), "Cota altura: ");
			sAlturaCota = GUI.TextField(new Rect(570, m*5, 40, m), sAlturaCota, 7);

			GUI.Box(new Rect(250, m*6, 180, m), "Dependencia inclinacion: ");
			sDependenciaInclinacion = GUI.TextField(new Rect(430, m*6, 40, m), sDependenciaInclinacion, 7);
			GUI.Box(new Rect(470, m*6, 80, m), "y = x \u005E n: ");
			sPotenciaInclinacion = GUI.TextField(new Rect(550, m*6, 40, m), sPotenciaInclinacion, 3);

			GUI.Box(new Rect(250, m*7, 180, m), "Descartar terreno previo: ");
			sNuevo = GUI.TextField(new Rect(430, m*7, 40, m), sNuevo, 1);
			
			if (!generar)
			{
				if (GUI.Button(new Rect(250, m*8, 120, m), "Generar"))
					{
						iterBourke = int.Parse(sIter);
						alturaInicial = float.Parse(sAltura);
						ataqueBourke = float.Parse(sAtaque);
						incrBourke = float.Parse(sIncr);
						decrBourke = float.Parse(sDecr);
						if ( (string.Equals(sNuevo, "0")) || (string.Equals(sNuevo, "f")) || (string.Equals(sNuevo, "F")) || (string.Equals(sNuevo, "n")) || (string.Equals(sNuevo, "N")) ) { generarNuevoBourke = false; }
						if ( (string.Equals(sNuevo, "1")) || (string.Equals(sNuevo, "t")) || (string.Equals(sNuevo, "T")) || (string.Equals(sNuevo, "v")) || (string.Equals(sNuevo, "V")) || (string.Equals(sNuevo, "s")) || (string.Equals(sNuevo, "S"))) { generarNuevoBourke = true; }
						generar = true;
						dependenciaInclinacion = int.Parse(sDependenciaInclinacion);
						potenciaInclinacion = float.Parse(sPotenciaInclinacion);
						alturaUmbral = float.Parse(sAlturaUmbral);
						alturaCota = float.Parse(sAlturaCota);
						velocidad = int.Parse(sVelocidad);
					}
			}
			else if (generar)
			{
				GUI.color = Color.yellow;
				GUI.Box(new Rect(250, m*8, 120, m), "Generando..." + contadorUniversal.contador + " %");
				GUI.color = Color.white;
			}
		}

		if (Event.current.Equals(Event.KeyboardEvent("m")))
		{
			if (mostrarGUI)
			{
				mostrarGUI = false;
				// GameObject.Find("objetoActivo") == true
				//controlRaton.enabled = true;
			}
			else if (!mostrarGUI)
			{
				mostrarGUI = true;
				//controlRaton.enabled = false;
			}
		}
	}
}
