using UnityEngine;
using System.Collections;
using AssemblyCSharp;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;


public class CordilleraVisual : MonoBehaviour
{

		Terrain terrain;
		TerrainData tData;
		float[,] heights;
		int xRes, yRes;
		
		int numPicos;			string sNumPicos;
		float hipMax;			string sHipMax;
		float hipMin;			string sHipMin;
		float ataqPicos;		string sAtaqPicos;
		float alturaPicos;		string sAlturaPicos;
		int radMax;				string sRadMax;
		int radMin;				string sRadMin;
		int radCrat;			string sRadCrat;
		float ataqCrat;			string sAtaqCrat;

		float angulo;

		int counter = 0, excepciones = 0;
		bool generar = false;
		bool mostrarGUI = true;

		int y, x, dir_y, dir_x;

		Contador contadorUniversal;
		Main main;

		string tituloGUI = "Generador de cordilleras";

		// tecnica de Ondas de Estanque:
		public float multiplicadorOndas = 0.1f;
		public float precisionOndas = 1f;
		public float alturaOndas = 0.02f;
		public float limiteAnguloOndas = 359f;
		public float gradienteOndas = 0.99f;


		// metodos de la deformacion de icosfera y uvesfera:
		bool formandoEsfera = false;
		Mesh mesh;
		Vector3[] vertices;
		int i, j;
		bool paLante = true;


		// Use this for initialization
		void Start ()
		{
				this.name = "objetoActivo";

				// referencia a Main para guardar y recuperar parametros:
				main = GameObject.Find("Terrain").GetComponent<Main>();
				// referencia a Main para acceder al terreno:
				terrain = GameObject.Find("Terrain").GetComponent<Terrain>();

				tData = terrain.terrainData;
				xRes = tData.heightmapWidth;
				yRes = tData.heightmapHeight;
				heights = tData.GetHeights(0, 0, xRes, yRes);
				
				contadorUniversal = GameObject.Find("Contador").GetComponent<Contador>();
				contadorUniversal.contador = 0;

				// recuperar valores establecidos por el usuario en otras instancias del objeto y colocarlos en los "labels" del GUI:
				numPicos = main.numPicos;		sNumPicos = numPicos.ToString();
				hipMax = main.hipMax;			sHipMax = hipMax.ToString();
				hipMin = main.hipMin;			sHipMin = hipMin.ToString();
				alturaPicos = main.alturaPicos;	sAlturaPicos = alturaPicos.ToString();
				ataqPicos = main.ataqPicos;		sAtaqPicos = ataqPicos.ToString();
				radMax = main.radMax;			sRadMax = radMax.ToString();
				radMin = main.radMin;			sRadMin = radMin.ToString();
				radCrat = main.radCrat;			sRadCrat = radCrat.ToString();
				ataqCrat = main.ataqCrat;		sAtaqCrat = ataqCrat.ToString();

				// escogemos un punto del mapa al azar:
				x = UnityEngine.Random.Range(50, (xRes-50));
				y = UnityEngine.Random.Range(50, (yRes-50));
				
				// escogemos un angulo al azar:
				angulo = UnityEngine.Random.Range(1, 90);
		}
	
		// Update is called once per frame
		void Update ()
		{

			if (generar)
					{				
					if (counter < numPicos)
					{
						//tData = AlgoritmoCordilleraV2(x, y, angulo, numPicos, alturaPicos, ataqPicos, hipMin, hipMax, 2, radMin, radMax, radCrat, ataqCrat);
	
	
						//tData.SetHeights(0, 0, heights);
					}
					
					// terminar y devolver el nuevo mapa a main():
					else
					{
							// memorizar parametros establecidos por el usuario:
							main.numPicos = numPicos;
							main.alturaPicos = alturaPicos;
							main.hipMin = hipMin;			main.hipMax = hipMax;
							main.ataqPicos = ataqPicos;		main.ataqCrat = ataqCrat;
							main.radMax = radMax;			main.radMin = radMin;
							main.radCrat = radCrat;
							
							UnityEngine.Object.Destroy(this.gameObject);								
					}
	
					counter++;
					contadorUniversal.contador = (int) (((double) counter / (double) numPicos) * 100);
					}

			if (formandoEsfera)
			{
				try
				{
					//vertices[i].Set(vertices[i].x * (1 + heights[x,y]), vertices[i].y * (1 + heights[x,y]), vertices[i].z * (1 + heights[x,y]));
					vertices[i].Set(vertices[i].x * 1.05f, vertices[i].y * 1.05f, vertices[i].z * 1.05f);
					heights[x,y] = 0f;

					if (paLante) { x ++; }
					else if (!paLante) { x --; }

					if (x == 768) { paLante = false; y ++; formandoEsfera = false; }
					if (x == 0) { paLante = true; y ++; }

					i ++;

					mesh.vertices = vertices;
					mesh.RecalculateBounds();
					tData.SetHeights(0,0, heights);
				}
				catch (IndexOutOfRangeException)
				{
					formandoEsfera = false;
					i = 0;
				}
				


			}

		}

		void OnGUI()
		{
				if (mostrarGUI)
				{
						GUI.Box(new Rect(250, 10, /*400*/ tituloGUI.Length*6+20, 20), tituloGUI);
						
						GUI.Box(new Rect(250, 40, 180, 20), "Num de picos: ");
						sNumPicos = GUI.TextField(new Rect(430, 40, 40, 20), sNumPicos, 2);

						GUI.Box(new Rect(250, 60, 180, 20), "Distancia entre picos: ");
						GUI.Box(new Rect(430, 60, 40, 20), "Min: ");
						sHipMin = GUI.TextField(new Rect(470, 60, 40, 20), sHipMin, 3);
						GUI.Box(new Rect(510, 60, 40, 20), "Max: ");
						sHipMax = GUI.TextField(new Rect(550, 60, 40, 20), sHipMax, 3);

						GUI.Box(new Rect(250, 80, 180, 20), "Altura maxima de los picos: ");
						sAlturaPicos = GUI.TextField(new Rect(430, 80, 40, 20), sAlturaPicos, 5);

						GUI.Box(new Rect(250, 120, 180, 20), "Grado ataque del suavizado: ");
						sAtaqPicos = GUI.TextField(new Rect(430, 120, 40, 20), sAtaqPicos, 5);
						
						GUI.Box(new Rect(250, 100, 180, 20), "Radio de las montañas: ");
						GUI.Box(new Rect(430, 100, 40, 20), "Min "); // radio Final
						sRadMin = GUI.TextField(new Rect(470, 100, 40, 20), sRadMin, 3);
						GUI.Box(new Rect(510, 100, 40, 20), "Max "); // radio Final
						sRadMax = GUI.TextField(new Rect(550, 100, 40, 20), sRadMax, 3);

						GUI.Box(new Rect(250, 140, 90, 20), "Crateres: ");
						GUI.Box(new Rect(340, 140, 90, 20), "Radio: ");
						sRadCrat = GUI.TextField(new Rect(430, 140, 40, 20), sRadCrat, 5);
						GUI.Box(new Rect(470, 140, 80, 20), "Ataque: ");
						sAtaqCrat = GUI.TextField(new Rect(550, 140, 40, 20), sAtaqCrat, 5);


						// Nueva tecnica basada en la simulacion de ondas de estanque:

						/*GUI.Box(new Rect(250, 230, 120, 20), "Ondas de estanque");
						if (GUI.Button(new Rect(370, 230, 90, 20), "Generar"))
						{
							generarOndas();
						}


						// Experimento con icosfera:
						
						GUI.Box(new Rect(250, 250, 120, 20), "Deformar icosfera");
						if (GUI.Button(new Rect(370, 250, 90, 20), "Venga"))
						{
							deformarIcosfera();
						} 


						// arboles L-system:
						
						GUI.Box(new Rect(250, 270, 120, 20), "Arboles L-system");
						if (GUI.Button(new Rect(370, 270, 90, 20), "Venga"))
						{
							arboles();
						}*/


						//
						
						/*GUI.Box(new Rect(250, 290, 120, 20), "");
						if (GUI.Button(new Rect(370, 290, 90, 20), "Venga"))
						{

							
						}*/
						
				} 
			
			
			
			
			
			
			if (!generar)
						{
							if(GUI.Button(new Rect(250, 190, 120, 20), "Generar"))
							{
								numPicos = int.Parse(sNumPicos);		alturaPicos = float.Parse(sAlturaPicos);
								hipMax = float.Parse(sHipMax);			hipMin = float.Parse(sHipMin);	
								ataqPicos = float.Parse(sAtaqPicos);	ataqCrat = float.Parse(sAtaqCrat);
								radMax = int.Parse(sRadMax);			radMin = int.Parse(sRadMin);		radCrat = int.Parse(sRadCrat);
								
								generar = true;
							}
						}
						else if (generar)
						{
							GUI.color = Color.yellow;
							GUI.Box(new Rect(250, 190, 120, 20), "Generando..." + contadorUniversal.contador + " %");
							GUI.color = Color.white;

							tData = AlgoritmoCordilleraV2(x, y, angulo, numPicos, alturaPicos, ataqPicos, hipMin, hipMax, 2, radMin, radMax, radCrat, ataqCrat);
							tData.SetHeights(0, 0, heights);
						}


				if (Event.current.Equals(Event.KeyboardEvent(main.teclaMenu)))
				{
						if (mostrarGUI)
						{
							mostrarGUI = false;
							// GameObject.Find("objetoActivo") == true
							//controlRaton.enabled = true;
						}
						else if (!mostrarGUI)
						{
							mostrarGUI = true;
							//controlRaton.enabled = false;
						}
				}
		}


		TerrainData AlgoritmoCordilleraV2(int x, int y, float angulo, int numPicos, float alturaPicos, float ataque, float hipMin, float hipMax, int ramas, int radioInicio, int radioFinal, int radCrat, float ataqCrat)
		{
			/*Action work = delegate	// metodo anonimo
				{*/
			// algoritmo per se:
			for (int i = 0; i < numPicos; i++)
			{
				// escoger hipotenusa y lado adyacente aleatorios:
				float hipotenusa = UnityEngine.Random.Range(hipMin, hipMax);
				
				float adyacente = Mathf.Cos (angulo) * hipotenusa;
				x = x + (int) adyacente;
				float opuesto = Mathf.Sin (angulo) * hipotenusa;
				y = y + (int) opuesto;
				
				try
				{
					if ((heights[x,y] < alturaPicos))
					{
						heights[x,y] += alturaPicos * UnityEngine.Random.Range(0.8f, 1.2f);
						heights = AlgoritmoSuavizarPico2(heights, x, y, ataque, radioInicio, UnityEngine.Random.Range((radioFinal/3), radioFinal), angulo);
					}
				}
				catch (IndexOutOfRangeException) { excepciones ++; }
				
				
				// escogemos si crear una ramificacion o no en la cordillera:
				//if (UnityEngine.Random.Range(1, 3) == 1)
				/*{
							//Debug.Log("Creando nueva rama");
							try
							{
								AlgoritmoCordilleraV2(x, y, (angulo + UnityEngine.Random.Range(-1f, 1f)), (numPicos-1), (alturaPicos*0.7f), ataque, hipMin, hipMax, 0, radioInicio, radioFinal);
								Debug.Log("Ok");
							}
							catch (IndexOutOfRangeException) { excepciones++; }
						}*/
				
				// escogemos un angulo al azar:
				angulo += UnityEngine.Random.Range(-0.1f, 0.1f);
			}
			/*};
				work();*/
			
			//Debug.Log("Excepciones encontradas: " + excepciones);
			
			// terminar y devolver el nuevo mapa a main():
			return tData;
			
		}
		
		
		float[,] AlgoritmoSuavizarPico2(float[,] heights, int x, int y, float ataque, int radioInicio, int radioFinal, float anguloInicial)
		{
			// metodo de suavizado en el que se ataca al terreno desde el centro hacia afuera en linea recta, para todos los angulos alrededor del centro
			
			int _x = x;
			int _y = y;
			float alturaInicial = heights[x,y];
			float alturaActual;
			
			// suavizar desde el centro hacia afuera:
			
			for (float ang = 0; ang < 359f; ang = ang + 0.1f)
			{
				alturaActual = alturaInicial;
				
				
				for (int radio = radioInicio; radio < UnityEngine.Random.Range((int) (radioFinal/3),radioFinal); radio++)
				{
					x = (int) (_x + radio * Mathf.Cos(ang)); // obtener lado adyacente
					y = (int) (_y + radio * Mathf.Sin(ang));
					
					try
					{
						if ((heights[x,y] < alturaActual))
						{
							alturaActual = alturaActual * UnityEngine.Random.Range(ataque-0.0001f, ataque+0.0001f);// * (0.5f + (Mathf.Sin(ang)*(Mathf.Cos(ang))));
							heights[x,y] = alturaActual;
						}
					}
					catch (IndexOutOfRangeException)
					{
						excepciones++;
					}
				}
			}
			
			// suavizar desde el fuera hacia adentro (para crear las calderas de los crateres):
			
			for (float ang = 0; ang < 359; ang = ang + 0.1f)
			{
				alturaActual = alturaInicial;
				
				
				for (int radio = radioInicio; radio > 0; radio--)
				{
					x = (int) (_x + radio * Mathf.Cos(ang));
					y = (int) (_y + radio * Mathf.Sin(ang));
					
					try
					{
						if ((heights[x,y] < alturaActual))
						{
							alturaActual = alturaActual * UnityEngine.Random.Range(ataqCrat - 0.001f, ataqCrat + 0.001f);
							heights[x,y] = alturaActual;
						}
					}
					catch (IndexOutOfRangeException)
					{
						excepciones++;
					}
				}
			}
			
			
			
			/*x = x;
				_y = y;
				alturaActual = heights[x, y];
	
				for (int radio = radioInicio; radio < radioFinal; radio++)	// distancia al punto
					{
						// calculo la nueva altura: altura original * 0.95
						alturaActual = alturaActual * ataque;
						
						for (int angulo = 0; angulo < 359; angulo++)	// girar alrededor del punto
						{
							x = (int) (_x + radio * Mathf.Cos(angulo));
							y = (int) (_y + radio * Mathf.Sin(angulo));
	
							try
							{
								if ((heights[x,y] < alturaActual))
								{
									heights[x,y] = alturaActual;
								}
								else { continue; }
	
							}
							catch (IndexOutOfRangeException) { excepciones ++; }
						}
					}*/
			
			
			//tData.SetHeights(0, 0, heights);
			return heights;
		}



		void generarOndas()
		{
			// escoger un punto aleatorio del mapa:
			int _x = x = (int) xRes/2;
			int _y = y = (int) yRes/2;

			heights[x,y] = alturaOndas;


			float alturaActual = alturaOndas;

			for (int radio = 0; radio < xRes; radio++)
				{
					alturaActual *= gradienteOndas;
				
					for (float angulo = 0; angulo < limiteAnguloOndas; angulo += precisionOndas)	// girar alrededor del punto
					{
						x = (int) (_x + radio * Mathf.Cos(angulo));
						y = (int) (_y + radio * Mathf.Sin(angulo));

						try
						{
							//if ((heights[x,y] < alturaActual))
							{
								heights[x,y] = alturaActual + (Mathf.Sin((float) radio * multiplicadorOndas));;
							}
							//else { continue; }

						}
						catch (IndexOutOfRangeException) { excepciones ++; }
					}
				}

			tData.SetHeights(0,0,heights);
		}


		void deformarIcosfera()
		{
			/*GameObject esfera = GameObject.CreatePrimitive(PrimitiveType.Sphere);
			esfera.transform.localScale = new Vector3(500,500,500);
			Collision collision = new Collision();*/

			//GameObject esfera = GameObject.Find("uvesfera129");
			/*UnityEngine.Object.Destroy(GameObject.Find("uvesfera129")); 
			GameObject esfera = (GameObject) Instantiate(Resources.Load("uvesfera129", typeof(GameObject)));
			esfera.name = "uvesfera129";*/
			UnityEngine.Object.Destroy(GameObject.Find("esfera")); 
			GameObject esfera = (GameObject) Instantiate(Resources.Load("esfera", typeof(GameObject)));
			esfera.name = "esfera";

			//esfera.transform.localScale = new Vector3(500f, 500f, 500f);

			//MeshFilter mf = esfera.transform.Find("default").GetComponent<MeshFilter>();
			MeshFilter mf = esfera.GetComponent<MeshFilter>();
			mesh = mf.mesh;
			vertices = mesh.vertices;

			Debug.Log("Cantidad de vertices de la esfera: " + vertices.Length + "\nTriangulos: " + mesh.triangles.Length);
			/*int[] nuevaCantidadTriangulos  = new int[33024];
			for (int a = 0; a < nuevaCantidadTriangulos.Length; a ++)
			{
				nuevaCantidadTriangulos[a] = mesh.triangles[a];
			}
			mesh.triangles = nuevaCantidadTriangulos;*/


			formandoEsfera =true;
			x = 0; y = 0; i = 0;


			//Debug.Log(vertices[0].magnitude);
			//vertices[0].Set(vertices[0].x * 1.5f, vertices[0].y * 1.5f, vertices[0].z * 1.5f);

			/*int i = 0; bool broke = false;
			for(int x = 0; x < xRes; x ++)
			{
				for (int y = 0; y < yRes; y ++)
				{
					try
					{
						vertices[i].Set(vertices[i].x + heights[x,y], vertices[i].y + heights[x,y], vertices[i].z + heights[x,y]);
						i ++;
					}
					catch (IndexOutOfRangeException)
					{
						broke = true;
						break;
					}
				}
				if (broke) { break; }
			}

			mesh.vertices = vertices;
			mesh.RecalculateBounds();*/
		}


		void arboles()
		{
			Instantiate(Resources.Load("ArbolesLSystem", typeof(ArbolesLSystem)) as ArbolesLSystem);
		}


	
}

